//ブラウザ判別
var IE = navigator.appName.indexOf("Microsoft Internet Explorer",0) != -1;

flag = false;
/*
 * ページスクロール
 */
function getAnchorPos(elementID){
	var objnew = new Object();
	if(document.getElementById){
		var obj = document.getElementById(elementID);
		objnew.x = obj.offsetLeft;
		objnew.y = obj.offsetTop;
		while((obj = obj.offsetParent) != null){
			objnew.x += obj.offsetLeft;
			objnew.y += obj.offsetTop;
		}
	}
	else if(document.all){
		var obj = document.all(elementID);
		objnew.x = obj.offsetLeft;
		objnew.y = obj.offsetTop;
		while((obj = obj.setParent) != null){
				objnew.x += obj.offsetLeft;
				objnew.y += obj.offsetTop;
		}
	}
	else if(domument.layers){
		objnew = document.anchors[elementID].x;
		objnew = document.anchors[elementID].y;
	}
	else{
		objnew .x = 0;
		objnew.y = 0;
	}
	return objnew;
}

function goToAnchor(elementID){
	if((getAnchorPos(elementID).x >=0 || getAnchorPos(elementID).y >=0)){
		pageScroll(0,getAnchorPos(elementID).y,10);
	}
	else {
		flag = true;
	}
	return false;
}

var timeID;
function pageScroll(endX,endY,frms,cuX,cuY){
//ページスクロール本体
	if(timeID){
		clearTimeout(timeID);
	}
	if(endX < 0){
		endX = 0;
	}
	if(endY < 0){
		endY = 0;
	}
	if(!cuX){
		cuX = getScrollLeft();
	}
	if(!cuY){
		cuY = getScrollTop();
	}
	if(endY > cuY && endY > (getAnchorPos('end').y) - getInnerSize().height) {
		endY = (getAnchorPos('end').y - getInnerSize().height)+1;
	}
	cuX += (endX - getScrollLeft())/frms;
	if(cuX < 0) cuX = 0;
	cuY +=(endY - getScrollTop())/frms;
	if(cuY < 0) cuY = 0;
	var posiX = Math.floor(cuX);
	var posiY = Math.floor(cuY);
	window.scrollTo(posiX,posiY);
	if(posiX != endX || posiY != endY){
	timeID = setTimeout("pageScroll("+endX+","+endY+","+frms+","+cuX+","+cuY+")",1);
	}
}

function getScrollLeft(){
//現在の横位置を取得
	if(document.body.scrollLeft){
		return document.body.scrollLeft;
	}
	else if (document.documentElement.scrollLeft) {
		return document.documentElement.scrollLeft;
	}
	else if(window.pageXOffset){
		return window.pageXOffset;
	}
	else{
	return 0;
	}
}

function getScrollTop(){
//現在の縦位置を取得
	if(document.body.scrollTop){
		return document.body.scrollTop;
	}
	else if(document.documentElement.scrollTop) {
		return document.documentElement.scrollTop;
	}
	else if(window.pageYOffset){
		return window.pageYOffset;
	}
	else{
		return 0;
	}
}

function getInnerSize(){
//ウィンドウサイズを取得
	var obj = new Object();
	if(document.all || (document.getElementById && IE)){
		obj.width = document.body.clientWidth;
		obj.height = document.body.clientHeight;
	}
	else if(document.layers || document.getElementById){
		obj.width = window.innerWidth;
		obj.height = window.innerHeight;
	}
	return obj;
}