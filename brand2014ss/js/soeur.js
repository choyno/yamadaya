$(document).ready(function() {
	/** SUBMENU **/
	$('ul.menu li ul').hide();
	$('ul.menu li').hover(function(){
		if($(this).find($('ul')).length > 0) {
			$('ul', this).show();
		}
	}, function(){
		if($(this).find($('ul')).length > 0) {
			$('ul', this).fadeOut();
		}
	});
	
	$('#slider').nivoSlider({
		effect: 'fade',
		animSpeed: 500,
		pauseTime: 5000,
		controlNav: false
    });
    
    $(".validate").each(function(i) {
		$(this).validate({
			ignore: ".validate :not(:visible)",
			errorClass: "error"
		});
		$.metadata.setType("attr", "validate");
	});
	
	$('#sliderControl').slider({
		step: 0.01,
		min: 18,
		max: 100,
		value: 59,
		slide: function(event, ui) {
			var percentage = ($('#sliderContainer').width() * ui.value) / 100;
			$('#sliderContainer').css('left', "-" + (percentage - $('body').width()) + "px");
		}
	});
	
	// start with -10 because of padding
	var totalWidth = -10;
	if($.browser.msie && $.browser.version < 9) { var totalWidth = 0; }
	
	var imagesNoCache = $('#sliderContainer .sliderElement img');
	for(i in imagesNoCache) {
		imagesNoCache[i].src = imagesNoCache[i].src +  "?" + new Date().getTime();
	}
		
	$('#sliderContainer .sliderElement img').load(function(e) {
		// first calculate total width
		totalWidth += 10 + $(e.target).width();
		$('#sliderContainer').width(totalWidth);
				
		// Set some defs
		var min = ($('body').width() * 100) / totalWidth;
		$('#sliderControl').slider({
			'min': min
		});
		$('#sliderControl').slider({
			'value': /*(min + 100) / 2*/ min
		});
		
		var percentage = ($('#sliderContainer').width() * 0 /*(min + 100) / 2*/) / 100;
		$('#sliderContainer').css('left', "-" + (percentage - $('body').width()) + "px");
	});
	
	$(window).resize(function() {
		var min = ($('body').width() * 100) / totalWidth;
		$('#sliderControl').slider({
			'min': min
		});
	});
});