function smartRollover() {
	if(document.getElementsByTagName) {
		var images = document.getElementsByTagName("img");

		for(var i=0; i < images.length; i++) {
			if(images[i].getAttribute("src").match("_off."))
			{
				images[i].onmouseover = function() {
					this.setAttribute("src", this.getAttribute("src").replace("_off.", "_on."));
				}
				images[i].onmouseout = function() {
					this.setAttribute("src", this.getAttribute("src").replace("_on.", "_off."));
				}
			}
		}
	}
}

if(window.addEventListener) {
	window.addEventListener("load", smartRollover, false);
}
else if(window.attachEvent) {
	window.attachEvent("onload", smartRollover);
}


function externalLinks() {   
	if (!document.getElementsByTagName) return;   
	var anchors = document.getElementsByTagName("a");   
	for (var i=0; i<anchors.length; i++) {   
		var anchor = anchors[i];   
		if (anchor.getAttribute("href") &&   
		anchor.getAttribute("rel") == "external")   
		anchor.target = "_blank";   
	}   
}

if(window.addEventListener) {
	window.addEventListener("load", externalLinks, false);
}
else if(window.attachEvent) {
	window.attachEvent("onload", externalLinks);
}
