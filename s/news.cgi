#!/usr/bin/perl
##=============================================================================
##■whatsnew管理システム
##■バージョン 1.3
##■作成日:2007/12/06
##■作成者:DOS湯上
##■更新履歴:
##	2007/12/06	ver1.0	・作成
##	2008/04/21	ver1.2	・カテゴリ、メモ項目追加。
##						・コメント欄URL自動リンク機能。
##	2008/12/08	ver1.3	・画像複数アップロード機能。
##
##■主な機能概要:
##	・IDパスワードで管理画面にログイン制限。
##		ログイン情報はクッキーに保存。ログアウトで削除。
##
##	・登録情報ログの一覧表示。
##		年、月、日、更新日付でソート。
##		ページング機能あり。
##
##	・[年][月][日][カテゴリ][メモ][タイトル][コメント][リンクタイトル][リンクURL][新規ウィンドウ][画像データ]を
##		新規登録、編集可能。（編集時は登録画像のプレビュー、差し替え、削除可能）
##		年、月、日、タイトルは必須項目。
##		各項目文字数制限指定可能。
##		画像データはjpg,gif,png形式複数登録可能。
##		項目エラーの場合エラーメッセージを表示。
##	・ログの削除は1件ごと可能
##	・確認ページはなし（削除、公開時はダイアログで確認）
##
##	・ログはcsv形式。
##		記事NO,年,月,日,カテゴリNO,メモ,タイトル,コメント,リンクURL,リンクタイトル,新規ウィンドウフラグ,画像NO,画像拡張子,登録日時,更新日時
##		更新日時順。
##		ログ最大登録件数を超えた場合、更新日時の古いデータから順に削除。
##	・アップロード画像名は記事NO+_+画像NO+拡張子で保存。
##
##	・公開機能でトップページ、リストページ、詳細ページのHTML生成する。
##		年、月、日、更新日時でソート（トップページ、リストページ）
##		リストページはページング機能あり。
##		トップページ、リストページ表示件数は個別に指定可。
##	・コメントがない場合、トップページ、リストページのタイトルリンクなし。
##		ただしリンクURLがある場合そのURLをリンクする。
##
##	・トップページ、リストページ、管理画面で返される情報
##		RECNO,年、月、日、曜日、カテゴリNo.、カテゴリ名、メモ、タイトル、省略タイトル、コメントありフラグ、リンクありフラグ、画像ありフラグ、
##		詳細ページ又はリンク先へのリンクURL、新規ウィンドウフラグ、更新(年,月,日,曜日,時,分,秒）、新着期間フラグ
##		前のページ、次のページ、ページリスト、現在ページ（リストページ、管理画面）、ビューフラグ
##		メッセージ（管理画面）
##	・詳細ページで返される情報
##		RECNO、年、月、日、曜日、カテゴリNo.、カテゴリ名、メモ、タイトル、コメント、リンクURL、リンクタイトル、省略リンク、新規ウィンドウフラグ、
##		画像毎(画像ありフラグ、画像URL、画像横幅、画像縦幅)、更新(年,月,日,曜日,時,分,秒）、新着期間フラグ、ビューフラグ
##		デフォルト画像設定時、画像なしの場合デフォルト画像URLを画像URLとして返す。
##
##	・公開データについて補足
##		公開データは静的なためリアルタイム更新はされない。（登録編集削除で差異）
##		画像データは登録時テンポラリディレクトリに保管される。（一般公開ディレクトリとは別）
##		公開作業時
##		1.公開ディレクトリのデータを一旦全削除。
##			リストページ、詳細ページ、公開画像データ（トップページは上書きのため削除しない）
##		2.新たに全ページ生成（トップページ、リストページ、詳細ページ）
##		3.テンポラリディレクトリより画像データを公開ディレクトリにコピー）
##		以上で公開データが登録データと一致。
##
##	・省略タイトル、省略URLについて補足
##		タイトル省略文字数設定時、文字数を超える場合、トップページとリストページのタイトルは省略表示。
##		リンク省略文字数設定時、文字数を超えるURLの場合、詳細ページのリンクURL表記を省略表示。（リンクタイトルありの場合省略なし）
##
##	・画像横幅、縦幅について補足
##		画像から横幅縦幅px情報を取得する。
##		横長か縦長かで返す横幅pxを指定。（縦幅pxは比率計算）
##
##	・コメント内URL自動リンクについて
##		コメント内にURL、メールアドレスが含まれる場合、タグを付加して表示する。
##
##	・プレビュー機能によりページを動的表示可能。（トップページ、リストーページ、詳細ページ）
##
##	・エラー発生時エラーページ表示
##
##	・文字コード
##		スクリプト=EUC
##		テンプレート＝SJIS
##		ログファイル＝SJIS
##		出力ページ=SJIS
##
##	・テンプレートはHTML::Templateを使用（ライブラリディレクトリにHTML/Template.pm配置）
##	・日本語文字コード変換はJcode.pmを使用（ライブラリディレクトリにJcode.pm,Jcode配置）
##	・PARAM値取得にCGIモジュールを使用（標準モジュール）
##	・ファイル拡張子判別にFile::Basenameを使用（標準モジュール）
##	・ファイルコピーにFile::Copyを使用（標準モジュール）
##	・ファイル制御にFcntlを使用（標準モジュール）
##
##■構成例:
##apps（whatsnewシステム利用ディレクトリ）※なるべくウェブ外に配置
##	lib
##		HTML
##			Template.pm(テンプレートライブラリ）
##		Jcode.pm（日本語ライブラリ）
##		Jcode（Jcode用ファイルディレクトリ）
##	tmpl（テンプレートディレクトリ）
##		admin
##			login.tmpl（ログイン画面テンプレート）
##			admin_list.tmpl（管理リスト画面テンプレート）
##			form.tmpl（入力フォームテンプレート）
##			error.tmpl（エラーページテンプレート）
##		top.tmpl（トップページテンプレート）
##		list.tmpl（リストページテンプレート）
##		detail.tmpl（詳細ページテンプレート）
##	log（ログディレクトリ）
##		news.log（ログファイル）
##
##htdocs（ウェブディレクトリ）
##	top.html（トップページ）（あらかじめ配置）
##	s（CGIディレクトリ）
##		news.cgi（本スクリプト）
##		image_temp（画像テンポラリディレクトリ）
##	news（リストページ出力ディレクトリ）
##		detail（詳細ページ出力ディレクトリ）
##			image（画像公開ディレクトリ）
##=============================================================================
##設定項目#####################################################################
#■用途
#テスト用
#---------------------------------------------------------------------
#■管理者ID
our $ID = 'yamadaya';
#■パスワード
our $PASSWORD = 'admin9306';
#---------------------------------------------------------------------
#■トップページ表示件数
our $TOP_COUNT = 5;
#■リストページ表示件数
our $LIST_COUNT = 20;
#■ページ表示件数端数調整するか（1:true,0:false）
our $ADJ_COUNT = 0;
#■新着判定期間（日）
our $NEWDATE = 1;
#■管理ページ表示件数
our $ADMIN_LIST_COUNT = 20;
#---------------------------------------------------------------------
#■タイトル最大文字数（全角）
our $TITLE_MAX = 100;
#■コメント最大文字数（全角）
our $COMMENT_MAX = 2500;
#■コメント内URL自動リンク（1:true,0:false）
our $AUTOLINK = 1;

#■メモ必須か（1:true,0:false）
our $MEMO_REQUIRED = 0;
#■メモ最大文字数（全角）
our $MEMO_MAX = 25;

#■カテゴリ必須か（1:true,0:false）
our $CATEGORY_REQUIRED = 1;

#■リンク最大文字数（半角）
our $LINK_MAX = 300;
#■リンク名最大文字数（全角）
our $LINKNAME_MAX = 100;

#■タイトル省略文字数（0の場合省略しない）
our $SHORT_TITLE = 0;
#■リンク省略文字数（0の場合省略しない）
our $SHORT_LINKNAME = 0;
#■省略時表示文
our $REP_CHAR = '…';

#■画像最大枚数
our $IMG_COUNT = 6;
#■画像回り込み設定（1枚のとき）（1:true,0:false）
our $IMG_FLOAT = 1;
#■画像折り返し
our $IMG_WRCOUNT = 3;

#■画像最大サイズ（byte)（0の場合制限なし）
our $IMG_SIZE_MAX = 0;
#■縦長画像幅表示(px)
our $IMG_TOOL_MAX = 161;
#■横長画像幅表示(px)
our $IMG_WIDE_MAX = 161;

#■セレクトボックス表示年数過去
our $SELECT_YEAR_PAST = 10;
#■セレクトボックス表示年数未来
our $SELECT_YEAR_FUTURE = 2;
#---------------------------------------------------------------------
#■CGI名
our $CGINAME = 'news.cgi';
#■ログ
our $LOGS = './apps/log/news.log';
#■ログ最大件数
our $MAX_COUNT = 100;
#■cookie名
our $COOKIENAME = 'whatsnews01';
#■cookie期間
our $LIFETIMES = 3600;
#---------------------------------------------------------------------
#■テンプレートディレクトリ
our $TMPL_DIR = './apps/tmpl/';
#■トップページ
our $TMPL_TOP = $TMPL_DIR . 'top.tmpl';
#■トップページ（iframe,flash等で別ページの場合設定）
#our $TMPL_MAIN = $TMPL_DIR . 'main.tmpl';
our $TMPL_MAIN = '';
#■リストページ
our $TMPL_LIST = $TMPL_DIR . 'list.tmpl';
#■詳細ページ
our $TMPL_DETAIL = $TMPL_DIR . 'detail.tmpl';
#■ログイン
our $TMPL_LOGIN = $TMPL_DIR . 'admin/login.tmpl';
#■管理ページ
our $TMPL_ADMIN_LIST = $TMPL_DIR . 'admin/admin_list.tmpl';
#■入力フォーム
our $TMPL_FORM = $TMPL_DIR . 'admin/form.tmpl';
#■エラー
our $TMPL_ERROR = $TMPL_DIR . 'admin/error.tmpl';
#---------------------------------------------------------------------
#■出力ページ
#■CGIディレクトリ
our $CGI_DIR = './';
#our $HTML_TOP_DIR = '/';
#■トップページ（CGIからの相対パス）
our $HTML_TOP_REL = '../';

#■トップページファイル名
our $HTML_TOP_NAME = 'index.html';

#■リストページ絶対パス
our $HTML_LIST_DIR = '/news/';
#■リストページ（CGIからの相対パス）
our $HTML_LIST_REL = '../news/';
#■リストページ接頭名
our $HTML_LIST_NAME = 'index';
#■リストページ拡張子
our $HTML_LIST_EXT = '.html';

#■詳細ページ絶対パス
our $HTML_DETAIL_DIR = '/news/detail/';
#■詳細ページ（CGIからの相対パス）
our $HTML_DETAIL_REL = '../news/detail/';
#■詳細ページ接頭名
our $HTML_DETAIL_NAME = 'news';
#■詳細ページ拡張子
our $HTML_DETAIL_EXT = '.html';
#---------------------------------------------------------------------
#■画像ディレクトリ絶対パス
our $IMG_DIR = '/news/detail/image/';

#■画像ディレクトリ（CGIからの相対パス）
our $IMG_DIR_REL = '../news/detail/image/';
#■画像テンポラリディレクトリ
our $IMG_TMP_DIR = '../news/detail/image_temp/';
our $IMG_TMP_DIR2 = '../news/detail/image_temp/';
#■画像名
our $IMG_NAME = 'img';

#■アップロード可能画像拡張子リスト（ドットなし）
our @IMG_SUFFIX_LIST = ('jpg','jpeg','gif','png','JPG','JPEG','GIF','PNG');
#■デフォルト画像（設定する場合画像テンポラリディレクトリに置く）
#$IMG_DEF = 'imgdef.jpg';
our $IMG_DEF = '';
#---------------------------------------------------------------------
#■カテゴリ設定
#our @CATEGORY = ();
our @CATEGORY = ('','','','','','');
our $CATEGORY_NONE = '表示なし';
#---------------------------------------------------------------------
#■曜日表示設定
our @WEEKNAME = ('Sun','Mon','Tue','Wed','Thr','Fri','Sut');
#our @WEEKNAME = ('日','月','火','水','木','金','土');
#---------------------------------------------------------------------
#■メッセージ
#■管理メッセージリスト
our @MSGLIST = ('','完了しました','削除しました','公開しました','ログインしました');
#■管理エラーメッセージリスト
our @ERRMSGLIST = ('','データ存在しません。','ファイルエラーです。');
#---------------------------------------------------------------------
#■入力フォームエラーメッセージ
#■日付エラーメッセージ
our $ERRMSG_DATE = '日付が正しくありません。';
#■カテゴリエラーメッセージ
our $ERRMSG_CATEGORY = '種別の選択が不正です。';
#■メモエラーメッセージ
our $ERRMSG_MEMO = '部署は全角' . $MEMO_MAX . '字以内入力必須です。';
#■タイトルエラーメッセージ
our $ERRMSG_TITLE = 'タイトルは全角' . $TITLE_MAX . '字以内入力必須です。';
#■コメントエラーメッセージ
our $ERRMSG_COMMENT = '詳細は全角' . $COMMENT_MAX . '字以内です。';
#■リンク記入なしエラーメッセージ
our $ERRMSG_LINK1 = 'リンクがありません。';
#■リンク書式エラーメッセージ
our $ERRMSG_LINK2 = 'リンクの書式が正しくありません。';
#■リンクタイトルエラーメッセージ
our $ERRMSG_LINKNAME = 'リンクタイトルは全角' . $LINKNAME_MAX . '字以内です。';
#■画像拡張子エラーメッセージ
our $ERRMSG_IMAGEEXT = '許可されない拡張子のファイルです。';
#■画像拡張子エラーメッセージ
our $ERRMSG_IMAGEEXT2 = '画像形式が異なります。';
#■画像ContentTypeエラーメッセージ
our $ERRMSG_IMAGETYPE = '許可されないContentTypeのファイルです。';
#■画像サイズエラーメッセージ
our $ERRMSG_IMAGESIZE = 'ファイルデータが大きすぎます。';
#■不正エラーメッセージ
our $ERRMSG_DEFAULT = '不正です。';
#---------------------------------------------------------------------
#■ライブラリディレクトリ
use lib('./apps/lib');
#■htmlテンプレート
use HTML::Template;
#■日本語文字コード変換ライブラリ
use Jcode;
#■標準モジュール
use CGI;
use File::Copy;
use File::Basename;
use Fcntl qw(:flock);
#---------------------------------------------------------------------
##設定ここまで#################################################################
#main
#デフォルト値
&initdef;
my $message;
#param値取得
&setParam;

#cookie取得
if($ENV{'HTTP_COOKIE'}){
	%arrCookie = &getCookie($COOKIENAME);
}
#cookieデータ確認
my $chkcookie = &chkCookie($arrCookie{'id'},$arrCookie{'code'});
if(!$chkcookie){
	if($arrPARM{'mode'} && $arrPARM{'mode'} ne 'login'){
		&logout();
	}
	#ログイン
	&login();
}else{
	&setLogs();
	#モード分岐
	if($arrPARM{'mode'} eq 'edit'){
		#データ入力
		$message = &editData();
		&returnView($message);
	}elsif($arrPARM{'mode'} eq 'form'){
		#入力フォーム表示
		&viewForm();
	}elsif($arrPARM{'mode'} eq 'delete'){
		#削除
		$message = &deleteData();
		&returnView($message);
	}elsif($arrPARM{'mode'} eq 'premain'){
		if($TMPL_MAIN){
			#プレビューメイン
			&viewMain(0);
		}else{
			#プレビュートップ
			print "Location: ./" . $CGINAME . "?mode=pretop\n\n";
			exit;
		}
	}elsif($arrPARM{'mode'} eq 'pretop'){
		#プレビュートップ
		&viewTop($ADJ_COUNT,0);
	}elsif($arrPARM{'mode'} eq 'prelist'){
		#プレビューリスト
		&viewList($TMPL_LIST,$LIST_COUNT,$ADJ_COUNT,0);
	}elsif($arrPARM{'mode'} eq 'predetail'){
		#プレビュー詳細
		&viewDetail($arrPARM{'recno'},0);
	}elsif($arrPARM{'mode'} eq 'make'){
		#ページ生成
		$message = &makePage();
		&returnView($message);
	}elsif($arrPARM{'mode'} eq 'logout'){
		#ログアウト
		&logout();
	}elsif($arrPARM{'mode'} eq 'admin'){
		&putCookie(1,0);
		#一覧表示
		&viewList($TMPL_ADMIN_LIST,$ADMIN_LIST_COUNT,0,0,$message);
	}else{
		#一覧表示に移行
		print "Location: ./" . $CGINAME . "?mode=admin\n\n";
		exit;
	}
	#ページ表示
	&openPage();
}
exit;
#---------------------------------------------------------------------
#デフォルト値
sub initdef{
	if($LIFETIMES <= 0){
		$LIFETIMES=3600;
	}
	if($TOP_COUNT <= 0){
		$TOP_COUNT=5;
	}
	if($LIST_COUNT <= 0){
		$LIST_COUNT=20;
	}
	if($ADMIN_LIST_COUNT <= 0){
		$ADMIN_LIST_COUNT=50;
	}
	if($MAX_COUNT <= 0){
		$MAX_COUNT=100;
	}
	if($TITLE_MAX <= 0){
		$TITLE_MAX = 50;
	}
	if($COMMENT_MAX <= 0){
		$COMMENT_MAX = 1200;
	}
	if($LINK_MAX <= 0){
		$TITLE_MAX = 300;
	}
	if($LINKNAME_MAX <= 0){
		$LINKNAME_MAX = 100;
	}
	our $COOKIESETYEAR = 1900;
	our $COOKIEDELYEAR = 1899;
	our $page = 0;
	our %arrPARM;
	our %arrCookie;
	our @arrLogs;
	our @arrSorts;
}
#---------------------------------------------------------------------
#ログイン処理
sub login{
	my $retcode = 0;
	#param値確認
	if($arrPARM{'account'} && $arrPARM{'passwd'}){
		#IDパスワードチェック
		$retcode = &chkLogin();
	}else{
		if((!$arrPARM{'account'} && $arrPARM{'passwd'}) || ($arrPARM{'account'} && !$arrPARM{'passwd'})){
			#エラー
			$retcode = 0;
		}else{
			#初期ログイン表示
			&setTemplate($TMPL_LOGIN);
			&openPage();
		}
	}
	if(!$retcode){
		#エラーログイン再表示
		&setTemplate($TMPL_LOGIN);
		$template->param(MSGFLG=>'1');
		&openPage();
	}else{
		#cookieセット
		&putCookie(1,4);
		#リスト表示
		print "Location: ./" . $CGINAME  . "?mode=admin\n\n";
		exit;
	}
}
#---------------------------------------------------------------------
#ログインチェック
sub chkLogin{
	my $retcode = 0;
	#ID パスワード
	if($arrPARM{'account'} eq $ID && $arrPARM{'passwd'} eq $PASSWORD){
		$retcode = 1;
	}else{
		$retcode = 0;
	}
	return $retcode;

}
#---------------------------------------------------------------------
#cookie取得
sub getCookie{
	my($cookie_name) = @_;
	my(%COOKIE,$http_cookie,@pairs,$pair,$name,$value,%cookie_data);
	$http_cookie = $ENV{'HTTP_COOKIE'};
	@pairs = split(/;/, $http_cookie);
	foreach $pair (@pairs) {
		($name, $value) = split(/=/, $pair);
		$name =~ s/ //g;
		$cookie_data{$name} = $value;
	}
	@pairs = split(/\,/,$cookie_data{$cookie_name});
	foreach $pair (@pairs) {
		($name, $value) = split(/\:/, $pair);
		$COOKIE{$name} = $value;
	}
	return %COOKIE;
}
#---------------------------------------------------------------------
#暗号化
sub cipher{ 
	my ($val) = @_;
	my ($sec, $min, $hour, $day, $mon, $year, $weekday) = localtime(time);
	my(@token) = ('0'..'9', 'A'..'Z', 'a'..'z');
	my $salt;
	$salt = $token[(time | $$) % scalar(@token)];
	$salt .= $token[($sec + $min*60 + $hour*60*60) % scalar(@token)];
	return crypt($val, $salt);
} 
#---------------------------------------------------------------------
#cookieセット
sub putCookie{
	my ($set,$msg) = @_;
	my $cookie_data = "";
	my $setyear;
	if($set){
		#更新
		my ($cookie_data_name,$idsalt) = &cipher($ID);
		my ($cookie_data_password,$passsalt) = &cipher($PASSWORD);
		$cookie_data = "id:" . $cookie_data_name . ",code:" . $cookie_data_password .",m:" . $msg;
		$setyear = $COOKIESETYEAR;
	}else{
		#削除
		$setyear = $COOKIEDELYEAR;
	}

	my($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst,$month,$youbi,$date_gmt);
	$ENV{'TZ'} = "GMT";
	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time + $LIFETIMES);
	$month = ('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec')[$mon];
	$youbi = ('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday')[$wday];
	$date_gmt = sprintf("%s\, %02d\-%s\-%04d %02d:%02d:%02d GMT",$youbi,$mday,$month,$year + $setyear,$hour,$min,$sec);
	print "Set-Cookie:" . $COOKIENAME . "=" . $cookie_data . "; expires=" . $date_gmt . ";\n";
}
#---------------------------------------------------------------------
#暗号化文字列チェック
sub decipher{
	my ($passwd1,$passwd2) = @_;
	if(!$passwd1 || !$passwd2){
		return 0;
	}
	if(crypt($passwd1,$passwd2) eq $passwd2){
		return 1;
	}else{
		return 0;
	} 
} 
#---------------------------------------------------------------------
#cookieデータチェック
sub chkCookie{
	my ($cookieid,$cookiepass) = @_;
	#ID
	my $chkid = decipher($ID,$cookieid);
	#パスワード
	my $chkpass = decipher($PASSWORD,$cookiepass);
	if(!$chkid || !$chkpass){
		return 0;
	}else{
		return 1;
	}
}
#---------------------------------------------------------------------
#ログアウト
sub logout{
	#cookie削除
	&putCookie(0,0);

	#ログイン画面表示
	print "Location: ./" . $CGINAME . "\n\n";
	exit;
}
#---------------------------------------------------------------------
#管理画面に戻る
sub returnView{
	my ($message) = @_;
	&putCookie(1,$message);
	print "Location: ./" . $CGINAME  . "?mode=admin\n\n";
	exit;
}
#---------------------------------------------------------------------
#メイン表示
sub viewMain{
	my ($viewflg) = @_;
	my $listlink;
	#テンプレート設定
	&setTemplate($TMPL_MAIN);
	#変数設定
	if($viewflg == 0){
		$listlink =  $CGI_DIR . $CGINAME . "?mode=prelist";
	}
	$template->param(VIEW_FLG=>$viewflg);
	$template->param(LINKLIST=>$listlink);
}
#---------------------------------------------------------------------
#トップ表示
sub viewTop{
	my ($adjcountflg,$viewflg) = @_;
	$page = 0;
	my $listlink;
	#ログ読み込み
	my @arrList = &getLog($TOP_COUNT,$adjcountflg,$viewflg);
	#テンプレート設定
	&setTemplate($TMPL_TOP);
	#変数設定
	$template->param(TBL=>\@arrList);
	if($viewflg == 0){
		$listlink = $CGI_DIR . $CGINAME . "?mode=prelist";
	}else{
		$listlink = $HTML_LIST_DIR . $HTML_LIST_NAME . $HTML_LIST_EXT;
	}
	$template->param(VIEW_FLG=>$viewflg);
	$template->param(LINKLIST=>$listlink);
}
#---------------------------------------------------------------------
#一覧表示
sub viewList{
	my ($template_file,$listcount,$adjcountflg,$viewflg,$message) = @_;
	#ログ読み込み
	my @arrList = &getLog($listcount,$adjcountflg,$viewflg);

	#ページング
	my @arrPaging = &setPage($viewflg);

	#メッセージ
	$message = $MSGLIST[$arrCookie{'m'}];
	Jcode::convert(\$message,'sjis','euc');
	#テンプレート設定
	&setTemplate($template_file);
	#変数設定
	$template->param(TBL=>\@arrList);
	$template->param(MSG=>$message);
	$template->param(VIEW_FLG=>$viewflg);
	$template->param(PAGING=>\@arrPaging);
	#現在ページ
	if($page > 0){
		my $pagenum = $page +1;
		Jcode::convert(\$pagenum,'sjis','euc');
		$template->param(PAGENUM=>$pagenum);
	}
}
#---------------------------------------------------------------------
#ログ取得
sub getLog{
	my ($count,$adjcountflg,$viewflg) = @_;
	my $i = 0;
	my $mode;
	my @data;
	my @arrList;
	$countstart = $page * $count;
	$countend = $countstart + $count;

	if($countstart > $loglength){
		#エラー
		my $errormsg = $ERRMSGLIST[2];
		&Print_Error($errormsg);
	}
	if($countend > $loglength){
		#ページ最終レコードカウントがログ超えてたら
		$countend = $loglength;
	}

	if($adjcountflg){
		#カウント分表示
		$countlast = $countstart + $count;
	}else{
		#最小限表示
		$countlast = $countend;
	}

	#ログ格納
	for($i=$countstart;$i<$countlast;$i++){
		my %tmp;
		chomp ($arrSorts[$i]);
		$arrSorts[$i] =~ s/(?:\x0D\x0A|[\x0D\x0A])?$/,/;
		@data = map {/^"(.*)"$/ ? scalar($_ = $1, s/""/"/g, $_) : $_}($arrSorts[$i] =~ /("[^"]*(?:""[^"]*)*"|[^,]*),/g);
		#"
		if($data[1]){
			my $recno = $i +1;
			$tmp{'RECNO'} = $data[0] +1;

			#記事NO
			$tmp{'ITEMNO'} = $data[1];
			$tmp{'DATE_YEAR'} = $data[2];
			$tmp{'DATE_MON'} = $data[3];
			$tmp{'DATE_DAY'} = $data[4];
			if($data[2] && $data[3] && $data[4]){
				#曜日取得
				my $wday = &getWday($data[2],$data[3],$data[4]);
				$tmp{'DATE_WEEK'} = $wday;
			}else{
				$tmp{'DATE_WEEK'} = 0;
			}
			$tmp{'CATEGORYNO'} = $data[5];
			#カテゴリ名
			if($data[5] > 0){
				$tmp{'CATEGORYNAME'} = $CATEGORY[$data[5]-1];
			}
			$tmp{'MEMO'} = $data[6];
			$tmp{'TITLE'} = $data[7];
			$tmp{'TITLE_S'} = &trim($data[7],$SHORT_TITLE,$REP_CHAR);
			$tmp{'TITLE_E'} = $tmp{'TITLE_S'};
			$tmp{'TITLE_E'} =~ s/\\/\\\\/g;
			if($data[8]){
				#コメントあり
				$tmp{'SETCOMMENT_FLG'} = 1;
			}else{
				$tmp{'SETCOMMENT_FLG'} = 0;
			}
			if($data[9]){
				#リンクあり
				$tmp{'SETLINK_FLG'} = 1;
			}else{
				$tmp{'SETLINK_FLG'} = 0;
			}
			if($data[13]){
				#画像あり
				$tmp{'IMAGENUM'} = $data[12];
				$tmp{'SETIMG_FLG'} = 1;
			}else{
				$tmp{'SETIMG_FLG'} = 0;
			}
			if($viewflg == 0){
				#cgi用リンク
				if($arrPARM{'mode'} eq 'pretop' || $arrPARM{'mode'} eq 'prelist'){
					if($tmp{'SETCOMMENT_FLG'}){
						#コメントありの場合
						$tmp{'LINKDETAIL'} = $CGI_DIR . $CGINAME . "?mode=predetail&recno=" . $recno;
					}elsif($tmp{'SETLINK_FLG'}){
						#リンクありの場合
						$tmp{'LINKDETAIL'} = $data[9];
						if($data[11]){
							#新ウィンドウ
							$tmp{'NEWLINK_FLG'} = 1;
						}
					}
				}else{
					#入力フォーム
					$tmp{'LINKDETAIL'} = $CGI_DIR . $CGINAME . "?mode=form&recno=" . $recno;
				}
			}else{
				#html出力用リンク
				if($tmp{'SETCOMMENT_FLG'}){
					$tmp{'LINKDETAIL'} = $HTML_DETAIL_DIR . $HTML_DETAIL_NAME . $data[1] . $HTML_DETAIL_EXT;
				}elsif($tmp{'SETLINK_FLG'}){
					#リンクありの場合
					$tmp{'LINKDETAIL'} = $data[9];
					if($data[11]){
						#新ウィンドウ
						$tmp{'NEWLINK_FLG'} = 1;
					}
				}
			}
			if($data[15]){
				#最終更新日付取得
				my ($year,$mon,$day,$week,$hour,$min,$sec) = &convDate($data[15]);
				$tmp{'UP_YEAR'} = $year;
				$tmp{'UP_MON'} = $mon;
				$tmp{'UP_DAY'} = $day;
				$tmp{'UP_WEEK'} = $week;
				$tmp{'UP_HOUR'} = $hour;
				$tmp{'UP_MIN'} = $min;
				$tmp{'UP_SEC'} = $sec;

				my $newtimes = time - $NEWDATE * 60 * 60 * 24;
				if($data[15] > $newtimes){
					#新しい期間フラグ
					$tmp{'NEWDATE_FLG'} = 1;
				}else{
					$tmp{'NEWDATE_FLG'} = 0;
				}
			}

			#sjis変換
			foreach(%tmp){
				Jcode::convert(\$_,'sjis','euc');
			}
		}
		push(@arrList,\%tmp); 
	}
	return @arrList;
}
#---------------------------------------------------------------------
#ページング設定
sub setPage{
	my ($viewflg) = @_;

	my $pagenum = $page +1;
	my $pageprev = $pagenum -1;
	my $pagenext = $pagenum +1;
	my $mode;
	my $pagelinkcount;
	my @arrPaging;
	my %tmp;
	my @arrPagelist;
	my %tmp2;

	if($arrPARM{'mode'} eq 'prelist'){
		#プレビューモードの場合
		$mode = "mode=prelist";
		$pagelinkcount = $LIST_COUNT;
	}elsif($arrPARM{'mode'} eq 'admin'){
		#管理リストモードの場合
		$mode = "mode=admin";
		$pagelinkcount = $ADMIN_LIST_COUNT;
	}else{
		$pagelinkcount = $LIST_COUNT;
	}
	
	#ページ数取得
	my $pagetotal = int($loglength / $pagelinkcount);
	if($loglength % $pagelinkcount > 0){
		$pagetotal++;
	}
	if($pagetotal <= 0){
		$pagetotal = 1;
	}

	#前ページURL,最初ページURL
	if($countstart > 0){
		#前ページフラグon
		$tmp{'PREV_FLG'} = 1;
		if($viewflg == 0){
			#cgi用リンク
			$tmp{'PREV'} = $CGI_DIR . $CGINAME . "?" . $mode . "&page=" . $pageprev;
			$tmp{'FAST'} = $CGI_DIR . $CGINAME . "?" . $mode;
		}else{
			#html出力用リンク
			if($pageprev > 1){
				#1ページ目はページ数なし
				$tmp{'PREV'} = $HTML_LIST_DIR . $HTML_LIST_NAME . $pageprev . $HTML_LIST_EXT;
			}else{
				$tmp{'PREV'} = $HTML_LIST_DIR . $HTML_LIST_NAME . $HTML_LIST_EXT;
			}
			$tmp{'FAST'} = $HTML_LIST_DIR . $HTML_LIST_NAME . $HTML_LIST_EXT;
		}
	}

	#全ページリンクリスト
	for(my $i=1;$i<=$pagetotal;$i++){
		my %tmp2;
		$tmp2{'PAGECOUNT'} = $i;
		if($i == $page+1){
			#現在ページの場合
			$tmp2{'PAGELINK'} = 0;
		}else{
			if($viewflg == 0){
				#cgi用リンク
				$tmp2{'PAGELINK'} = $CGINAME . '?' .$mode . '&page=' . $i;
			}else{
				#html出力用リンク
				if($i > 1){
					$tmp2{'PAGELINK'} = $HTML_LIST_DIR . $HTML_LIST_NAME . $i . $HTML_LIST_EXT;
				}else{
					$tmp2{'PAGELINK'} = $HTML_LIST_DIR . $HTML_LIST_NAME . $HTML_LIST_EXT;
				}
			}
		}
		push(@arrPagelist,\%tmp2);
	}
	push(@{$tmp{'PAGELIST'}},@arrPagelist);

	#次ページURL,最終ページURL
	if($countend < $loglength){
		#次ページフラグon
		$tmp{'NEXT_FLG'} = 1;
		if($viewflg == 0){
			#cgi用リンク
			$tmp{'NEXT'} = $CGI_DIR . $CGINAME . "?" .$mode . "&page=" . $pagenext;
			$tmp{'LAST'} = $CGI_DIR . $CGINAME . "?" . $mode . "&page=" . $pagetotal;
		}else{
			#html出力用リンク
			$tmp{'NEXT'} = $HTML_LIST_DIR . $HTML_LIST_NAME . $pagenext . $HTML_LIST_EXT;
			$tmp{'LAST'} = $HTML_LIST_DIR . $HTML_LIST_NAME . $pagetotal . $HTML_LIST_EXT;
		}
	}

	push(@arrPaging,\%tmp);
	return @arrPaging;
}
#---------------------------------------------------------------------
#詳細表示
sub viewDetail{
	my ($recno,$viewflg) = @_;
	#ログ読み込み
	my @arrDetail = &getDetail($recno,$viewflg);
	#画像データ取得
	my @images = &getImageData($arrDetail[0]{'IMAGENO'},$arrDetail[0]{'IMAGEEXT'},$arrDetail[0]{'IMAGENUM'},$arrDetail[0]{'IMAGEDIR'});
	my @imagen = split(/:/,$arrDetail[0]{'IMAGENUM'});
	my $imagelen = @imagen;
	#画像表示分岐
	if($IMG_FLOAT == 1 && $imagelen == 1){
		#1枚表示
		push(@{$arrDetail[0]{'IMAGEO'}},@images);
	}elsif($imagelen > 0){
		#羅列表示
		push(@{$arrDetail[0]{'IMAGES'}},@images);
	}

	#テンプレート設定
	&setTemplate($TMPL_DETAIL);
	#変数設定
	$template->param(VIEW_FLG=>$viewflg);
	$template->param(DTL=>\@arrDetail);

	return $arrDetail[0]{'ITEMNO'};
}

#---------------------------------------------------------------------
#詳細取得
sub getDetail{
	my ($recno,$viewflg) = @_;
	$recno--;
	my $imgdir;
	my @arrDetail;
	my @data;

	if($viewflg == 1){
		$imgdir = $IMG_DIR;
	}else{
		$imgdir = $IMG_TMP_DIR2;
	}

	if($recno >= $loglength || $recno < 0){
		#エラー
		my $errormsg = $ERRMSGLIST[1];
		&Print_Error($errormsg);
	}
	#ログ格納
	my %tmp;

	chomp ($arrSorts[$recno]);
	$arrSorts[$recno] =~ s/(?:\x0D\x0A|[\x0D\x0A])?$/,/;
	@data = map {/^"(.*)"$/ ? scalar($_ = $1, s/""/"/g, $_) : $_}($arrSorts[$recno] =~ /("[^"]*(?:""[^"]*)*"|[^,]*),/g);
	#"
	$tmp{'RECNO'} = $data[0] + 1;

	#記事NO
	$tmp{'ITEMNO'} = $data[1];
	$tmp{'DATE_YEAR'} = $data[2];
	$tmp{'DATE_MON'} = $data[3];
	$tmp{'DATE_DAY'} = $data[4];
	if($data[2] && $data[3] && $data[4]){
		#曜日取得
		my $wday = &getWday($data[2],$data[3],$data[4]);
		$tmp{'DATE_WEEK'} = $wday;
	}else{
		$tmp{'DATE_WEEK'} = 0;
	}

	$tmp{'CATEGORYNO'} = $data[5];
	#カテゴリ名
	if($data[5] > 0){
		$tmp{'CATEGORYNAME'} = $CATEGORY[$data[5]-1];
	}
	$tmp{'MEMO'} = $data[6];
	$tmp{'TITLE'} = $data[7];
	if($AUTOLINK == 0 or $viewflg == 2){
		$tmp{'COMMENT'} = $data[8];
	}else{
		$tmp{'COMMENT'} = &autolink($data[8]);
	}
	$tmp{'LINK'} = $data[9];
	if($data[10]){
		#リンクタイトルある時
		$tmp{'LINKNAME'} = $data[10];
		$tmp{'LINKNAME_S'} = $data[10];
	}else{
		#リンクタイトルない時リンクURL（長い場合省略）
		$data[7] =~ s/&amp;/&/g;
		$tmp{'LINKNAME_S'} = &trim($data[9],$SHORT_LINKNAME,$REP_CHAR);
	}
	$tmp{'NEWLINK_FLG'} = $data[11];
	if($data[13]){
		#画像ある場合
		$tmp{'SETIMG_FLG'} = 1;
		$tmp{'IMAGEDIR'} = $imgdir;
		$tmp{'IMAGENO'} = $data[1];
		$tmp{'IMAGEEXT'} = $data[13];
		$tmp{'IMAGENUM'} = $data[12];
	}else{
		if($IMG_DEF){
			#デフォルト画像設定の場合
			$tmp{'SETIMGE_FLG'} = 1;
			$tmp{'IMAGE'} = $imgdir . $IMG_DEF;
		}else{
			$tmp{'SETIMGE_FLG'} = 0;
		}
	}
	#最終更新日付取得
	my ($year,$mon,$day,$week,$hour,$min,$sec) = &convDate($data[15]);
	$tmp{'UP_YEAR'} = $year;
	$tmp{'UP_MON'} = $mon;
	$tmp{'UP_DAY'} = $day;
	$tmp{'UP_WEEK'} = $week;
	$tmp{'UP_HOUR'} = $hour;
	$tmp{'UP_MIN'} = $min;
	$tmp{'UP_SEC'} = $sec;

	my $newtimes = time - $NEWDATE * 60 * 60 * 24;
	if($data[15] > $newtimes){
		#新着期間フラグ
		$tmp{'NEWDATE_FLG'} = 1;
	}else{
		$tmp{'NEWDATE_FLG'} = 0;
	}

	#sjis変換
	foreach(%tmp){
		Jcode::convert(\$_,'sjis','euc');
	}
	
	push(@arrDetail,\%tmp); 

	return @arrDetail;
}
#---------------------------------------------------------------------
#入力フォーム表示
sub viewForm{
	my (@errors) = @_;
	my $modeflg;
	my @arrDetail;
	my %tmp;
	my @imagedata;
	my @imageforms;

	&setTemplate($TMPL_FORM);

	#変数設定
	if(@errors){
		#エラー
		$template->param(ERR=>\@errors);
		#入力値格納
		@arrDetail = &remakeInput();
		$template->param(DTL=>\@arrDetail);
		if($arrDetail[0]{'RECNO'}){
			$modflg = 0;
		}else{
			$modeflg = 1;
		}
	}elsif(exists($arrPARM{'recno'})){
		#編集
		#データ取得
		@arrDetail = &getDetail($arrPARM{'recno'},2);

		#改行コード→<br />
		$arrDetail[0]{'COMMENT'} =~ s/<br \/>/\n/g;

		my($html_sel_year,$html_sel_mon,$html_sel_day) = &makeSelectdate($arrDetail[0]{'DATE_YEAR'},$arrDetail[0]{'DATE_MON'},$arrDetail[0]{'DATE_DAY'});
		my($html_sel_category) = &makeSelectcategory($arrDetail[0]{'CATEGORYNO'});
		Jcode::convert(\$html_sel_year,'sjis','euc');
		Jcode::convert(\$html_sel_mon,'sjis','euc');
		Jcode::convert(\$html_sel_day,'sjis','euc');
		$arrDetail[0]{'SELECT_YEAR'} = $html_sel_year;
		$arrDetail[0]{'SELECT_MON'} = $html_sel_mon;
		$arrDetail[0]{'SELECT_DAY'} = $html_sel_day;
		if($html_sel_category){
			Jcode::convert(\$html_sel_category,'sjis','euc');
			$arrDetail[0]{'SELECT_CATEGORY'} = $html_sel_category;
			$arrDetail[0]{'CATEGORY'} = 1;
		}else{
			$arrDetail[0]{'CATEGORY'} = 0;
		}
		#画像フォーム
		@imageforms = &getImageData($arrDetail[0]{'IMAGENO'},$arrDetail[0]{'IMAGEEXT'},$arrDetail[0]{'IMAGENUM'},$arrDetail[0]{'IMAGEDIR'});
		$template->param(IMAGE_FORM=>\@imageforms);

		$template->param(DTL=>\@arrDetail);
		$modflg = 0;
	}else{
		#新規
		push(@arrDetail,\%tmp);
		my($year,$mon,$day) = &convDate(time);
		my($html_sel_year,$html_sel_mon,$html_sel_day) = &makeSelectdate($year,$mon,$day);
		my($html_sel_category) = &makeSelectcategory();
		Jcode::convert(\$html_sel_year,'sjis','euc');
		Jcode::convert(\$html_sel_mon,'sjis','euc');
		Jcode::convert(\$html_sel_day,'sjis','euc');
		$arrDetail[0]{'SELECT_YEAR'} = $html_sel_year;
		$arrDetail[0]{'SELECT_MON'} = $html_sel_mon;
		$arrDetail[0]{'SELECT_DAY'} = $html_sel_day;
		if($html_sel_category){
			Jcode::convert(\$html_sel_category,'sjis','euc');
			$arrDetail[0]{'SELECT_CATEGORY'} = $html_sel_category;
			$arrDetail[0]{'CATEGORY'} = 1;
		}else{
			$arrDetail[0]{'CATEGORY'} = 0;
		}

		#画像フォーム
		@imageforms = &getImageData($arrDetail[0]{'IMAGENO'},$arrDetail[0]{'IMAGEEXT'},$arrDetail[0]{'IMAGENUM'},$arrDetail[0]{'IMAGEDIR'});
		$template->param(IMAGE_FORM=>\@imageforms);

		$template->param(DTL=>\@arrDetail);
		$modeflg = 1;
	}
	$template->param(NEW=>$modeflg);
}
#---------------------------------------------------------------------
#日付selectoption生成
sub makeSelectdate{
	my ($year,$mon,$day) = @_;

	my $nyear = (localtime(time))[5];
	$nyear = $nyear + 1900;

	#年セレクトフォーム
	my $start_year = $nyear - $SELECT_YEAR_PAST;
	my $end_year = $nyear + $SELECT_YEAR_FUTURE;
	my $html_sel_year;
	for(my $i=$start_year;$i<=$end_year;$i++){
		$html_sel_year .= '<option value="' . $i . '"';
		if($i eq $year){
			$html_sel_year .= ' selected';
		}
		$html_sel_year .='>' . $i . '</option>';
	}
	#月セレクトフォーム
	my $start_mon = '1';
	my $end_mon = '12';
	my $html_sel_mon;
	for(my $i=$start_mon;$i<=$end_mon;$i++){
		my $option_mon = sprintf("%02d",$i);
		$html_sel_mon .= '<option value="' . $option_mon . '"';
		if($option_mon eq $mon){
			$html_sel_mon .= ' selected';
		}
		$html_sel_mon .='>' . $option_mon . '</option>';
	}
	#日セレクトフォーム
	my $start_day = '1';
	my $end_day = '31';
	my $html_sel_day;
	for(my $i=$start_day;$i<=$end_day;$i++){
		my $option_day = sprintf("%02d",$i);
		$html_sel_day .= '<option value="' . $option_day . '"';
		if($option_day eq $day){
			$html_sel_day .= ' selected';
		}
		$html_sel_day .='>' . $option_day . '</option>';
	}
	return ($html_sel_year,$html_sel_mon,$html_sel_day);
}
#---------------------------------------------------------------------
#カテゴリselectoption生成
sub makeSelectcategory{
	my ($cate) = @_;
	my $html_sel_category;
	my $length = @CATEGORY;
	my $i = 1;
	if($length > 0){
		#カテゴリ必須じゃない場合
		if(!$CATEGORY_REQUIRED){
			$html_sel_category .='<option value="0"';
			if($cate eq 0){
				$html_sel_category .= ' selected';
			}
			$html_sel_category .='>' . $CATEGORY_NONE . '</option>';
		}
		foreach my $categoryname (@CATEGORY){
			$html_sel_category .='<option value="' . $i . '"';
			if($i eq $cate){
				$html_sel_category .= ' selected';
			}
			$html_sel_category .='>' . $categoryname . '</option>';
			$i++;
		}
	}
	return $html_sel_category;
}
#---------------------------------------------------------------------
#入力
sub editData{
	#入力チェック
	my ($errorflg,@errors) = &chkInput();
	if($errorflg){
		#エラーだったらフォーム再表示
		&viewForm(@errors);
		&openPage();
	}else{
		#入力処理
		&inputData();
		#入力完了
		my $message = 1;

		#ログ再読み込み
		&setLogs;
		return $message;
	}
}
#---------------------------------------------------------------------
#入力値再格納
sub remakeInput{
	my $errorflg = 0;
	my %indata;
	my @retdata;

	my($html_sel_year,$html_sel_mon,$html_sel_day) = &makeSelectdate($arrPARM{'sel_year'},$arrPARM{'sel_mon'},$arrPARM{'sel_day'});
	$indata{'SELECT_YEAR'} = $html_sel_year;
	$indata{'SELECT_MON'} = $html_sel_mon;
	$indata{'SELECT_DAY'} = $html_sel_day;
	
	my($html_sel_category) = &makeSelectcategory($arrPARM{'sel_category'});
	if($html_sel_category){
		$indata{'SELECT_CATEGORY'} = $html_sel_category;
		$indata{'CATEGORY'} = 1;
	}else{
		$indata{'CATEGORY'} = 0;
	}

	$indata{'MEMO'} = $arrPARM{'memo'};

	$indata{'TITLE'} = $arrPARM{'title'};

	$indata{'COMMENT'} = $arrPARM{'comment'};
	#改行コード→<br />
	$indata{'COMMENT'} =~ s/<br \/>/\n/g;

	$indata{'LINK'} = $arrPARM{'link'};

	$indata{'LINKNAME'} = $arrPARM{'linkname'};

	$indata{'NEWLINK_FLG'} = $arrPARM{'linkflg'};
	#画像フォーム
	@imageforms = &getImageData($arrPARM{'itemno'},$arrPARM{'ext'},$arrPARM{'imagenum'},$IMG_TMP_DIR2,$arrPARM{'delimagenum'});
	$template->param(IMAGE_FORM=>\@imageforms);

	$indata{'RECNO'} = $arrPARM{'recno'};
	$indata{'ITEMNO'} = $arrPARM{'itemno'};
	$indata{'IMAGEEXT'} = $arrPARM{'ext'};

	#sjis変換
	foreach(%indata){
		Jcode::convert(\$_,'sjis','euc');
	}

	push(@retdata,\%indata);
	return (@retdata);
}

#---------------------------------------------------------------------
#入力チェック
sub chkInput{
	my $errorflg = 0;
	my %errormsg;
	my @errors;

	#日付
	my $daycheck = &dayCheck($arrPARM{'sel_year'},$arrPARM{'sel_mon'},$arrPARM{'sel_day'});
	if(!$daycheck){
		#エラー
		my %errormsg;
		$errorflg = 1;
		$errormsg{'ERRMSG'} = $ERRMSG_DATE;
		Jcode::convert(\$errormsg{'ERRMSG'},'sjis','euc');
		push(@errors,\%errormsg);
	}
	#title
	if($arrPARM{'title'} eq "" || length($arrPARM{'title'}) > $TITLE_MAX * 2){
		#エラー
		my %errormsg;
		$errorflg = 1;
		$errormsg{'ERRMSG'} = $ERRMSG_TITLE;
		Jcode::convert(\$errormsg{'ERRMSG'},'sjis','euc');
		push(@errors,\%errormsg);
	}

	#カテゴリ
	if($CATEGORY_REQUIRED){
		if($arrPARM{'sel_category'} eq "" or $arrPARM{'sel_category'} eq 0){
			#エラー
			my %errormsg;
			$errorflg = 1;
			$errormsg{'ERRMSG'} = $ERRMSG_CATEGORY;
			Jcode::convert(\$errormsg{'ERRMSG'},'sjis','euc');
			push(@errors,\%errormsg);
		}
	}
	my $length = @CATEGORY;
	if($length > 0){
		if($arrPARM{'sel_category'} > $length){
			#エラー
			my %errormsg;
			$errorflg = 1;
			$errormsg{'ERRMSG'} = $ERRMSG_CATEGORY . $length;
			Jcode::convert(\$errormsg{'ERRMSG'},'sjis','euc');
			push(@errors,\%errormsg);
		}
	}

	#メモ
	if($MEMO_REQUIRED){
		if($arrPARM{'memo'} eq ""){
			#エラー
			my %errormsg;
			$errorflg = 1;
			$errormsg{'ERRMSG'} = $ERRMSG_MEMO;
			Jcode::convert(\$errormsg{'ERRMSG'},'sjis','euc');
			push(@errors,\%errormsg);
		}
	}
	if(length($arrPARM{'memo'}) > $MEMO_MAX * 2){
		#エラー
		my %errormsg;
		$errorflg = 1;
		$errormsg{'ERRMSG'} = $ERRMSG_MEMO;
		Jcode::convert(\$errormsg{'ERRMSG'},'sjis','euc');
		push(@errors,\%errormsg);
	}

	#comment
	if(length($arrPARM{'comment'}) > $COMMENT_MAX * 2){
		#エラー
		my %errormsg;
		$errorflg = 1;
		$errormsg{'ERRMSG'} = $ERRMSG_COMMENT;
		Jcode::convert(\$errormsg{'ERRMSG'},'sjis','euc');
		push(@errors,\%errormsg);
	}

	#link
	if($arrPARM{'linkname'} ne ""){
		if($arrPARM{'link'} eq ""){
			#エラー
			my %errormsg;
			$errorflg = 1;
			$errormsg{'ERRMSG'} = $ERRMSG_LINK1;
			Jcode::convert(\$errormsg{'ERRMSG'},'sjis','euc');
			push(@errors,\%errormsg);
		}
	}
	if($arrPARM{'link'} ne ""){
		if($arrPARM{"link"}!~/s?https?:\/\/[-_.!~*'()a-zA-Z0-9;\/?:\@&=+\$,%#]+/g || length($arrPARM{'link'}) > $LINK_MAX){
			#'エラー
			my %errormsg;
			$errorflg = 1;
			$errormsg{'ERRMSG'} = $ERRMSG_LINK2;
			Jcode::convert(\$errormsg{'ERRMSG'},'sjis','euc');
			push(@errors,\%errormsg);
		}
	}

	#linkname
	if(length($arrPARM{'linkname'}) > $LINKNAME_MAX * 2){
		#エラー
		my %errormsg;
		$errorflg = 1;
		$errormsg{'ERRMSG'} = $ERRMSG_LINKNAME;
		Jcode::convert(\$errormsg{'ERRMSG'},'sjis','euc');
		push(@errors,\%errormsg);
	}

	my $paramimage;
	my $paramimageext;
	my $paramtype;
	my $imageextc;
	my @imagen;
	my $imagelen;
	my @imagen2;
	my $imagelen2;
	my @imagen3;
	my $imagelen3;


	#upimage
	if($IMG_COUNT > 0){
		#画像アップロードある場合
		if($arrPARM{'upimagenum'}){
			$paramimage;
			$paramimageext;
			$paramtype;
			@imagen = split(/:/,$arrPARM{'upimagenum'});
			$imagelen = @imagen;
			
			if($arrPARM{'ext'} ne ".jpg" &&$arrPARM{'ext'} ne ".gif" && $arrPARM{'ext'} ne ".png" && $arrPARM{'ext'} ne ""){
				my %errormsg;
				$errorflg = 1;
				$errormsg{'ERRMSG'} = $ERRMSG_IMAGEEXT2;
				Jcode::convert(\$errormsg{'ERRMSG'},'sjis','euc');
				push(@errors,\%errormsg);

				$imageerrorflg = 1;
			}else{
				$imageextc = $arrPARM{'ext'};
			}

			for(my $i=0;$i<$imagelen;$i++){
				$paramimage = "image_" . $imagen[$i];
				$paramimageext = "imageext_" . $imagen[$i];
				$paramtype = "type_" . $imagen[$i];

				if($arrPARM{$paramimage} && $imageerrorflg == 0){
					#拡張子
					my ($name, $path, $suffix) = fileparse($arrPARM{$paramimage},@IMG_SUFFIX_LIST);
					if(!$suffix){
						#エラー
						my %errormsg;
						$errorflg = 1;
						$errormsg{'ERRMSG'} = $ERRMSG_IMAGEEXT;
						Jcode::convert(\$errormsg{'ERRMSG'},'sjis','euc');
						push(@errors,\%errormsg);

						$imageerrorflg = 1;
					}else{
						$arrPARM{$paramimageext} = 0;
						#Content-Type
						if($arrPARM{$paramtype} =~ /image\/p?jpeg/i){
							$arrPARM{$paramimageext} = '.jpg';
							$arrPARM{'imageext'} = '.jpg';
						}elsif($arrPARM{$paramtype} =~ /image\/gif/i){
							 $arrPARM{$paramimageext} = '.gif';
							 $arrPARM{'imageext'} = '.gif';
						}elsif ($arrPARM{$paramtype} =~ /image\/(x\-)?png/i){
							$arrPARM{$paramimageext} = '.png';
							$arrPARM{'imageext'} = '.png';
						}else{
							#エラー
							my %errormsg;
							$errorflg = 1;
							$errormsg{'ERRMSG'} = $ERRMSG_IMAGETYPE;
							Jcode::convert(\$errormsg{'ERRMSG'},'sjis','euc');
							push(@errors,\%errormsg);

							$imageerrorflg = 1;
						}
						if($imageextc ne ""){
							#拡張子チェック
							if($arrPARM{'imageext'} ne $imageextc){
								my %errormsg;
								$errorflg = 1;
								$errormsg{'ERRMSG'} = $ERRMSG_IMAGEEXT2;
								Jcode::convert(\$errormsg{'ERRMSG'},'sjis','euc');
								push(@errors,\%errormsg);

								$imageerrorflg = 1;
							}
						}else{
							$imageextc = $arrPARM{'imageext'};
						}

						if($arrPARM{$paramimageext}&& $IMG_SIZE_MAX > 0){
							#サイズ
							my $size = (stat($arrPARM{$paramimage}))[7];
							if($size > $IMG_SIZE_MAX){
								#エラー
								$errorflg = 1;
								$errormsg{'ERRMSG'} = $ERRMSG_IMAGESIZE;
								Jcode::convert(\$errormsg{'ERRMSG'},'sjis','euc');
								push(@errors,\%errormsg);

								$imageerrorflg = 1;
							}
						}
					}
				}
			}
		}
		#imagenum
		if($arrPARM{'imagenum'}){
			#既存imageno取得
			@imagen2 = split(/:/,$arrPARM{'imagenum'});
			if($arrPARM{'delimagenum'}){
				#削除分削除
				@imagen3 = split(/:/,$arrPARM{'delimagenum'});
				$imagelen3 = @imagen3;
				foreach my $value3 (@imagen3){
					@imagen2 = &delArray(\@imagen2, $value3);
				}
			}
			$imagelen2 = @imagen2;
			#追加分マージ
			push(@imagen2,@imagen);
			my %c;
			#重複削除
			@imagen2 = grep {!$c{$_}++} @imagen2;
				foreach my $value2 (@imagen2){
				$arrPARM{'imagenums'} .= $value2 .":";
			}
		}else{
			#既存ない場合
			if($arrPARM{'upimagenum'}){
				#アップロードある場合
				$arrPARM{'imagenums'} = $arrPARM{'upimagenum'};
			}else{
				$arrPARM{'imagenums'} = "";
			}
		}
		if($arrPARM{'imagenums'}){
			if($arrPARM{'imagenums'} !~ /^[0-9:]+$/i){
				#エラー
				$errorflg = 1;
				$errormsg{'ERRMSG'} = $ERRMSG_DEFAULT;
				Jcode::convert(\$errormsg{'ERRMSG'},'sjis','euc');
				push(@errors,\%errormsg);
			}
		}
	}
	return ($errorflg,@errors);
}
#---------------------------------------------------------------------
#配列要素削除
sub delArray {
	my($array,$v) = @_;
	my @new = ();
	foreach (@{$array}) {
		next if($v eq $_);
		push(@new,$_);
	}
	return @new;
}
#---------------------------------------------------------------------
#データ入力
sub inputData{
	my $imageflg;
	my $line;
	my $itemno;
	my $times;
	my $utimes;
	my $oldfile;
	my @param;
	my @data;
	my $delfile;
	#入力データ
	my $recno = $arrPARM{'recno'};
	$recno--;
	
	#年月日
	push(@param,$arrPARM{'sel_year'});
	push(@param,$arrPARM{'sel_mon'});
	push(@param,$arrPARM{'sel_day'});
	#カテゴリ
	push(@param,$arrPARM{'sel_category'});
	#メモ
	push(@param,$arrPARM{'memo'});
	#タイトル
	push(@param,$arrPARM{'title'});
	#コメント
	push(@param,$arrPARM{'comment'});
	#リンク
	push(@param,$arrPARM{'link'});
	#リンク名
	push(@param,$arrPARM{'linkname'});
	#リンクフラグ
	if($arrPARM{'linkflg'}){
		push(@param,'1');
	}else{
		push(@param,'0');
	}
	#logに書き込み
	if($recno > $loglength || $recno < 0){
		#新規
		#記事No
		$itemno = $maxitemno +1;
		unshift(@param,$itemno);
		#画像
		push(@param,$arrPARM{'imagenums'});
		push(@param,$arrPARM{'imageext'});
		#日時
		$times = time;
		#作成日時
		push(@param,$times);
		#更新日時
		push(@param,$times);
		#csv形式に変換
		$line = &makeCsvdata(@param);
		#先頭に追加
		unshift(@arrLogs,$line);
		#$MAX_COUNT超えてたら
		if($loglength >= $MAX_COUNT){
			$arrLogs[$MAX_COUNT] =~ s/(?:\x0D\x0A|[\x0D\x0A])?$/,/;
			@data = map {/^"(.*)"$/ ? scalar($_ = $1, s/""/"/g, $_) : $_}($arrLogs[$MAX_COUNT] =~ /("[^"]*(?:""[^"]*)*"|[^,]*),/g);
			#"#削除画像ある場合削除
			if($data[11]){
				#画像全部削除
				my @imagen = split(/:/,$data[11]);
				my $imagelen = @imagen;
				for($i=0;$i<$imagelen;$i++){
					my $delfile = $data[0] . "_" . $imagen[$i] . $data[12];
					&deleteImage($delfile);
				}
			}
			#行削除
			@arrLogs = splice(@arrLogs,0,$MAX_COUNT);
		}
	}else{
		#編集
		chomp ($arrLogs[$recno]);
		$arrLogs[$recno] =~ s/(?:\x0D\x0A|[\x0D\x0A])?$/,/;
		@data = map {/^"(.*)"$/ ? scalar($_ = $1, s/""/"/g, $_) : $_}($arrLogs[$recno] =~ /("[^"]*(?:""[^"]*)*"|[^,]*),/g);
		#"
		#記事No
		$itemno = $data[0];
		unshift(@param,$data[0]);
		#画像
		if($arrPARM{'upimagenum'}){
			#アップロードある場合
			push(@param,$arrPARM{'imagenums'});
			push(@param,$arrPARM{'imageext'});
		}elsif($arrPARM{'delimagenum'}){
			#削除の場合
			push(@param,$arrPARM{'imagenums'});
			if($arrPARM{'imagenums'}){
				#画像まだ存在
				push(@param,$data[12]);
			}else{
				#画像なくなる場合
				push(@param,'0');
			}
		}else{
			#変更なし
			push(@param,$data[11]);
			push(@param,$data[12]);
		}

		#作成日時再取得
		$times = $data[13];
		push(@param,$times);
		#更新日時
		$utimes = time;
		push(@param,$utimes);
		#csv形式に変換
		$line = &makeCsvdata(@param);

		#更新前データ削除
		splice(@arrLogs,$recno,1);
		#先頭に追加
		unshift(@arrLogs,$line);

		#画像アップしなおし or 画像削除
		if($arrPARM{'upimagenum'} && $data[12] || $arrPARM{'delimagenum'} && $data[12]){
			my @imagen2 = split(/:/,$arrPARM{'upimagenum'});
			my $imagelen2 = @imagen2;
			my @imagen3 = split(/:/,$arrPARM{'delimagenum'});
			my $imagelen3 = @imagen3;
			push(@imagen3,@imagen2);
			my %c;
			#重複削除
			@imagen3 = grep {!$c{$_}++} @imagen3;
			for($i=0;$i<$imagelen3;$i++){
				my $oldfile = $data[0] . "_" . $imagen3[$i] . $data[12];
				#古い画像削除
				&deleteImage($oldfile);
			}
		}
	}
	#ログ書き込み
	&makeLogs(@arrLogs);

	#画像アップロード
	if($arrPARM{'imageext'}){
		#アップロード
		&uploadImage($itemno,$arrPARM{'upimagenum'},$arrPARM{'imageext'});
	}
}
#---------------------------------------------------------------------
#画像削除
sub deleteImage{
	my ($filename) = @_;
	#削除
	$imagefile = $IMG_TMP_DIR . $IMG_NAME . $filename;
	unlink $imagefile;
}
#---------------------------------------------------------------------
#データ削除
sub deleteData{
	my $recno = $arrPARM{'recno'};
	my @data;
	my $imagefile;
	my $message;
	$recno--;
	if($recno > $loglength || $recno < 0){
		#エラー
		my $errormsg = $ERRMSGLIST[1];
		&Print_Error($errormsg);
	}else{
		chomp ($arrLogs[$recno]);
		$arrLogs[$recno] =~ s/(?:\x0D\x0A|[\x0D\x0A])?$/,/;
		@data = map {/^"(.*)"$/ ? scalar($_ = $1, s/""/"/g, $_) : $_}($arrLogs[$recno] =~ /("[^"]*(?:""[^"]*)*"|[^,]*),/g);
		#"
		#削除画像ある場合削除
		if($data[11]){
			#画像全部削除
			my @imagen = split(/:/,$data[11]);
			my $imagelen = @imagen;
			for($i=0;$i<$imagelen;$i++){
				my $delfile = $data[0] . "_" . $imagen[$i] . $data[12];
				&deleteImage($delfile);
			}
		}

		#該当行削除
		splice(@arrLogs,$recno,1);
		#ログファイル更新
		&makeLogs(@arrLogs);
		$message = 2;
	}
	return $message;
}
#---------------------------------------------------------------------
#ページ生成
sub makePage{
	#トップページ
	&viewTop($ADJ_COUNT,1);
	#ページ生成
	&makeHtml($HTML_TOP_REL . $HTML_TOP_NAME);

	#リストページ
	#ページ削除
	my $listfiles = $HTML_LIST_REL . $HTML_LIST_NAME . "*" . $HTML_LIST_EXT;
	unlink glob($listfiles);

	my $pagenum = int($loglength / $LIST_COUNT);
	if($loglength % $LIST_COUNT > 0){
		$pagenum++;
	}
	if($pagenum <= 0){
		$pagenum = 1;
	}

	for(my $i=1;$i<=$pagenum;$i++){
		$page = $i -1;
		my $listpage;
		&viewList($TMPL_LIST,$LIST_COUNT,$ADJ_COUNT,1);
		#ページ生成
		if($i > 1){
			$listpage = $HTML_LIST_REL . $HTML_LIST_NAME . $i . $HTML_LIST_EXT;
		}else{
			#1ページ目はページ数なし
			$listpage = $HTML_LIST_REL . $HTML_LIST_NAME . $HTML_LIST_EXT;
		}
		&makeHtml($listpage);
	}
	#$page値0にする。
	$page = 0;

 	#詳細ページ
 	#ページ削除
	my $detailfiles = $HTML_DETAIL_REL . $HTML_DETAIL_NAME . "*" . $HTML_DETAIL_EXT;
	unlink glob($detailfiles);

	#公開ディレクトリ画像削除（jpg,gif,png）
	my $openfilesjpg = $IMG_DIR_REL . "*" . ".jpg";
	my $openfilesgif = $IMG_DIR_REL . "*" . ".gif";
	my $openfilespng = $IMG_DIR_REL . "*" . ".png";
	unlink(glob($openfilesjpg),glob($openfilesgif),glob($openfilespng));

	for(my $i=1;$i<=$loglength;$i++){
		#画像コピー
		&copyImage();
		my $itemno = &viewDetail($i,1);
		#ページ生成
		my $detailpage = $HTML_DETAIL_REL . $HTML_DETAIL_NAME . $itemno . $HTML_DETAIL_EXT;
		&makeHtml($detailpage);
	}

	my $message = 3;
	return $message;
}
#---------------------------------------------------------------------
#文字省略
sub trim{
	my ($string,$strlong,$marker) = @_;

	if($strlong > 0){
		#文字カウントのため一時戻し
		$string =~ s/’/'/g;
		#'
		$string =~ s/&quot;/"/g;
		#"
		$string =~ s/&gt;/>/g;
		$string =~ s/&lt;/</g;
		$string =~ s/&amp;/&/g;

		my $ascii = '[\x00-\x7F]';
		my $twoBytes = '[\x8E\xA1-\xFE][\xA1-\xFE]';
		my $threeBytes = '\x8F[\xA1-\xFE][\xA1-\xFE]';
		my $euc_num = "(?:$ascii|$twoBytes|$threeBytes){$strlong}";
		$string =~ s/($euc_num)/$1\n/o;
		if($string =~ /\n/) {
			$string = (split(/\n/, $string))[0] . $marker;
		}

		#再変換
		$string =~ s/&/&amp;/g;
		$string =~ s/</&lt;/g;
		$string =~ s/>/&gt;/g;
		$string =~ s/"/&quot;/g;
		#"
		$string =~ s/'/’/g;
		#'
	}

	return $string;
}
#---------------------------------------------------------------------
#自動リンク
sub autolink{
	my ($str) = @_;
	my $text_regex = q{[^<]*};
	my $result = '';
	my $skip = 0;

	#http
	my $http_URL_regex =
		q{\b(?:https?|shttp)://(?:(?:[-_.!~*'()a-zA-Z0-9;:&=+$,]|%[0-9A-Fa-f} .
		q{][0-9A-Fa-f])*@)?(?:(?:[a-zA-Z0-9](?:[-a-zA-Z0-9]*[a-zA-Z0-9])?\.)} .
		q{*[a-zA-Z](?:[-a-zA-Z0-9]*[a-zA-Z0-9])?\.?|[0-9]+\.[0-9]+\.[0-9]+\.} .
		q{[0-9]+)(?::[0-9]*)?(?:/(?:[-_.!~*'()a-zA-Z0-9:@&=+$,]|%[0-9A-Fa-f]} .
		q{[0-9A-Fa-f])*(?:;(?:[-_.!~*'()a-zA-Z0-9:@&=+$,]|%[0-9A-Fa-f][0-9A-} .
		q{Fa-f])*)*(?:/(?:[-_.!~*'()a-zA-Z0-9:@&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f} .
		q{])*(?:;(?:[-_.!~*'()a-zA-Z0-9:@&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f])*)*)} .
		q{*)?(?:\?(?:[-_.!~*'()a-zA-Z0-9;/?:@&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f])} .
		q{*)?(?:#(?:[-_.!~*'()a-zA-Z0-9;/?:@&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f])*} .
		q{)?};
	
	#ftp
	my $ftp_URL_regex =
		q{\bftp://(?:(?:[-_.!~*'()a-zA-Z0-9;&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f])*} .
		q{(?::(?:[-_.!~*'()a-zA-Z0-9;&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f])*)?@)?(?} .
		q{:(?:[a-zA-Z0-9](?:[-a-zA-Z0-9]*[a-zA-Z0-9])?\.)*[a-zA-Z](?:[-a-zA-} .
		q{Z0-9]*[a-zA-Z0-9])?\.?|[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)(?::[0-9]*)?} .
		q{(?:/(?:[-_.!~*'()a-zA-Z0-9:@&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f])*(?:/(?} .
		q{:[-_.!~*'()a-zA-Z0-9:@&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f])*)*(?:;type=[} .
		q{AIDaid])?)?(?:\?(?:[-_.!~*'()a-zA-Z0-9;/?:@&=+$,]|%[0-9A-Fa-f][0-9} .
		q{A-Fa-f])*)?(?:#(?:[-_.!~*'()a-zA-Z0-9;/?:@&=+$,]|%[0-9A-Fa-f][0-9A} .
		q{-Fa-f])*)?};

	#mail
	my $mail_regex =
		q{(?:[^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff]+(?![^(\040)<>@,;:".\\\\} .
		q{\[\]\000-\037\x80-\xff])|"[^\\\\\x80-\xff\n\015"]*(?:\\\\[^\x80-\xff][} .
		q{^\\\\\x80-\xff\n\015"]*)*")(?:\.(?:[^(\040)<>@,;:".\\\\\[\]\000-\037\x} .
		q{80-\xff]+(?![^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff])|"[^\\\\\x80-} .
		q{\xff\n\015"]*(?:\\\\[^\x80-\xff][^\\\\\x80-\xff\n\015"]*)*"))*@(?:[^(} .
		q{\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff]+(?![^(\040)<>@,;:".\\\\\[\]\0} .
		q{00-\037\x80-\xff])|\[(?:[^\\\\\x80-\xff\n\015\[\]]|\\\\[^\x80-\xff])*} .
		q{\])(?:\.(?:[^(\040)<>@,;:".\\\\\[\]\000-\037\x80-\xff]+(?![^(\040)<>@,} .
		q{;:".\\\\\[\]\000-\037\x80-\xff])|\[(?:[^\\\\\x80-\xff\n\015\[\]]|\\\\[} .
		q{^\x80-\xff])*\]))*};

	my $tag_regex_ = q{[^"'<>]*(?:"[^"]*"[^"'<>]*|'[^']*'[^"'<>]*)*(?:>|(?=<)|$(?!\n))}; #'}}}}
	my $comment_tag_regex = '<!(?:--[^-]*-(?:[^-]+-)*?-(?:[^>-]*(?:-[^>-]+)*?)??)*(?:>|$(?!\n)|--.*$)';
	my $tag_regex = qq{$comment_tag_regex|<$tag_regex_};

	while ($str =~ /($text_regex)($tag_regex)?/gso) {
		last if $1 eq '' and $2 eq '';
		my $text_tmp = $1;
		my $tag_tmp = $2;
		if ($skip) {
			$result .= $text_tmp . $tag_tmp;
			$skip = 0 if $tag_tmp =~ /^<\/[aA](?![0-9A-Za-z])/;
		} else {
			$text_tmp =~ s{($http_URL_regex|$ftp_URL_regex|($mail_regex))}
				{my($org, $mail) = ($1, $2);
					(my $tmp = $org) =~ s/"/&quot;/g;
					'<A HREF="' . ($mail ne '' ? 'mailto:' : '') . "$tmp\">$org</A>"}ego;
			$result .= $text_tmp . $tag_tmp;
			$skip = 1 if $tag_tmp =~ /^<[aA](?![0-9A-Za-z])/;
			if ($tag_tmp =~ /^<(XMP|PLAINTEXT|SCRIPT)(?![0-9A-Za-z])/i) {
				$str =~ /(.*?(?:<\/$1(?![0-9A-Za-z])$tag_regex_|$))/gsi;
				$result .= $1;
			}
		}
	}

	#'
	return $result;

}
#---------------------------------------------------------------------
#日付変換
sub convDate{
	my ($logdate) = @_;
	my($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst);

	$ENV{'TZ'} = "JST-9";
	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($logdate);
	
	#表示調整
	$year = $year + 1900;
	$mon++;
	$mon = sprintf("%02d",$mon);
	$mday = sprintf("%02d",$mday);
	$hour = sprintf("%02d",$hour);
	$min = sprintf("%02d",$min);
	$sec = sprintf("%02d",$sec);
	$wday = $WEEKNAME[$wday];

	return ($year,$mon,$mday,$wday,$hour,$min,$sec);
}
#---------------------------------------------------------------------
#日付チェック
sub dayCheck{
	my($year, $month, $day) = @_;
	my(@mlast) = (31,28,31,30,31,30,31,31,30,31,30,31);
	if($month < 1 || 12 < $month) {
		return 0;
	}
	if($month == 2) {
		if ( (($year % 4 == 0) && ($year % 100 != 0)) || ($year % 400 == 0) ) {
			$mlast[1]++;
		}
	}
	if($day < 1 || $mlast[$month-1] < $day) {
		return 0;
	}
	return 1;
}
#---------------------------------------------------------------------
#曜日取得
sub getWday{
	my($year,$mon,$day) = @_;
	if($mon == 1 || $mon == 2) {
	$year--;
	$mon += 12;
	}
	my $wday = ($year + int($year/4) - int($year/100) + int($year/400) + int((13*$mon+8)/5) + $day) % 7;
	$wday = $WEEKNAME[$wday];
  return $wday;
}
#---------------------------------------------------------------------
#画像データ取得
sub getImageData{
	#画像NO,画像拡張子,画像枚数,ディレクトリ
	my($imageno,$imageext,$imagenum,$imagedir,$delimagenum) = @_;
	my $i;
	my $j = 1;
	my @imagedata;
	my @imagen = split(/:/,$imagenum);
	my $imagelen = @imagen;
	my $imagelast = 1;
	my @imagen3 = split(/:/,$delimagenum);
	my $imagelen3 = @imagen3;
	for($i=1;$i<=$IMG_COUNT;$i++){
		my %tmp;
		if($i <= $imagelen){
			#画像ある場合
			$tmp{'IMAGECO'} = $i;
			#画像名生成
			$tmp{'IMAGE'} = $imagedir . $IMG_NAME . $imageno . "_" . $imagen[$i-1] . $imageext;
			$tmp{'IMAGENO'} = $imagen[$i-1];
			$imagelast = $imagen[$i-1];
			#画像x,y
			my($image_x,$image_y) = getImageXY($IMG_TMP_DIR . $IMG_NAME . $imageno . "_" . $imagen[$i-1] . $imageext,$imageext);
			$tmp{'IMAGEX'} = $image_x;
			$tmp{'IMAGEY'} = $image_y;
			$tmp{'SETIMG_FLG'} = 1;
			if($j == 1){
				#あたまフラグ
				$tmp{'IMAGEF_FLG'} = 1;
			}else{
				$tmp{'IMAGEF_FLG'} = 0;
			}
			if($i == $imagelen && $j != $IMG_WRCOUNT){
				#おわりフラグ
				$tmp{'IMAGEE_FLG'} = 1;
			}else{
				$tmp{'IMAGEE_FLG'} = 0;
			}
			if($j == $IMG_WRCOUNT){
				#折り返しフラグ
				$tmp{'IMAGEW_FLG'} = 1;
				$j = 1;
			}else{
				$tmp{'IMAGEW_FLG'} = 0;
				$j++;
			}

			#削除フラグある場合
			if($delimagenum){
				if(grep(/$imagen[$i-1]/, @imagen3)){
					$tmp{'SETDEL_FLG'} = 1;
				}
			}
		}else{
			#画像ない場合
			$tmp{'IMAGECO'} = $i;
			$tmp{'IMAGENO'} = $imagelast;
			$tmp{'IMAGE'} = "";
			$tmp{'IMAGEX'} = "";
			$tmp{'IMAGEY'} = "";
			$tmp{'SETIMG_FLG'} = 0;
			$tmp{'IMAGEW'} = 0;
		}
		$imagelast++;
		push(@imagedata,\%tmp);
	}
	#sjis変換
#	foreach(@arrImageData){
#		Jcode::convert(\$_,'sjis','euc');
#	}
#	push(@imagedata,\%tmp); 
	return @imagedata;

}
#---------------------------------------------------------------------
#画像xy値取得
sub getImageXY {
	my($imagedata,$imageext) = @_;
	my $width;
	my $height;
	my $buffer;
	my $size = -s $imagedata;
	if(open(INFILE, "$imagedata")){ 
		binmode(INFILE);
		read(INFILE, $buffer, $size);
		close(INFILE);
	}else{
		return(0);
	}
	if($imageext eq '.gif') {
		#gifの場合
		$width = unpack("v",substr($buffer,6,2));
		$height= unpack("v",substr($buffer,8,2));
	}elsif($imageext eq '.png') {
		#pngの場合
		$width = unpack("N", substr($buffer,16,4));
		$height= unpack("N", substr($buffer,20,4));
	}elsif($imageext eq '.jpg') {
		#jpgの場合
		my $i = 2;
		my($t, $m, $c, $l);
		while (1) {
			$t = substr($buffer,$i,4);
			$i += 4;
			($m, $c, $l) = unpack("a a n", $t);
			if ($m ne "\xFF") {
				$width = $height = 0;
				last;
			}elsif((ord($c) >= 0xC0) && (ord($c) <= 0xC3)){
				$height = unpack("n", substr($buffer, $i+1, 2));
				$width = unpack("n", substr($buffer, $i+3, 2));
				last;
			}else{
				$t = substr($buffer, $t, ($l - 2));
				$i += $l - 2;
			}
		}
	} else {
		return(0);
	}

	if($IMG_TOOL_MAX > 0 && $IMG_WIDE_MAX > 0){
		if($width == 0){
			$width = $IMG_WIDE_MAX;
			$height = 0;
		}else{
			if($width >= $height){
				#横長の場合
				my $floor = $width / $IMG_WIDE_MAX;
				$height = $height / $floor;
				$height = int($height);
				$width = $IMG_WIDE_MAX;
			}else{
				#縦長の場合
				my $floor = $width / $IMG_TOOL_MAX;
				$height = $height / $floor;
				$height = int($height);
				$width = $IMG_TOOL_MAX;
			}
		}
	}

	return ($width,$height);
}
#---------------------------------------------------------------------
#テンプレート設定
sub setTemplate{
	my($tmpl) = @_;
	$template = HTML::Template->new(die_on_bad_params => 0,loop_context_vars => 1,global_vars => 1,filename => $tmpl);
}
#---------------------------------------------------------------------
#画面表示
sub openPage{
	print "Content-Type: text/html\n\n";
	print $template->output;
	exit;
}
#---------------------------------------------------------------------
#html吐き出し
sub makeHtml{
	my ($HTML) = @_;
	if(open(FH,">$HTML")){
		flock(FH, LOCK_EX);
		truncate(FH, 0);
		print FH $template->output;
		close(FH);
		chmod(0664, $HTML);
	}else{
		#エラー
		my $errormsg = $ERRMSGLIST[2];
		&Print_Error($errormsg);
	}
}
#---------------------------------------------------------------------
#画像コピー
sub copyImage{
	my @tmpfiles;

	#テンポラリディレクトリ内のファイル名取得
	opendir(DIR,"$IMG_TMP_DIR");
	@tmpfiles =  readdir(DIR);
	closedir(DIR);

	#画像ファイル名規則
	my $imgmatch = $IMG_NAME . "[0-9]+(_)[0-9]+\.(jpg|gif|png)";
	foreach $tmpfilename (@tmpfiles){
		if($tmpfilename =~/^$imgmatch$/){
			#画像ファイル名規則にマッチしたファイルはコピー
			#テンポラリディレクトリ
			my $fromfile = $IMG_TMP_DIR . $tmpfilename;
			#公開ディレクトリ
			my $tofile = $IMG_DIR_REL . $tmpfilename;
			my $retcode = copy ($fromfile,$tofile);
			if($retcode == 0){
				#エラー
				my $errormsg = $ERRMSGLIST[2];
				&Print_Error($errormsg);
			}
		}
	}
	if ($IMG_DEF){
		#デフォルト画像ある場合
		my $from_image_def = $IMG_TMP_DIR . $IMG_DEF;
		my $to_image_def = $IMG_DIR_REL . $IMG_DEF;
		my $retcode = copy ($from_image_def,$to_image_def);
		if($retcode == 0){
			#エラー
			my $errormsg = $ERRMSGLIST[2];
			&Print_Error($errormsg);
		}
	}
}
#---------------------------------------------------------------------
#csv形式に変換
sub makeCsvdata{
	my (@param) =@_;
	my $line;
	$line = join ',', map {(s/"/""/g or /[\r\n,]/) ? qq("$_") : $_} @param;
	#"
	$line = $line . "\n";
	Jcode::convert(\$line,'sjis','euc');
	return $line;
}
#---------------------------------------------------------------------
#ログセット
sub setLogs{
	@arrLogs = ();
	@arrData =();
	@arrSorts =();
	$loglength=0;
	$maxitemno=0;
	my @data;
	my $line;
	my $i = 0;
	if(open (INFILE, $LOGS)){
		while($line = <INFILE>){
			push(@arrLogs,$line);
			$line = $i . "," . $line;
			Jcode::convert(\$line,'euc','sjis');
			push(@arrData,$line);
			$i++;
		}
		close(INFILE);
		$loglength = $i;

		if($loglength > 0){
			#記事No最大値取得
			$maxitemno = (sort {(split(/\,/,$b))[1] <=> (split(/\,/,$a))[1]}@arrData)[0];
			$maxitemno =~ s/(?:\x0D\x0A|[\x0D\x0A])?$/,/;
			@data = map {/^"(.*)"$/ ? scalar($_ = $1, s/""/"/g, $_) : $_}($maxitemno =~ /("[^"]*(?:""[^"]*)*"|[^,]*),/g);
			#"
			$maxitemno = $data[1];
		}
		#ソート
		@arrSorts = sort {
			(split(/\,/,$b))[2] <=> (split(/\,/,$a))[2] || 
			(split(/\,/,$b))[3] <=> (split(/\,/,$a))[3] || 
			(split(/\,/,$b))[4] <=> (split(/\,/,$a))[4] || 
			(split(/\,/,$a))[0] <=> (split(/\,/,$b))[0]
		}@arrData;
	}else{
		#エラー
		my $errormsg = $ERRMSGLIST[2];
		&Print_Error($errormsg);
	}
}
#---------------------------------------------------------------------
#ログ書き込み
sub makeLogs{
	my (@makeLogs) = @_;
	if(open(FH,">$LOGS")){
		flock(FH, LOCK_EX);
		truncate(FH, 0);
		print FH @makeLogs;
		close(FH);
		chmod(0664, $LOGS);
	}else{
		#エラー
		my $errormsg = $ERRMSGLIST[2];
		&Print_Error($errormsg);
	}
}
#---------------------------------------------------------------------
#画像保存
sub uploadImage{
	my ($itemno,$imagenum,$imageext) = @_;
	my $buffer;
	my $i;
	#imagenum分解
	my @imagen = split(/:/,$imagenum);
	my $imagelen = @imagen;
	for($i=0;$i<$imagelen;$i++){
		my $tempfile = $IMG_TMP_DIR . $IMG_NAME . $itemno . "_" . $imagen[$i] . $imageext;
		my $imagep = "image_" . $imagen[$i];
		if(open(FH,">$tempfile")){
			flock(FH, LOCK_EX);
			truncate(FH, 0);
			binmode (FH);
			while(read($arrPARM{$imagep}, $buffer, 1024)){
				print FH $buffer;
			}
			close(FH);
			chmod(0664, $tempfile);
		}else{
			#エラー
			my $errormsg = $ERRMSGLIST[2];
			&Print_Error($errormsg);
		}
	}
}
#---------------------------------------------------------------------
#エラー画面表示
sub Print_Error{
	my ($errormsg) = @_;
	#エラー表示
	&setTemplate($TMPL_ERROR);
	Jcode::convert(\$errormsg,'sjis','euc');

	$template->param(ERRMSG=>$errormsg);
	&openPage();
}
#---------------------------------------------------------------------
#param値取得
sub setParam{
	$query = new CGI;
	my @parm = $query->param;
	my $name;
	my $value;
	my @delname;
	my @imagename;
	my $filedata;
	my $type;
	my $paramtype;
	my $paramimage;

	foreach  $name (@parm){
		$value = $query->param($name);

		if($name !~ /^image_[0-9]+$/i){
			Jcode::convert(\$name,'euc','sjis');
			#テキストデータ無害化
			$name =~ s/&/&amp;/g;
			$name =~ s/</&lt;/g;
			$name =~ s/>/&gt;/g;
			$name =~ s/"/&quot;/g;
			#"
			$name =~ s/'/’/g;
			#'
			Jcode::convert(\$value,'euc','sjis');
			$value =~ s/&/&amp;/g;
			$value =~ s/</&lt;/g;
			$value =~ s/>/&gt;/g;
			$value =~ s/"/&quot;/g;
			#"
			$value =~ s/'/’/g;
			#'
			#改行コード→<br />
			$value =~ s/\x0D\x0A/<br \/>/g;
			$value =~ s/\x0D/<br \/>/g;
			$value =~ s/\x0A/<br \/>/g;
			#配列に値格納
			$arrPARM{$name} = $value;
			
			if($name =~ /^imageno_[0-9]+$/i){
				$arrPARM{'imagenum'} .= $value .":";
			}
			if($name =~ /^imagedel_[0-9]+$/i){
				@delname = split(/_/,$name);
				$arrPARM{'delimagenum'} .= $delname[1] .":";
			}
		}else{
			if($value){
				#ファイル形式
				$filedata = $query->upload($name);
				$type = $query->uploadInfo($filedata)->{'Content-Type'};

				@imagename = split(/_/,$name);
				$paramtype = "type_" . $imagename[1];
				$paramimage = "image_" . $imagename[1];
				$arrPARM{$paramtype} = $type;
				$arrPARM{$paramimage} = $filedata;
				#ファイルナンバー
				$arrPARM{'upimagenum'} .= $imagename[1] . ":";
			}
		}
	}
	#page取得
	if(exists($arrPARM{'page'}) && $arrPARM{'page'} > 0){
		$page = $arrPARM{'page'} -1;
	}else{
		$page = 0;
	}
}
#---------------------------------------------------------------------
1;
