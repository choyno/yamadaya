// 希望の店舗の取得
function changeShopSel(){
    // 選択されたエリアの取得
    selIndex = document.form1.area.selectedIndex;
    area = EscapeSJIS(document.form1.area.options[selIndex].value);

    // エリアが選択されていないときは先に選択してもらう
    if(area == ""){
        document.form1.shop1.length = 1;
        document.form1.shop1.options[0].text = "まずはエリアを選択してください";
        document.form1.shop1.options[0].value = "";
        document.form1.shop2.length = 1;
        document.form1.shop2.options[0].text = "まずはエリアを選択してください";
        document.form1.shop2.options[0].value = "";
    }else{
        // バックグラウンドで店舗のデータを取得
        var url = "./?act=shop&area=" + area;

        $.ajax({
            url:url,
            type: 'GET',
            timeout: 1000,
            error: function(data){
                alert('店舗データが取得できません');
            },
            success: function(data){
                changeShopData(data);
            }
        });
    }
}

// 希望の店舗のセレクトボックスの内容を変更する
function changeShopData(data){
    // TABが含まれなかったら(TSVの形で送られてこなかったら)対象エリアでの募集がない
    if(data.length == 0){
        document.form1.shop1.length = 1;
        document.form1.shop1.options[0].text = "現在この地域での募集はありません";
        document.form1.shop1.options[0].value = "";
        document.form1.shop2.length = 1;
        document.form1.shop2.options[0].text = "現在この地域での募集はありません";
        document.form1.shop2.options[0].value = "";
    } else {
        // TSVのテキストを配列に格納
        arrShop = data.split("	");

        document.form1.shop1.length = 1;
        document.form1.shop1.options[0].text = "選択してください";
        document.form1.shop1.options[0].value = "";
        document.form1.shop2.length = 1;
        document.form1.shop2.options[0].text = "選択してください";
        document.form1.shop2.options[0].value = "";

        // 店舗の数だけセレクトボックスのオプションを増やす
        for(var i in arrShop){
            document.form1.shop1.length++;
            document.form1.shop1.options[parseInt(i) + 1].text = arrShop[i];
            document.form1.shop1.options[parseInt(i) + 1].value = arrShop[i];

            document.form1.shop2.length++;
            document.form1.shop2.options[parseInt(i) + 1].text = arrShop[i];
            document.form1.shop2.options[parseInt(i) + 1].value = arrShop[i];
        }
    }
}
