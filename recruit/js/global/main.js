/**
 * ================================================
 *
 * [javascript Common]
 * 必須ライブラリ : jQuery, velocity
 * 
 * ================================================
 */
var qsParm = new Array(), // GETパラメータ
	userAgent = window.navigator.userAgent.toLowerCase(),
	browserIE = 0, // IE判定
	browser_v = 0, // IEバージョン番号
	browser_nm = "", // browser名
	roImgCache, // 画像切り替えロールオーバー元ソース
	ua = navigator.userAgent;
	
var	isIE 	= ua.match(/msie/i),
	isIE6 	= ua.match(/msie [6.]/i),
	isIE7 	= ua.match(/msie [7.]/i),
	isIE8 	= ua.match(/msie [8.]/i),
	isIE9 	= ua.match(/msie [9.]/i),
	isIE10 	= ua.match(/msie [10.]/i),
	isIE11 	= ua.match(/msie [11.]/i);
	
var	isiPhone 	= false,
	isiPad 		= false,
	isiPod 		= false,
	isAndroid 	= false,
	isWinPhone 	= false,
	isMobile 	= false;

var isNavOpen 		= false,
	spBreakPoint 	= 768,
	wWidth 			= 0,
	wHeight 		= 0,
	isRespMode 		= false,
	isPageTopShow 	= false,
	pageTopPoint	= 100,
	scrTop 			= 0,
	scrLeft 		= 0,
	orgMode 		= null,
	currentMode 	= null,
	isChangeMode 	= false;
/**
 * 実行
 * OFFにしたいものはコメントアウトする
 */
$(function() {
	// 初期実行
	$.main.setGETqs();
	$.main.checkRespMode();
	$.main.setWbVer();
	// $.main.sScroll();
	$.main.getScrPos();
	$.main.getMobile();
	
	// loadイベント
	$(window).ready(function() {
		$.main.roImg();
		$.main.roOpa();
		$.main.adjList();
		$.main.pageTop();
	});
	// resizeイベント
	$(window).bind('resize', function() {
		$.main.checkRespMode();
		$.main.getMobile();
	});
	// scrollイベント
	$(window).bind('scroll', function() {
		$.main.pageTop();
		$.main.getScrPos();
	});
});
/**
 * functions
 */
$.main = {
	/**
	 * strict対策blank open
	 * @param 開くURL
	 */
	openBlank: function(url) {
		window.open(url);
	},
	/**
	 * 画像の先読み
	 * @param 画像配列
	 */
	preloadImg: function(arguments) {
		if (!arguments) return;
		
		for(var i = 0; i < arguments.length; i++) {
			$('<img>').attr('src', arguments[i]);
		}
	},
	/**
	 * GET値の取得
	 * @return GET値をセットしたグローバル変数qsParm
	 */
	setGETqs: function() {
		var query = window.location.search.substring(1),
			parms = query.split('&');
			
		for (var i = 0; i < parms.length; i++) {
			var pos = parms[i].indexOf('=');
			
			if (pos > 0) {
				var key = parms[i].substring(0, pos),
					val = parms[i].substring(pos +1);
					
				qsParm[key] = val;
			}
		}
		return qsParm;
	},
	/**
	 * Webブラウザバージョンの取得
	 */
	setWbVer: function() {
		if (/*@cc_on!@*/false) {
		
			browserIE = 1;
			browser_nm = "IE";
		
			if (navigator.userAgent.match(/MSIE (¥d¥.¥d+)/)) {browser_v = parseFloat(RegExp.$1);}//IE6.7.8.9
		
			}
			else if (userAgent.indexOf("firefox") > -1) {browser_nm = "Firefox";}
			else if (userAgent.indexOf("opera") > -1) {browser_nm = "Opera";}
			else if (userAgent.indexOf("chrome") > -1) {browser_nm = "Chrome";}
			else if (userAgent.indexOf("safari") > -1) {browser_nm = "Safari";}
			else {
			browser_nm = "Unknown";
		}
	},
	/**
	 * 画像切り替えロールオーバー
	 */
	roImg: function() {
		var imgCache = new Array;
		
		$(document).on({
			mouseenter: function() {
				var imgSrc 		= $(this).attr('src'),
					imgSrcDot 	= imgSrc.lastIndexOf('.'),
					imgSrcOver 	= imgSrc.substr(0, imgSrcDot) + '_on' + imgSrc.substr(imgSrcDot, 4);
				
				imgCache[$(this)] = imgSrc;
				
				$(this).attr('src', imgSrcOver);
			},
			mouseleave: function() {
				$(this).attr('src', imgCache[$(this)]);
			}
		}, 'a > .roImg');
	},
	/**
	 * 画像透明度ロールオーバー
	 */
	roOpa: function() {
		$(document).on({
			mouseenter: function() {
				$(this).fadeTo(300, 0.5);
			},
			mouseleave: function() {
				$(this).stop(true, true).fadeTo(300, 1.0);
			}
		}, '.jqHover');
	},
	/**
	 * 単純な高さ合わせ
	 * (画像があると読み込み順の関係でうまく動かないことがある)
	 */
	adjList: function() {
		$('.adjustList').each(function() {
			var setHeight = 0;
			
			$(this).children().each(function() {
				var getHeight = $(this).height();
				
				if (getHeight > setHeight) {
					setHeight = getHeight;
				}
			});
			
			$(this).children().height(setHeight);
			
			if ($(this).children().length == 0) $(this).remove();
		});
	},
	/**
	 * スムーススクロール
	 * @param オフセット値
	 * @param スピード
	 * @param イージング
	 */
	sScroll: function(offset, duration, easing) {
		$(document).on('click', 'a[href^="#"].scroll', function() {
			var $anchor = $(this),
				$target = $anchor.attr('href'),
				duration = duration !== 500 && $.isNumeric(duration) ? duration : 500,
				easing = easing !== 'easeInQuad' ? easing : 'easeInQuad';
				
			$($target).velocity('stop').velocity('scroll', {
				offset: offset,
				duration:duration,
				easing: easing
			});
			
			return false;
		});
	},
	/**
	 * レスポンシブ状態のチェック
	 */
	checkRespMode: function() {
		wHeight 	= parseInt($(window).height()),
		wWidth 		= parseInt($(window).width()),
		isRespMode 	= wWidth < spBreakPoint ? true : false;
		
		if (orgMode === null) {
			orgMode = isRespMode ? 2 : 1;
			currentMode = orgMode;
		} else {
			oldMode = currentMode;
			currentMode = isRespMode ? 2 : 1;
			
			isChangeMode = oldMode !== currentMode ? true : false;
		}
	},
	/**
	 * スクロール位置の取得
	 */
	getScrPos: function() {
		scrTop = $(window).scrollTop(),
		scrLeft = $(window).scrollLeft();
	},
	/**
	 * 標準的なpagetop(表示／非表示のみ)
	 * 表示ポイントをpageTopPointに設定
	 * 後から追加する要素には未対応
	 */
	pageTop: function() {
		if (scrTop > pageTopPoint) {
			if (!isPageTopShow) {
				isPageTopShow = true;
				$('#pageTopVox').velocity('stop').show().velocity({
					opacity: 1
				}, 200);
			}
		} else {
			if (isPageTopShow) {
				isPageTopShow = false;
				$('#pageTopVox').velocity('stop').velocity({
					opacity: 0
				}, 200, function() {
					$(this).hide();
				});
			}
		}
	},
	/**
	 * iPhone / iPad / iPod / Android / winPhone 判定
	 */
	getMobile: function() {
		ua = navigator.userAgent;
		
		isiPhone 	= ua.indexOf('iPhone') != -1 ? true : false,
		isiPad 		= ua.indexOf('iPad') != -1 ? true : false,
		isiPod 		= ua.indexOf('iPod') != -1 ? true : false,
		isAndroid 	= ua.indexOf('android') != -1 || ua.indexOf('Android') != -1 ? true : false,
		isWinPhone 	= ua.indexOf('Windows Phone') != -1 ? true : false;
		
		isMobile = isiPhone || isiPad || isiPod || isAndroid || isWinPhone ? true : false;
	}
};