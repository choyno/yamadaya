/**
 ***********************************************
 * 
 * 画像をその場で拡大→縮小のアニメーション
 * @category 	Application of AZLINK.
 * @author 		Norio Murata <nori@azlink.jp>
 * @copyright 	2014- AZLINK. <http://www.azlink.jp>
 * @final 		2014.08.18
 * 
 ***********************************************
 */
(function($) {
	
	$.fn.extend({
		
		imgZooming: function(config) {
			
			var img = this;
			
			var defaults = {
				duration: 500
			};
			
			var options = $.extend(defaults, config);
			
			return this.each(function(i) {
				var objOrgW = parseInt($(img).width()),
					objOrgH = parseInt($(img).height()),
					objNewW = objOrgW - (objOrgW * 0.8),
					objNewH = objOrgH - (objOrgH * 0.8),
					objOrgT = parseInt($(img).css('top')),
					objOrgL = parseInt($(img).css('left')),
					objT = objOrgT + ((objOrgH - objNewH) / 2),
					objL = objOrgL + ((objOrgW - objNewW) / 2);
				
				$(img).width(objNewW).height(objNewH).css({
					top: objT,
					left: objL
				});
				
				$(img).show().stop().animate({
					height: objOrgH,
					width: objOrgW,
					left: objOrgL,
					top: objOrgT
				}, defaults.duration, 'easeOutBack');
			});
		}
		
	});
	
}(jQuery))
