/**
 ***********************************************
 * 
 * ポップアップを画面の中央に表示
 * @category 	Application of AZLINK.
 * @author 		Norio Murata <nori@azlink.jp>
 * @copyright 	2010- AZLINK. <http://www.azlink.jp>
 * @final 		2016.01.22
 * 
 ***********************************************
 */
(function($) {
	$.fn.popupAdjust = function(config) {
		var popup = this;
		
		var defaults = {
			wrapper: '#wrapper',
			layer: '#alphaBg',
			event: 'resize scroll',
			scrollAdjust: true,
			realtimeAdjust: true
		};
		
		var options = $.extend(defaults, config);
		
		$.fn.popupAdjust.adjust = function(target) {
			var wrapperHeight = $(options.wrapper).height(),
				windowHeight = $(window).height(),
				windowWidth = $(window).width(),
				popupHeight = $(target).outerHeight(),
				popupWidth = $(target).outerWidth(),
				topPos,
				leftPos,
				bgHeight,
				scrollTop = $(window).scrollTop(),
				scrollLeft = $(window).scrollLeft();
				
			topPos = windowHeight > popupHeight ? (windowHeight - popupHeight) / 2 : 0,
			leftPos = windowWidth > popupWidth ? (windowWidth - popupWidth) / 2 : 0;
			
			if (target) {
				$(target).css({
					left: leftPos + scrollLeft,
					top: topPos + scrollTop
				});
			}
		};
		
		$.fn.popupAdjust.bgAdjust = function(target) {
			var wrapperHeight = $(options.wrapper).height(),
				windowHeight = $(window).height(),
				windowWidth = $(window).width(),
				pTop = $(target).position().top,
				pHeight = $(target).height(),
				pSpace = wrapperHeight - pTop,
				bgHeight = pSpace < pHeight ? wrapperHeight + (pHeight - pSpace) : wrapperHeight;
				
			$(options.layer).height(bgHeight);
		};
		
		$(window).load(function() {
			$.fn.popupAdjust.adjust(popup);
			
			var resizeTimer;
			
			$(window).bind(options.event, function() {
				if (resizeTimer !== false) {
					clearTimeout(resizeTimer);
			    }
			    resizeTimer = setTimeout(function() {
			    	if (options.scrollAdjust) {
			    		if (!$(popup).is(':visible')) $.fn.popupAdjust.adjust(popup);
			    	}
			    	if (options.realtimeAdjust) {
			    		if ($(popup).is(':visible')) $.fn.popupAdjust.adjust(popup);
			    	}
			    }, 200);
			});
		});
		
		return this;
	};
	
}(jQuery));
