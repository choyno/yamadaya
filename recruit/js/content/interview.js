/**
 * ================================================
 *
 * [interview]
 *
 * ================================================
 */
var rTimer 			= false,
	isFirst 		= true,
	isAccOpen 		= new Array,
	isAllowMSort 	= false;

(function($) {
	$(function() {
		/**
		 ********************************************
		 * load event
		 ********************************************
		 */
		$(window).load(function() {
			functions.init();
		});
		/**
		 ********************************************
		 * resize event
		 ********************************************
		 */
		$(window).resize(function() {
			if (rTimer !== false) {
				clearTimeout(rTimer);
			}
			
			rTimer = setTimeout(function() {
				functions.adjust();
			}, 500);
		});
		/**
		 ********************************************
		 * scroll event
		 ********************************************
		 */
		
		/**
		 ********************************************
		 * click event
		 ********************************************
		 */
		$('#interviewList .header').click(function() {
			if ($('#interviewList .inner').hasClass('.velocity-animating')) return;
			
			var index = $('#interviewList .header').index(this);
			
			if (isAccOpen[index]) {
				$(this).next('#interviewList .inner').velocity('stop').velocity('slideUp', {
					duration: 300,
					easing: 'easeOutSine',
					complete: function() {
						isAccOpen[index] = false;
					}
				});
				$(this).removeClass('open');
				$(this).children('.button').removeClass('minus');
			} else {
				$(this).next('#interviewList .inner').velocity('stop').velocity('slideDown', {
					duration: 300,
					easing: 'easeOutSine',
					complete: function() {
						isAccOpen[index] = true;
					}
				});
				$(this).addClass('open');
				$(this).children('.button').addClass('minus');
				
				$(this).velocity('scroll', {
					duration: 300,
					offset: -hHeight
				});
			}
		});
		/**
		 ********************************************
		 * hover event
		 ********************************************
		*/
		
		/**
		 ********************************************
		 * mousewheel event
		 ********************************************
		 */

		/**
		 ********************************************
		 * key event
		 ********************************************
		 */

		/**
		 ********************************************
		 * etc
		 ********************************************
		 */
		$.main.preloadImg(
			new Array(
				'../images/content/interview/interview_list01_on.jpg',
				'../images/content/interview/interview_list02_on.jpg',
				'../images/content/interview/interview_list03_on.jpg',
				'../images/content/interview/interview_list04_on.jpg',
				'../images/content/interview/interview_list05_on.jpg',
				'../images/content/interview/interview_list06_on.jpg'
			)
		);
	});
}(jQuery));
/**
 **********************************************************
 *
 * functions
 *
 **********************************************************
 */
var functions = {
	init: function() {
		setTimeout(function() {
			functions.adjust();
		}, 500);
	},
	adjust: function() {
		$(".contVox#topics .inner").height('auto').adjustSize();
	}
};