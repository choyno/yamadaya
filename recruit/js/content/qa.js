/**
 * ================================================
 *
 * [qa]
 *
 * ================================================
 */
var rTimer 			= false,
	isFirst 		= true,
	isAccOpen 		= new Array;

(function($) {
	$(function() {
		/**
		 ********************************************
		 * load event
		 ********************************************
		 */
		$(window).load(function() {
			functions.init();
		});
		/**
		 ********************************************
		 * resize event
		 ********************************************
		 */
		$(window).resize(function() {
			if (rTimer !== false) {
				clearTimeout(rTimer);
			}
			
			rTimer = setTimeout(function() {
				functions.adjust();
			}, 500);
		});
		/**
		 ********************************************
		 * scroll event
		 ********************************************
		 */
		
		/**
		 ********************************************
		 * click event
		 ********************************************
		 */
		$('.qaWrapper .question').click(function() {
			if ($('.qaWrapper .answer').hasClass('.velocity-animating')) return;
			
			var index = $('.qaWrapper .question').index(this);
			
			if (isAccOpen[index]) {
				$(this).next('.qaWrapper .answer').velocity('stop').velocity('slideUp', {
					duration: 300,
					easing: 'easeOutSine',
					complete: function() {
						isAccOpen[index] = false;
					}
				});
				$(this).removeClass('open');
				$(this).children('.button').removeClass('minus');
			} else {
				$(this).next('.qaWrapper .answer').velocity('stop').velocity('slideDown', {
					duration: 300,
					easing: 'easeOutSine',
					complete: function() {
						isAccOpen[index] = true;
					}
				});
				$(this).addClass('open');
				$(this).children('.button').addClass('minus');
				
				$(this).velocity('scroll', {
					duration: 300,
					offset: -hHeight
				});
			}
		});
		/**
		 ********************************************
		 * hover event
		 ********************************************
		*/
		
		/**
		 ********************************************
		 * mousewheel event
		 ********************************************
		 */

		/**
		 ********************************************
		 * key event
		 ********************************************
		 */

		/**
		 ********************************************
		 * etc
		 ********************************************
		 */
		
	});
}(jQuery));
/**
 **********************************************************
 *
 * functions
 *
 **********************************************************
 */
var functions = {
	init: function() {
		setTimeout(function() {
			functions.adjust();
		}, 500);
	},
	adjust: function() {
		
	}
};