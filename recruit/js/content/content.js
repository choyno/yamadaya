/**
 * ============================================================
 *
 * [content]
 * 
 * ============================================================
 */
var isNavOpen 		= false,
	isSkip 			= false,
	isDefaultFirst 	= true,
	resizeTimer 	= false,
	flowAnime 		= new Array,
	hHeight 		= 0,
	hHeightOrg 		= 0,
	isFlowAnime 	= false,
	isFlowDefault 	= true;
	
(function($) {
	$(function() {
		isSkip = $('body').hasClass('skip') ? true : false,
		isFlowAnime = $('body').hasClass('flowAnime') && !isMobile ? true : false;
		/**
		 ********************************************
		 * load event
		 ******************************************** 
		 */
		$(window).on('load', function() {
			defaultFunctions.init();
		});
		/**
		 ********************************************
		 * resize event
		 ******************************************** 
		 */
		$(window).on('resize', function() {
			if (resizeTimer !== false) {
				clearTimeout(resizeTimer);
			}
			
			resizeTimer = setTimeout(function() {
				defaultFunctions.adjust();
				
				if (isChangeMode) {
					$(window).off('.flowVox');
					isFlowAnime = false;
					// isFlowDefault = true;
					defaultFunctions.initFlow();
					if (!isRespMode) {
						$('#gNavOpener').removeClass('open');
						
						$('#gNav').css({
							opacity: 1,
							display: 'block'
						});
					} else {
						$('#gNav').css({
							opacity: 1,
							display: 'none'
						});
					}
						
					isNavOpen = false;
				}
			}, 500);
		});
		/**
		 ********************************************
		 * scroll event
		 ******************************************** 
		 */
		/**
		 ********************************************
		 * click event
		 ******************************************** 
		 */
		$(document).on('click', '#pageTopVox a', function() {
			$('#wrapper').velocity('scroll', 500);
		});
		
		$(document).on('click', '#gNavOpener', function() {
			if ($('#gNav').hasClass('.velocity-animating')) return;
			
			gNavWidth = $("#gNav").outerWidth();
			
			if (isNavOpen) {
				$('#gNavOpener').removeClass('open');
				
				$('#gNav').velocity({
					opacity: 0
				}, 250, function() {
					$('#gNav').css({
						opacity: 1,
						display: 'none'
					});
					
					isNavOpen = false;
				});
			} else {
				$('#gNavOpener').addClass('open');
				
				$('#gNav').css({
					opacity: 0,
					display: 'block',
					//top: scrTop + hHeight
				}).velocity({
					opacity: 1
				}, 250, function() {
					isNavOpen = true;
				});
			}
		});
		
		/*$(document).on('click', '#gNavClose', function() {
			$("#gNav").fadeOut(200);
		});*/
		/**
		 ********************************************
		 * hover event
		 ******************************************** 
		 */
		
		/**
		 ********************************************************** 
		 * 
		 * sns 
		 * 
		 ********************************************************** 
		 */
		
	});
}(jQuery));
/**
 ********************************************
 * functions
 ******************************************** 
 */
var defaultFunctions = {
	init: function() {
		hHeightOrg = $('#siteHeader').outerHeight();
		
		$.main.sScroll(-hHeightOrg, 500, 'easeInQuad');
		
		$('.roFadeImg').rolloverFade();
		
		$('.rplSPImg').replaceImageSP({
			breakPoint: spBreakPoint
		});
		
		setTimeout(function() {
			if (!isSkip && isFlowAnime) defaultFunctions.initFlow();
			defaultFunctions.runIntro();
		}, 500);
		
		defaultFunctions.adjust();
	},
	adjust: function() {
		hHeight = $('#siteHeader').outerHeight() !== hHeightOrg ? $('#siteHeader').outerHeight() : hHeightOrg;
		
		if (hHeight !== hHeightOrg) {
			$('.scroll').off('click');
			$.main.sScroll(-hHeight, 500, 'easeInQuad');
		}
		
		if (isRespMode) {
			$('#container').css({
				paddingTop: hHeight
			});
		} else {
			$('#container').css({
				paddingTop: 0
			});
		}
	},
	runIntro: function() {
		if (isSkip) return;
		
		$('#wrapper').css({
			//opacity: 0,
			visibility: 'visible'
		});
		
		if (location.hash !== '') {
			$(location.hash).velocity('stop').velocity('scroll', {
				offset: -hHeight,
				duration: 10,
				easing: 'easeInQuad'
			});
		}
		
		$('#wrapper').stop().velocity({
			opacity: 1
		}, {
			delay: 400,
			duration: 250,
			complete: function() {
				isDefaultFirst = false;
			}
		});
	},
	initFlow: function() {
		if (isFlowAnime) {
			if (isFlowDefault) {
				var per = wWidth > wHeight ? 0.8 : 0.95;
					
				$('.flowVox').each(function(i) {
					$(this).children().wrapAll('<div class="animeObj" />');
				
					$(this).find('.animeObj').css({
						visibility: 'visible'
					}).velocity({
						translateY: 60,
						opacity: 0
					}, 0);
					
					var param = {
						pos: $(this).find('.animeObj:eq(0)').offset().top,
						isDone: false,
						height: $(this).height()
					};
					
					flowAnime[i] = function() {
						if (scrTop > param.pos - (wHeight * per)) {
							if (param.isDone) return;
							
							$('.flowVox').eq(i).find('.animeObj').each(function(j) {
								$(this).velocity('stop').velocity({
									translateY: 0,
									opacity: 1
								}, {
									duration: 1200,
									delay: j * 300,
									easing: 'easeOutCubic'
								});
							});
							
							param.isDone = true;
						}
					};
					
					defaultFunctions.runFlow(flowAnime[i]);
				});
				
				isFlowDefault = false;
			}
		} else {
			$('.flowVox .animeObj').css({
				visibility: 'visible'
			}).velocity({
				translateY: 0,
				opacity: 1
			}, 0);
		}
	},
	runFlow: function(fa) {
		if (!isFlowAnime) return;
		
		$(window).on({
			'scroll.flowVox': fa
		}).scroll();
	}
};