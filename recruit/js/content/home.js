/**
 * ================================================
 *
 * [home]
 *
 * ================================================
 */
var rTimer 			= false,
	isFirst 		= true,
	isAllowMSort 	= false;

(function($) {
	$(function() {
		/**
		 ********************************************
		 * load event
		 ********************************************
		 */
		$(window).load(function() {
			functions.init();
		});
		/**
		 ********************************************
		 * resize event
		 ********************************************
		 */
		$(window).resize(function() {
			if (rTimer !== false) {
				clearTimeout(rTimer);
			}
			
			rTimer = setTimeout(function() {
				functions.adjust();
			}, 500);
		});
		/**
		 ********************************************
		 * scroll event
		 ********************************************
		 */
		
		/**
		 ********************************************
		 * click event
		 ********************************************
		 */
		/**
		 ********************************************
		 * hover event
		 ********************************************
		*/
		
		/**
		 ********************************************
		 * mousewheel event
		 ********************************************
		 */

		/**
		 ********************************************
		 * key event
		 ********************************************
		 */

		/**
		 ********************************************
		 * etc
		 ********************************************
		 */
		
	});
}(jQuery));
/**
 **********************************************************
 *
 * functions
 *
 **********************************************************
 */
var functions = {
	init: function() {
		setTimeout(function() {
			functions.adjust();
			
			setTimeout(function() {
				functions.runIntro();
			}, 500);
		}, 500);
	},
	adjust: function() {
		$(".homeSection#section01, .homeSection#section02, .homeSection#section03, .homeSection#section04").height('auto').adjustSize();
	},
	runIntro: function(err, args) {
		isFirst = false;
		
		$('#wrapper').css({
			visibility: 'visible',
			opacity: 0
		}).velocity({
			opacity: 1
		}, 500);
	}
};