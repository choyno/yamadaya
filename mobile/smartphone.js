// window.onloadイベントの追加
function addWindowOnload(func) {
  if ( typeof window.addEventListener != "undefined" ) {
    window.addEventListener( "load", func, false );
  } else if ( typeof window.attachEvent != "undefined" ) {
    window.attachEvent( "onload", func );
  } else {
    if ( window.onload != null ) {
      var oldOnload = window.onload;
      window.onload = function ( e ) {
        oldOnload( e );
        window[func]();
      };
    } else {
      window.onload = func;
    }
  }
}
// スマートフォン独自の処理を追加
addWindowOnload( function( ) {
    var elements, i;
    // parent-linkクラスのA要素について親要素もリンクに偽装する
    elements = document.getElementsByClassName('parent-link');
    for(i = 0; i < elements.length; i++) {
        var element = elements[i];
        if(element.tagName == 'A' && element.href != '') {
            var parent = element.parentNode;
            parent.setAttribute('href', element.href);
            parent.onclick = function () { location.href=this.getAttribute('href'); };
            parent.style.cursor = 'pointer';
        }
    }
    // grand-parent-linkクラスのA要素について親の親要素もリンクに偽装する
    elements = document.getElementsByClassName('grand-parent-link');
    for(i = 0; i < elements.length; i++) {
        var element = elements[i];
        if(element.tagName == 'A' && element.href != '') {
            var parent = element.parentNode.parentNode;
            parent.setAttribute('href', element.href);
            parent.onclick = function () { location.href=this.getAttribute('href'); };
            parent.style.cursor = 'pointer';
        }
    }
} );
