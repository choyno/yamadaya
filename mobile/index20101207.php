<?php
  // Copyright 2009 Google Inc. All Rights Reserved.
  $GA_ACCOUNT = "MO-3354725-1";
  $GA_PIXEL = "/ga.php";

  function googleAnalyticsGetImageUrl() {
    global $GA_ACCOUNT, $GA_PIXEL;
    $url = "";
    $url .= $GA_PIXEL . "?";
    $url .= "utmac=" . $GA_ACCOUNT;
    $url .= "&utmn=" . rand(0, 0x7fffffff);
    $referer = $_SERVER["HTTP_REFERER"];
    $query = $_SERVER["QUERY_STRING"];
    $path = $_SERVER["REQUEST_URI"];
    if (empty($referer)) {
      $referer = "-";
    }
    $url .= "&utmr=" . urlencode($referer);
    if (!empty($path)) {
      $url .= "&utmp=" . urlencode($path);
    }
    $url .= "&guid=ON";
    return str_replace("&", "&amp;", $url);
  }
?>
<html>
<head>
<title>YAMADAYA/株式会社ﾔﾏﾀﾞﾔ ﾓﾊﾞｲﾙｻｲﾄ</title>
<style type="text/css">
<!--
.red {
color: #ff0000;
}
-->
</style>
</head>
<body bgcolor="#ffffff" text="#666666" link="#666666" vlink="#666666" alink="#cccccc">
<font size="-2">
<a name="pagetop"></a>
<p style="margin: 0;"><img alt="YAMADAYA" src="/mobile/images/common/img_logo_mobile.gif" /></p>
<p style="margin: 0;">
<img alt="img_main20100607_mobile.jpg" class="mt-image-none" src="http://www.ymdy.co.jp/mobile/images_mt/img_main20100607_mobile.jpg" /><br />
<img alt="" src="/mobile/images/common/img_line01_mobile.gif" /></p>

<p align="center"><a href="http://www.e-radiate.jp/"><img src="/mobile/images/parts/banner_ws1.jpg" alt="radiate WEB SHOP" border="0"></a></p>

<p style="margin: 10px 0 10px 0;"><a href="/mobile/news/index.php"><img alt="NEW" border="0" src="/mobile/images/btn/btn_news_mobile.gif" /></a></p>
<p style="margin: 0 0 10px 0;"><a href="/mobile/new_arrival/index.php"><img alt="NEW ARRIVAL" border="0" src="/mobile/images/btn/btn_na_mobile.gif" /></a></p>
<p style="margin: 0 0 10px 0;"><a href="/mobile/collection/index.php"><img alt="COLLECTION" border="0" src="/mobile/images/btn/btn_collection_mobile.gif" /></a></p>

<p style="margin: 0 0 10px 0;"><a href="/mobile/shop/index.php"><img alt="SHOP" border="0" src="/mobile/images/btn/btn_shop_mobile.gif" /></a></p>
<p style="margin: 0 0 10px 0;"><a href="/mobile/gallery/index.php"><img alt="GALLERY" border="0" src="/mobile/images/btn/btn_gallery_mobile.gif" /></a></p>
<p style="margin: 0;"><a href="/mobile/event_information/index.php"><img alt="EVENT INFORMATION" border="0" src="/mobile/images/btn/btn_event_info_mobile.gif" /></a></p>
<p style="margin: 20px 0 10px 0;"><img alt="INFORMATION" src="/mobile/images/parts/img_title_info_mobile.gif" /></p>
<p style="margin: 0 0 10px 0;"><a href="/mobile/company/index.php">COMPANY</a></p>
<p style="margin: 0 0 10px 0;"><a href="/mobile/recruit/index.php">RECRUIT</a></p>
<p style="margin: 0 0 10px 0;"><a href="/mobile/contact/index.php">CONTACT</a></p>
<p style="margin: 0 0 10px 0;"><a href="/mobile/company/privacy_policy.php">PRIVACY POLICY</a></p>
<p style="margin: 0;"><a href="/mobile/company/sitemap.php">SITEMAP</a></p>
<div align="right" style="margin: 20px 0 0 0;"><a href="#pagetop"><img alt="PageTop" border="0" src="/mobile/images/btn/btn_pagetop_mobile.gif" /></a></div>
<p style="margin: 10px 0 0 0;"><img alt="Copyright(C)2010 YAMADAYA All Rights Reserved." src="/mobile/images/common/img_copyright_mobile.gif" /></p>
</font>

<?php
  $googleAnalyticsImageUrl = googleAnalyticsGetImageUrl();
  echo '<img src="' . $googleAnalyticsImageUrl . '" />';?>
</body>
</html>