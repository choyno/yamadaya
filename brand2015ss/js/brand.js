jQuery.widget("ui.scrubber", {
    options: {
        min: 0,
        max: 100,
        value: 0,
        start: function () {},
        slide: function () {},
        stop: function () {}
    },
    _create: function () {
        var self = this;
        self.track = self.element.find(".ui-scrubber-track");
        self.trackOffset = self.track.offset();
        self.handle = jQuery("<div />", {
            "class": "ui-scrubber-handle"
        });
        self.handle.appendTo(self.track);
        self._bindEvents();
        self.value(self.options.value)
    },
    _bindEvents: function () {
        var self = this;
        self.isTouchDevice = isTouchDevice();
        self.handle.bind("touchstart mousedown", function (event) {
            event.preventDefault();
            if (event.originalEvent.touches && event.originalEvent.touches.length) {
                event = event.originalEvent.touches[0]
            } else {
                if (event.originalEvent.changedTouches && event.originalEvent.changedTouches.length) {
                    event = event.originalEvent.changedTouches[0]
                }
            }
            self.mouseDown = true;
            self._trigger("start", event)
        });
        self[self.isTouchDevice ? "element" : "track"].bind("mousedown touchstart", function (event) {
            event.preventDefault();
            if (event.originalEvent.touches && event.originalEvent.touches.length) {
                event = event.originalEvent.touches[0]
            } else {
                if (event.originalEvent.changedTouches && event.originalEvent.changedTouches.length) {
                    event = event.originalEvent.changedTouches[0]
                }
            }
            self.mouseDown = true;
            self.value(self._normValueFromMouse(event.pageX), true);
            self._trigger("start", event);
            self._trigger("slide", event)
        });
        jQuery(window).bind("mouseup touchend", function (event) {
            if (!self.mouseDown) {
                return
            }
            if (event.originalEvent.touches && event.originalEvent.touches.length) {
                event = event.originalEvent.touches[0]
            } else {
                if (event.originalEvent.changedTouches && event.originalEvent.changedTouches.length) {
                    event = event.originalEvent.changedTouches[0]
                }
            }
            self.mouseDown = false;
            self._trigger("stop", event);
            self._trigger("change", event)
        }).bind("mousemove touchmove", function (event) {
            if (!self.mouseDown) {
                return
            }
            event.preventDefault();
            if (event.originalEvent.touches && event.originalEvent.touches.length) {
                event = event.originalEvent.touches[0]
            } else {
                if (event.originalEvent.changedTouches && event.originalEvent.changedTouches.length) {
                    event = event.originalEvent.changedTouches[0]
                }
            }
            self.value(self._normValueFromMouse(event.pageX), false);
            self._trigger("slide", event)
        }).resize(function () {
            self.trackOffset = self.track.offset()
        })
    },
    _normValueFromMouse: function (x) {
        var self = this;
        return self._trimAlignValue(x - self.trackOffset.left)
    },
    _trimAlignValue: function (val) {
        var self = this;
        if (val < self._valueMin()) {
            return self._valueMin()
        }
        if (val > self._valueMax()) {
            return self._valueMax()
        }
        return val
    },
    value: function (newValue, animate) {
        var self = this;
        if (arguments.length) {
            self.options.value = this._trimAlignValue(newValue);
            self._refreshValue(animate);
            self._trigger("change slide stop")
        }
        return self.options.value
    },
    _valueMin: function () {
        return this.options.min
    },
    _valueMax: function () {
        return this.options.max
    },
    _refreshValue: function (animate) {
        var self, value, valPercent, mouseState, w_clipper;
        var self = this;
        value = this.value();
        valueMin = this._valueMin();
        valueMax = this._valueMax();
        valPercent = (valueMax !== valueMin) ? (value - valueMin) / (valueMax - valueMin) * 100 : 0;
        w_clipper = 1060;
        if (animate) {
            self.handle.stop().animate({
                left: valPercent + "%"
            }, 400, "easeOutCubic")
        } else {
            self.handle.stop().css({
                left: valPercent + "%"
            })
        }
        self.iefix()
    },
    iefix: function () {
        var self, valueMin, valueMax, mouseState, w_clipper;
        self = this;
        valueMin = this._valueMin();
        valueMax = this._valueMax();
        w_clipper = 1060;
        if (jQuery.browser.msie) {
            jQuery(document).ready(function () {
                document.onselectstart = function () {
                    return false
                }
            });
            self.track.mousedown(function () {
                mouseState = true;
                jQuery(this).mouseleave(function () {
                    mouseState = false
                })
            });
            self.track.mouseleave(function () {
                mouseState = false
            });
            self.track.parents(".previews").mouseup(function () {
                mouseState = false
            }).mouseleave(function () {
                mouseState = false
            });
            self.handle.mousedown(function () {
                self.track.parents(".previews").mousemove(function (e) {
                    var position = (e.pageX - ((jQuery(window).width() - w_clipper) / 2)) - 60,
                        max = (position > valueMax) ? valueMax : position;
                    if (mouseState) {
                        self.handle.stop().css({
                            left: ((max < 0) ? 0 : max) + "px"
                        })
                    }
                }).mouseup(function () {
                    mouseState = false;
                    return
                })
            }).mouseup(function () {
                mouseState = false;
                return
            })
        }
    },
    _trigger: function (type, event) {
        var self = this,
            types = type.split(/\s+/);
        if (types.length > 1) {
            jQuery.each(types, function (i, t) {
                self._trigger(t, event)
            });
            return
        }
        switch (type) {
            case "start":
                if (jQuery.isFunction(self.options.start)) {
                    self.options.start(event, {
                        value: self.options.value
                    })
                }
                break;
            case "slide":
                if (jQuery.isFunction(self.options.slide)) {
                    self.options.slide(event, {
                        value: self.options.value
                    }, self.track)
                }
                break;
            case "stop":
                if (jQuery.isFunction(self.options.stop)) {
                    self.options.stop(event, {
                        value: self.options.value
                    })
                }
                break;
            case "change":
                if (jQuery.isFunction(self.options.change)) {
                    self.options.change(event, {
                        value: self.options.value
                    })
                }
                break
        }
    }
});











(function () {
    var root = this,
    	previousUnderscore = root._,
    	breaker = {},
    	ArrayProto = Array.prototype,
        ObjProto = Object.prototype,
        slice = ArrayProto.slice,
    	nativeForEach = ArrayProto.forEach;
        
        var _ = function (obj) {
        return new wrapper(obj)
    };
    if (typeof module !== "undefined" && module.exports) {
        module.exports = _;
        _._ = _
    } else {
        root._ = _
    }
    _.VERSION = "1.1.3";
    var each = _.each = _.forEach = function (obj, iterator, context) {
        var value;
        if (nativeForEach && obj.forEach === nativeForEach) {
            obj.forEach(iterator, context)
        } else {
            if (_.isNumber(obj.length)) {
                for (var i = 0, l = obj.length;
                i < l;
                i++) {
                    if (iterator.call(context, obj[i], i, obj) === breaker) {
                        return
                    }
                }
            } else {
                for (var key in obj) {
                    if (hasOwnProperty.call(obj, key)) {
                        if (iterator.call(context, obj[key], key, obj) === breaker) {
                            return
                        }
                    }
                }
            }
        }
    };

 
    _.extend = function (obj) {
        each(slice.call(arguments, 1), function (source) {
            for (var prop in source) {
                obj[prop] = source[prop]
            }
        });
        return obj
    };

  
    _.isNumber = function (obj) {
        return !!(obj === 0 || (obj && obj.toExponential && obj.toFixed))
    };



})();




window.compare = function (s1, s2, ignoreCase) {
    return (ignoreCase) ? (s1 + "").toLowerCase() === (s2 + "").toLowerCase() : s1 === s2
};





window.isTouchDevice = function () {
    var el = document.createElement("div");
    el.setAttribute("ongesturestart", "return;");
    if (typeof el.ongesturestart == "function") {
        return true
    } else {
        return false
    }
};





AW = {};



AW.core = (function () {
    var listeners = {}, debug = true;
    return {
        notify: function (note, sender, data) {
            if (!listeners[note]) {
                listeners[note] = []
            }
            _.each(listeners[note], function (object, i) {
                object.handler.apply(object.listener, [note, sender, data])
            });
            return
        },
        listen: function (notification, handler, listener) {
            if (notification.split(/\s/).length > 1) {
                _.each(notification.split(/\s/), function (note, i) {
                    AW.core.listen(note, handler, listener)
                });
                return
            }
            if (!listeners[notification]) {
                listeners[notification] = []
            }
            listeners[notification].push({
                handler: handler,
                listener: listener
            });
            return
        },
        log: function (msg, type) {
            if (!debug) {
                return
            }
            if (window.console) {
                switch (type) {
                    case "warn":
                        console.warn(msg);
                        break;
                    case "info":
                        console.info(msg);
                        break;
                    default:
                        console.log(msg);
                        break
                }
            }
            return
        },
        error: function (sev, ex) {
            jQuery.post("/error/logclienterror", {
                name: ex.name,
                description: ex.description,
                message: ex.message,
                lineNumber: ex.lineNumber,
                stackTrace: ex.stack
            });
            if (sev > 1) {
                alert("An error has occurred. Please refresh your browser")
            }
            return
        },
        debug: function (val) {
            debug = val || true;
            if (debug) {
                this.log("debugging...", "info")
            }
        },
        debugOn: function () {
            return !!debug
        }
    }
})();



AW.view = (function () {
    return {
        fn: {},
        define: function (name, prototype) {
            var base = {
                opts: {},
                data: {},
                objects: {},
                delegate: null,
                _createView: function (view, options, data, delegate) {
                    var self = this,
                        defaults = {
                            block: {
                                message: null,
                                overlayCSS: {
                                    backgroundColor: "#fff",
                                    opacity: "0.20"
                                }
                            }
                        };
                    self.view = view;
                    self.delegate = delegate;
                    self.opts = _.extend({}, defaults, self.opts, options);
                    self.data = _.extend({}, data);
                    self.init();
                    return self
                }
            };
            if (!AW.read[name]) {
                AW.read._create(name)
            }
            AW.view.fn[name] = function (view, options, data, delegate) {
                return AW.view.fn[name].impl._createView(view, options, data, delegate)
            };
            AW.view.fn[name].impl = _.extend({}, base, prototype);
            return
        },
        create: function (name, options) {
            args = Array.prototype.slice.call(arguments);
            options = args[args.length - 1];
            args = args.slice(1, args.length - 1);
            args.push(function (response) {
                options.view = response;
                AW.view.register(name, options)
            });
            AW.read[name].apply(this, args);
            return
        },
        register: function (name, options) {
            options = options || {};
            if (!options.view) {
                var stub = name.match(/[A-Z]?[a-z]+/g).join("_").toLowerCase();
                options.view = ".view-" + stub + ", ." + stub
            }
            if (options.modal) {
                if (typeof (options.modal) !== "object") {
                    options.modal = {}
                }
                jQuery.modal(options.view, {
                    appendTo: "#container",
                    opacity: 70,
                    overlayId: "shared_dialog-overlay",
                    containerId: "shared_dialog-container",
                    dataId: "shared_dialog-data",
                    overlayClose: true,
                    maxWidth: options.modal.maxWidth || 300,

                    onClose: function (dialog) {
                        dialog.container.fadeOut(200, function () {
                            dialog.overlay.fadeOut(300, function () {
                                jQuery.modal.close()
                            })
                        })
                    },
                    onOpen: function (dialog) {
                        dialog.overlay.fadeIn(200, function () {
                            var jQuerycontainer = jQuery("<div />");
                            jQuerycontainer.addClass("dialog-data-container").appendTo(dialog.container).append(dialog.data);
                            dialog.container.css({
                                opacity: 0,
                                display: "block"
                            }).height(dialog.data.outerHeight()).add(dialog.data).fadeTo(300, 1)
                        })
                    }
                })
            } else {
                return this.start(name, options)
            }
            return
        },
        start: function (name, options) {
            AW.core.log("trying start: " + name, "info");
            if (AW.core.debugOn()) {
                if (name in AW.view.fn) {
                    return AW.view.fn[name](jQuery(options.view), options.options, options.data, options.delegate)
                } else {
                    AW.core.log("No view with name '" + name + "'", "warn")
                }
                return
            }
            try {
                return AW.view.fn[name](jQuery(options.view), options.options, options.data, options.delegate)
            } catch (ex) {
                AW.core.log(name + " failed to start.");
                AW.core.error(1, ex)
            }
        }
    }
})();







AW.read = (function () {
    return {
        _create: function (name) {
        }        
    }
})();




AW.view.define("brandList", {
    opts: {
        previewWdith: 157,
        sliderWidth: 1050,
        carouselFeaturedItemAttr: {
            opacity: 1
        },
        carouselRegularItemAttr: {
            opacity: 0.3
        },
        carouselFeaturedThumbnailAttr: {
            opacity: 1
        },
        carouselRegularThumbnailAttr: {
            opacity: 0.45
        },
        isNewTemplate: null
    },
    itemCount: 0,
    carouselVisible: false,
    init: function () {
        this.objects();
        this.looks();
        this.previews();
        this.events();
        return
    },
    objects: function () {
        this.objects.looks = this.view.find(".look-set");
        this.objects.previews = this.view.find(".preview-set");
        this.objects.slider = this.view.find(".ui-scrubber-container");
        this.objects.index = this.view.find(".status-index");
        this.objects.status = this.view.find(".status-index");
        this.objects.stage = this.view.find(".stage");
        return
    },
    previews: function () {
        var self = this,
            start = 0,
            w_content, w_clipper, left, mouseDown;
        w_clipper = 1060;
        w_content = this.objects.previews.children().length * this.opts.previewWdith - 2;
        this.objects.previews.width(w_content + 2);
        this.objects.slider.scrubber({
            min: 0,
            max: this.opts.sliderWidth,
            value: start,
            animate: !jQuery.browser.msie,
            slide: function (e, ui, track) {
                left = Math.round(ui.value / self.opts.sliderWidth * (w_clipper - w_content));
                if (jQuery.browser.msie) {
                    track.mousedown(function () {
                        mouseDown = true
                    });
                    self.objects.previews.parents(".previews").mouseup(function () {
                        mouseDown = false
                    });
                    track.mousemove(function (e) {
                        var position = e.pageX - ((jQuery(window).width() - w_clipper) / 2);
                        if (mouseDown) {
                            self.objects.previews.css({
                                left: ("-" + position) + "px"
                            })
                        }
                    }).mouseleave(function () {
                        mouseDown = false;
                        return
                    })
                } else {
                    self.objects.previews.stop(true).animate({
                        left: left + "px"
                    }, (jQuery.browser.msie) ? 0 : 400, "easeOutCubic")
                }
            },
            stop: function (e, ui) {
                left = Math.round(left / self.opts.previewWdith) * self.opts.previewWdith;
                self.objects.previews.stop(true).animate({
                    left: left + "px"
                }, 900, "easeOutCubic")
            }
        });
        return
    },
    looks: function () {
        var self = this,
            prevIndex, index, statusIndex, sliderValue;
        this.itemCount = this.objects.looks.children().length;
        var isNewTemplate = this.objects.stage.hasClass("altTemplate");
        this.opts.isNewTemplate = isNewTemplate;
        this.objects.looks.jcarousel({
            start: 0,
            visible: (isNewTemplate) ? 1 : 1.7,
            scroll: 1,
            wrap: "circular",
            animation: !jQuery.browser.msie,
            itemFallbackDimension: (isNewTemplate) ? 936 : 434,
            initCallback: function (carousel) {
                jQuery(carousel.buttonPrev).add(jQuery(carousel.buttonNext)).click(function (e) {
                    e.preventDefault()
                });
                self.animateItemIn(1, false);
                jQuery(window).bind("keydown", function (e) {
                    if (e.keyCode == 37) {
                        carousel.prev()
                    }
                    if (e.keyCode == 39) {
                        carousel.next()
                    }
                });
            },
            itemVisibleOutCallback: {
                onBeforeAnimation: function (carousel, item, i, state) {
                    if (prevIndex === carousel.prevFirst + 1) {
                        return
                    }
                    index = (self.opts.isNewTemplate) ? carousel.first : carousel.first + 1;
                    prevIndex = carousel.prevFirst + 1;
                    statusIndex = index;
                    if (compare(state, "init", true)) {
                        return
                    }
                    self.animateItemOut(prevIndex, jQuery.browser.msie);
                    self.animateItemIn(index, jQuery.browser.msie);
                    statusIndex = statusIndex % self.itemCount;
                    if (statusIndex < 1) {
                        statusIndex += self.itemCount
                    }
                    self.objects.index.text(statusIndex < 10 ? "0" + statusIndex : statusIndex);
                    sliderValue = Math.round(((statusIndex - 1) / (self.itemCount - 1)) * self.opts.sliderWidth);
                    self.objects.slider.scrubber("value", sliderValue, true);
                    self.objects.status.parent().css({
                        visibility: "visible"
                    })
                }
            }
        });
        return
    },
    events: function () {
        var self = this;
        this.objects.previews.children().each(function (i) {
            var $self = jQuery(this),
                $link = $self.find(".preview-link");
            $self.click(function (e) {
                e.preventDefault();
                jQuery(".jcarousel-prev").add(jQuery(".jcarousel-next")).css({
                    display: "block"
                });
                self.objects.looks.data("jcarousel").scroll((self.opts.isNewTemplate) ? i + 1 : i);
                if (!self.carouselVisible) {
                    self.objects.looks.css({
                        visibility: "visible"
                    }).animate({
                        opacity: 1
                    }, 400);
                    self.carouselVisible = true;
                }
                self.objects.status.add(self.objects.status.siblings()).css({
                    visibility: "visible"
                });
                self.hoverState()
            })
        });
        jQuery("shape").hide();
        return
    },
    animateItemIn: function (index, animate) {
        var self = this;
        jQuery('.view-brand_list .preview').css({'opacity':'0.45'});
        var duration = animate || animate === undefined ? 400 : 0,
            previewIndex = (index - 1) % this.itemCount;
        if (previewIndex < 0) {
            previewIndex += this.itemCount
        }
        var lookOptions = function (self) {
            self.objects.previews.children(":eq(" + previewIndex + ")").animate(self.opts.carouselFeaturedThumbnailAttr, duration, "easeOutCubic");
            var $fullLink = jQuery(self.objects.looks.find(".jcarousel-item-" + index).find(".link-fullscreen"));
            var id = $fullLink.find("span").attr("id");
            var image = $fullLink.attr("id");
        };
        var item = this.objects.looks.find(".jcarousel-item-" + index);
        item.animate(this.opts.carouselFeaturedItemAttr, duration, "easeOutCubic", function () {
        	item.find('.look-hover').hide();
        	self.hoverState();
            lookOptions(self);

            item.find("shape").hide()
        });
        return
    },
    animateItemOut: function (index, animate) {
        var self = this,
            duration = animate || animate === undefined ? 600 : 0,
            previewIndex = (index - 1) % this.itemCount,
            item = this.objects.looks.find(".jcarousel-item-" + index);
        if (previewIndex < 0) {
            previewIndex += this.itemCount
        }
        item.animate(this.opts.carouselRegularItemAttr, duration, "easeOutCubic").find(".look-hover").fadeOut(duration, function () {
            self.hoverState()
        });
        this.objects.previews.children(":eq(" + ((self.opts.isNewTemplate) ? previewIndex - 1 : previewIndex) + ")").animate(this.opts.carouselRegularThumbnailAttr, duration, "easeOutCubic");
        return
    },
    hoverState: function () {
        var self = this;
        this.objects.looks.find(".look-hover").each(function () {
            if (jQuery(this).css("display") == "none") {
                jQuery(this).hover(function () {
                    jQuery(this).parent().find("shape").show()
                }, function () {
                    jQuery(this).parent().find("shape").hide()
                })
            }
        })
    }
});



jQuery(document).ready(function(){
	jQuery('html').addClass('js');
	AW.view.register("brandList",{
    	options: {
        	imageUrlFormat: ""
        }
        });
});
