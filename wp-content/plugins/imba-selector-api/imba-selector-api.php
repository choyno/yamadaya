<?php
/**
 * Plugin Name: Custom API For Area, Prefecture and Store Select
 * Plugin URI: http://ariellopez.teamalphainifinity.com/imba-selector-api
 * Description: Crushing it!
 * Version: 1.0
 * Author: Ariel Lopez
 * Author URI: http://areiellopez.teamalphainifinifty.com
 */


function prefectureList(){

  $area_val = $_GET['area'];
  $areas = array(
         'hokkaido' => 'field_5de625a670b89',
         'tohoku' => 'field_5de628b037e1a',
         'koshinetsu' => 'field_5de6809013855',
         'kitakantou' => 'field_5de699bdbaae3',
         'minamikantou' => 'field_5de69a03baae4',
         'tokai_area' => 'field_5de69a4dbaae5',
         'hokuriku' => 'field_5de69c0117934',
         'kansai' => 'field_5de69d0693ed0',
         'chugoku' => 'field_5de69da129939',
         'shikoku' => 'field_5de69dc8da76f',
         'okinawa' => 'field_5de86e6f15261',
         'kyushu_area' => 'field_5de69e38b6615',
       );

  $area= array();

  foreach($areas as $key => $val) {
    $k = trim($key);
    $area_prefectures = get_field_object($val);

    $area[$k]['area'] = $k;
    $area[$k]['prefectures'] = $area_prefectures['choices'];
  }

  //return $area["kyushu_area"];
  //return $area[$area_val];
  return $area;
}

function storeList(){

  $area_val = $_GET['area'];
  $prefecture_val = $_GET['prefecture'];

  $args = array(
    'post_type' => 'store_detail',
    'posts_per_page' => 9999,
    'order' => 'ASC',
    'meta_query'  => array(
      'relation'    => 'AND',
      array(
        'key'   => 'area',
        'value'   => $_GET['area'],
        'compare'   => '=',
      ),
      array(
        'key'   => 'prefecture',
        'value'   => $_GET['prefecture'],
        'compare'   => '=',
      ),
    ),
  );

	$posts = get_posts($args);
	$data = [];
	$i = 0;

	foreach($posts as $post) {
    $address =  get_post_meta($post->ID)['store_address'][0];
    $brands = get_field('store_brand', $post->ID);
    $new_post_title = str_replace(' ', '', $post->post_title);

    $data[$i]['id'] =  $post->ID;
    $data[$i]['title'] = $new_post_title;
    $data[$i]['address'] =  $address;
    $data[$i]['terms'] = implode("」「", $brands);
    $i++;
  }

  return $data;
}

add_action('rest_api_init', function() {
	register_rest_route('selector/v1', 'store-list', [
		'methods' => 'GET',
		'callback' => 'storeList',
	]);

	register_rest_route('selector/v1', 'prefecture-list', [
		'methods' => 'GET',
		'callback' => 'prefectureList',
	]);
});


