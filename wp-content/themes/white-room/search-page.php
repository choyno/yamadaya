<?php
/**
 * Template Name: 検索
 *
 * @version 1.0.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head prefix="og: http://ogp.me/ns# <?php echo ( is_single() || is_page() ) ? 'fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#' : 'fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#' ?>">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo( 'name' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->


<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>">

	<?php wp_head(); ?>
<!--[if lt IE 9]>
<script src="<?php bloginfo('template_url'); ?>/js/html5.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/css3-mediaqueries.js"></script>

<script src="<?php bloginfo('template_url'); ?>/js/menu.js"></script>
<![endif]-->
<?php wp_head(); ?>
<!--<script src="<?php bloginfo('template_url'); ?>/js/script.js"></script>-->

<?php /*wp_deregister_script('jquery');*/ ?>
<!-- WordPressのjQueryを読み込ませない -->
 
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/slider.css" />

        <!-- JS -->
      
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.coda-slider-2.0.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/slider.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/dataload.js"></script>



<style type="text/css">
<!--
#jquery-sample {
    margin: 10px;
    min-width: 200px;
}
#jquery-sample-ajax {
    margin: 10px;
    padding: 10px;
    height: auto;
    background-color: white;
    border: 1px solid gray;
    border-radius: 10px;
}
}

@media screen and (max-width: 840px) {
#jquery-sample {
    margin: 0px;

}
#jquery-sample-ajax{
margin:0;
}
}
-->
</style>
  
</head>

	<body class="coda-slider-no-js">
	<!--<div id="wrap">-->

<main id="main" role="main">
		

<div id="container" class="hfeed">
	<header id="header">
		<div class="row">
			<div class="col-12">
				<div class="site-branding">
					<h1 class="site-title">
						<?php
						$header_logo = get_theme_mod( 'header_logo' );
						if ( !$header_logo ) {
							$header_logo = get_template_directory_uri() . '/images/common/header-logo.png';
						}
						?>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
							<?php if ( $header_logo ) : ?>
							<img src="<?php echo esc_url( $header_logo ); ?>" alt="<?php bloginfo( 'name' ); ?>" />
							<?php else : ?>
							<?php bloginfo( 'name' ); ?>
							<?php endif; ?>
						</a>
					</h1>
				<!-- end .site-branding --></div>
<div id="center-nav">
		<nav class="global-nav">
					<?php
					wp_nav_menu( array(
						'theme_location' => 'global-nav',
					) );
					?>
				<!-- end .global-nav --></nav>
</div>
				<span id="responsive-btn">MENU</span>
			<!-- end .col-12 --></div>
		<!-- end .row --></div>
	<!-- end #header --></header>
		

	<div id="contents" class="row">

		<div class="col-12">


				<header class="entry-header">
<div id="center-nav">
<?php dynamic_sidebar( 'submenu' ); ?>
</div>
					<?php whiteroom_the_page_heading(); ?>
					<?php whiteroom_the_bread_crumb(); ?>
				<!-- end .entry-header --></header>

<div id="search-wp">
<div id="search-in">

<div class="coda-slider-wrapper">
    <div class="coda-slider preload" id="coda-slider-1">

        <div class="panel">
            <div class="panel-wrapper">
                
                <h2 class="title">STEP１．ブランド選択</h2>
<form method="post" name="form1" id="form1">
<?php
tax_checkbox('brands','checkbox','label');
function tax_checkbox($taxonomy_name,$type,$wrapper){
$taxonomys = get_terms($taxonomy_name);

if(!is_wp_error($taxonomys) && count($taxonomys)):
    foreach($taxonomys as $taxonomy):
        $tax_posts = get_posts(array('post_type' => "store", 'taxonomy' => $taxonomy_name, 'term' => $taxonomy->slug ,'ids' => $taxonomy->ids) );
        if($tax_posts):
?>

<<?php echo $wrapper; ?>><input id="check" type="checkbox" name="order_no[]" value="<?php echo $taxonomy->slug ; ?>"><img src="<?php echo home_url(); ?>/img/logo/<?php echo $taxonomy->slug ; ?>.png" class="search_logo <?php echo $taxonomy->slug ; ?>">（<?php echo $taxonomy->name; ?>）</<?php echo $wrapper; ?>><!--<br>-->

<?php
        endif;
    endforeach;
endif;
}
?>
						<!--<input type="submit" name="btn" id="btn" value="submit">-->
					</form>
 			</div>
        </div>

        <div class="panel">
            <div class="panel-wrapper">
                  <h2 class="title">STEP２．地域を選択</h2>
					<div id="jquery-sample">
        				<span id="jquery-sample-textStatus"></span>
						<div id="jquery-sample-ajax"></div>
					</div>
            </div>
        </div>
       
    </div><!-- coda-slider -->
</div><!-- .coda-slider-wrapper -->


</div><!-- search-wp -->
</div><!-- search-in -->


</div>
 	<!-- end main --></main>


	<aside id="sub">
		<?php get_sidebar(); ?>
	<!-- end sub --></aside>

<?php get_footer(); ?>

