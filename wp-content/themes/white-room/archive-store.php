<?php get_header(); ?>

	<main id="main" role="main">
	
				<header class="entry-header">

					<?php whiteroom_the_page_heading(); ?>
					<?php whiteroom_the_bread_crumb(); ?>
				<!-- end .entry-header --></header>

				<div class="entry-content">



	<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php
$custom_fields = get_post_custom();
$id = get_the_ID();

$stitle = get_post_custom("", '店舗コード');
echo $stitle[0];
 ?>

<p><?php echo $custom_fields["店舗コード"][0]; ?>　【<?php $area = get_the_terms( $id, "area" ); echo $area[0]->name;?>
<?php //echo get_the_term_list($id, "area", "", "　/　",""); ?>
】　<a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a>　【<?php //echo get_the_term_list($id, "brands", "", "　/　","");  ?>
<?php
$terms = get_the_terms( $id, 'brands' );
if ( !empty($terms) ) : if ( !is_wp_error($terms) ) :
?>
<?php
$length = count($terms);     // 追加
$no = 0;
foreach( $terms as $term ) : 
$no++;
?>
<?php echo $term->name; ?>
<?php 
if($no !== $length){    // 変更
        echo "/";
    }
?>
<?php endforeach; ?>
<?php endif; endif; ?>
】</p>

<?php endwhile;  ?>
<?php whiteroom_the_pager(); ?>
<!-- end .entry-content --></div>

</section>
	<!-- end #main --></main>

	<aside id="sub">
		<?php get_sidebar(); ?>
	<!-- end #sub --></aside>

<?php get_footer(); ?>
