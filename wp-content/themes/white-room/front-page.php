<?php
/**
 * @author NetBusinessAgent
 * @version 1.0.2
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head prefix="og: http://ogp.me/ns# <?php echo ( is_single() || is_page() ) ? 'fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#' : 'fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#' ?>">
		<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-3354725-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-3354725-1');
</script>

	<meta name="google-site-verification" content="PpMmaP2-d0SDdWnOA24MHstlaNeXWAcH6ynoMmQ9IVU" />
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo( 'name' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>

	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/owl_carousel/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/owl_carousel/owl-carousel/owl.theme.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/owl_carousel/style.css">


	<script src="<?php bloginfo('template_url'); ?>/owl_carousel/js/jquery-1.11.0.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/owl_carousel/owl-carousel/owl.carousel.min.js"></script>
</head>
<body <?php body_class( get_theme_mod( 'font_family' ) ); ?>>
<?php 
    echo do_shortcode("[metaslider id=11]"); 
?>

<div id="container" class="hfeed">
	<header id="header2">
		<div class="row">
			<div class="col-12">

<div id="center-nav">
		<nav class="global-nav">
<div id="pc-nav">
					<?php
					wp_nav_menu( array(

'walker' => new Walker_Nav_Menu_Test(),
						'theme_location' => 'global-nav'
					) );
					?>
</div>
<div id="sp-nav">
					<?php
					wp_nav_menu( array(

						'theme_location' => 'sp-nav'

					) );?>
</div>
</nav>
<!-- end .global-nav -->
</div>
				<span id="responsive-btn" style="margin-top:15px !important;">MENU</span>
			<!-- end .col-12 --></div>
		<!-- end .row --></div>
	<!-- end #header --></header>

		<div class="col-12">
	<main id="main" role="main">
		
		<?php while ( have_posts() ) : the_post(); ?>
		<div class="entry-content2">
			<?php the_content(); ?>
			<?php
			wp_link_pages( array(
				'before' => '<div class="pager"><p>',
				'after'  => '</p></div>',
			) );
			?>
		<!-- end .entry-content --></div>
		<?php endwhile; ?>
<div class="entry-content">

<script>
/*カルーセル１つ目：ニュース*/
$(document).ready(function() {
$("#owl-news").owlCarousel({
items : 3,
navigation :true, // Show next and prev buttons

slideSpeed : 300,//ページ送りした時のスライドスピード
paginationSpeed : 800,//自動のスライドスピード
autoPlay: 5000,//自動でスライドするスピード。例：5000の場合、5秒
    navigationText: [
      "<i class='icon-chevron-left icon-white'><img src='<?php echo get_template_directory_uri();?>/images/car_top_left3.png'></i>",
      "<i class='icon-chevron-right icon-white'><img src='<?php echo get_template_directory_uri();?>/images/car_top_right3.png'></i>"
      ],
});
});
</script>

<script>
/*カルーセル２つ目：ブランドサイト紹介*/
$(document).ready(function() {
$("#owl-brand").owlCarousel({
items : 3,
navigation :true, // Show next and prev buttons

slideSpeed : 300,//ページ送りした時のスライドスピード
paginationSpeed : 800,//自動のスライドスピード
autoPlay: 5000,//自動でスライドするスピード。例：5000の場合、5秒
    navigationText: [
      "<i class='icon-chevron-left icon-white'><img src='<?php echo get_template_directory_uri();?>/images/car_top_left3.png'></i>",
      "<i class='icon-chevron-right icon-white'><img src='<?php echo get_template_directory_uri();?>/images/car_top_right3.png'></i>"
      ],
});
});
</script>

<h2>NEWS</h2>

<div class="wrapper-width">
<div class="wrapper-with-margin">
<div id="owl-news" class="owl-carousel owl-theme">
 <?php $args = array(
        'numberposts' => 20,                //表示（取得）する記事の数
        'post_type' => 'post'    //投稿タイプの指定
    );
    $posts = get_posts( $args );

    if( $posts ) : foreach( $posts as $post) : setup_postdata( $post ); 

$image_id = get_post_thumbnail_id();
$image_url = wp_get_attachment_image_src($image_id, true);

?>

<div class="carousel_item">
        <div class="carousel_thumbnail"><a href="<?php the_permalink(); ?>"><!--<img src="<?php echo $image_url[0];?>">--><?php 

if(has_post_thumbnail() ):
$out =    the_post_thumbnail('news_thumbnail' ,array('class'=>'carousel_thumbnail'));
else:
$out =    "<img src='".get_template_directory_uri()."/images/noimg200.png' alt='no image'>";
endif;
echo $out;

?>
</a></div>

<div class="center right leadtext">
<p class="ctitle"><?php echo get_the_date(); ?></p>
<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
</div>
</div>
    <?php endforeach; ?>
    <?php else : //記事が無い場合 ?>
       <p>記事はまだありません。</p>
    <?php endif;
    wp_reset_postdata(); ?>


</div><!--owl-demo-->
</div><!--wrapper-with-margin-->
</div><!--wrapper-with-margin-->

<p class="mark margin2"><a href="<?php echo home_url( '/' ); ?>category-feature/">NEWS一覧へ</a></p>


</div><!--entrycontent-->
<div class="bggray">
<div class="entry-content">

<h2>SHOP　BRAND　SITE</h2>

<div class="wrapper-width top_brand_site">
<div class="wrapper-with-margin">
<div id="owl-brand" class="owl-carousel owl-theme">
  <?php $args = array(
        'numberposts' => -1,                //表示（取得）する記事の数
        'post_type' => 'brand'    //投稿タイプの指定
    );
    $posts = get_posts( $args );

    if( $posts ) : foreach( $posts as $post) : setup_postdata( $post ); 

$image_id = get_post_thumbnail_id();
$image_url = wp_get_attachment_image_src($image_id, true);


$terms = get_the_terms("" ,"classify");
//print_r($terms);
//echo $terms[0]->name; 
if($terms[0]->name == "product-brand"):
//echo "ファッションブランド→非表示";
else:
//echo "ストアブランド→表示";
?>

<div class="carousel_item">
        <div class="carousel_thumbnail"><a href="<?php the_permalink(); ?>"><!--<img src="<?php echo $image_url[0];?>">--><?php 

if(has_post_thumbnail() ):
$out =    the_post_thumbnail('brand_thumbnail' ,array('class'=>'carousel_thumbnail'));
else:
$out =    "<img src='".get_template_directory_uri()."/images/noimg200.png' alt='no image'>";
endif;
echo $out;

?>
</a></div>
</div>

<?php endif; ?>


    <?php endforeach; ?>
    <?php else : //記事が無い場合 ?>
       <p>記事はまだありません。</p>
    <?php endif;
    wp_reset_postdata(); ?>


</div><!--owl-demo-->
</div><!--wrapper-with-margin-->
</div><!--wrapper-width-->

<p class="mark margin2"><a href="<?php echo home_url( '/' ); ?>brands-info/">SHOP BRAND SITE一覧へ</a></p>

</div><!--entrycontent-->
</div><!--bggray-->

	<!-- end #main --></main>

	<aside id="sub">
		<?php get_sidebar(); ?>
	<!-- end #sub --></aside>
<?php get_footer(); ?>
