/*
 * WordPress Plugin 'Contact From 7' support confirm phase v0.12
 * http://elearn.jp/wpman/
 *
 * Copyright 2011-2012, Takenori Matsuura
 * Licensed under GPL Version 2 licenses.
 *
 * Date: Thu Dec 16 10:46:00 2012 +0900
 */

var SUBMIT_NAME = '送信';
var CONFIRM_NAME = '確認';
var MODIFY_NAME = '修正';
var SELECT_EMPTY_LABEL = '---';
var SELECT_EMPTY_VALUE = '---';
var MODIFY_BUTTON_ID = 'wpcf7-modify';
var ALERT_MESSAGE_CLASS = 'wpcf7-not-valid-tip';
var EMPTY_MESSAGE = '必須項目に記入もれがあります。';
var INVALIDE_EMAIL_MESSAGE = 'メールアドレスの形式が正しくないようです。';
var DISABLE_SELECT_COLOR = 'color:#000000';

( function($) {
$(document).ready( function () {
	$('select.wpcf7-validates-as-required').each( function () {
		if ( $(this).find( 'option:first' ).text() == SELECT_EMPTY_LABEL ) SELECT_EMPTY_VALUE = $(this).val();
	} );
	$('.wpcf7-form-control-wrap').mouseover( function () {
		$(this).find( '.wpcf7-not-valid-tip:visible' ).fadeOut();
	} );
	if ( $('.wpcf7-submit').val() == SUBMIT_NAME ) {
		$('.wpcf7-submit').val( CONFIRM_NAME );
		$('.wpcf7-form').submit( function () {
			if ( $('.wpcf7-submit').val() == CONFIRM_NAME ) {
				$(this).fadeOut('fast', function () {
					$('span.'+ALERT_MESSAGE_CLASS).remove();
					var validate = true;
					$('input.wpcf7-validates-as-required, textarea.wpcf7-validates-as-required').each( function () {
						if ( jQuery.trim( $(this).val() ) == '' ) {
							br = $(this).get(0).tagName.match( /textarea/i )? '<br />': '';
							$(this).after( '<span class="'+ALERT_MESSAGE_CLASS+'">'+br+EMPTY_MESSAGE+'</span>' );
							validate = false;
						}
					} );
					$('select.wpcf7-validates-as-required').each( function () {
						if ( $(this).children('option:selected').val() == SELECT_EMPTY_VALUE ) {
							$(this).after( '<span class="'+ALERT_MESSAGE_CLASS+'">'+EMPTY_MESSAGE+'</span>' );
							validate = false;
						}
					} );
					$('span.wpcf7-validates-as-required').each( function () { // for checkbox ( and radio? )
						if ( $(this).find('input:checked').length == 0 ) {
							$(this).after( '<span class="'+ALERT_MESSAGE_CLASS+'">'+EMPTY_MESSAGE+'</span>' );
							validate = false;
						}
					} );
					$('.wpcf7-validates-as-email ').each( function () {
						if ( jQuery.trim( $(this).val() ) != '' && !$(this).val().match( /^[A-Za-z0-9]+[\w\.-]+@[A-Za-z0-9]+[\w\.-]+\.\w{2,}$/ ) ) {
							$(this).after( '<span class="'+ALERT_MESSAGE_CLASS+'">'+INVALIDE_EMAIL_MESSAGE+'</span>' );
							validate = false;
						}
					} );
					if ( validate ) {
						$('.wpcf7-form input[type="text"], .wpcf7-form textarea').attr( 'readonly', 'readonly' );
						$('.wpcf7-form select').attr( 'disabled', 'disabled' ).attr( 'style', DISABLE_SELECT_COLOR );
						$('.wpcf7-radio input[type="radio"], .wpcf7-checkbox input[type="checkbox"]').each( function () {
							if ( !$(this).attr("checked") ) {
								$(this).next('span').hide();
								$(this).prev('span').hide();
							}
							$(this).hide();
						} );
						$('.wpcf7-submit').val( SUBMIT_NAME ).before( '<input type="button" name="'+MODIFY_BUTTON_ID+'" id="'+MODIFY_BUTTON_ID+'" value="'+MODIFY_NAME+'" />' );
						$('#'+MODIFY_BUTTON_ID).click( function () {
							$(this).remove();
							$('.wpcf7-form').fadeOut('fast', function () {
								$('.wpcf7-form input[type="text"], .wpcf7-form textarea').removeAttr( 'readonly' );
								$('.wpcf7-form select').removeAttr( 'disabled' ).removeAttr( 'style' );
								$('.wpcf7-radio input[type="radio"], .wpcf7-checkbox input[type="checkbox"]').each( function () {
									$(this).show();
									$(this).next().show();
									$(this).prev().show();
								} );
								$('.wpcf7-submit').val( CONFIRM_NAME );
								$(this).fadeIn('slow');
							} );
						} );
					}
					$(this).fadeIn('slow');
				} );
				return false;
			} else {
				$('.wpcf7-form select').removeAttr( 'disabled' ).removeAttr( 'style' );
			}
			return true;
		} );
	}
} );
} )( jQuery );
