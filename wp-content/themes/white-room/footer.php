<?php
/**
 * @author NetBusinessAgent
 * @version 1.0.0
 */
?>
		<!-- end .col-12 --></div>
	<!-- end #contents --></div>
	<footer id="footer">
		<div class="row">
			
			<div class="footer-widget-area col-12">
				<!--<div class="row">-->
<div class="footer-w-wrapper">
<div class="footer-w flexbox">

<div class="footer-content m-item">
<?php dynamic_sidebar('footer1');?>
</div>
<div class="footer-content m-item">
<?php dynamic_sidebar('footer2');?>
</div>

<div class="footer-content m-item">
<?php dynamic_sidebar('footer3');?>
</div>

<div class="footer-content m-item">
<?php dynamic_sidebar('footer4');?>
</div>

<div class="footer-content m-item listlarge ">
<?php dynamic_sidebar('footer5');?>
</div>
<div class="footer-content m-item listlarge ">
<?php dynamic_sidebar('footer6');?>
</div>

</div>

			

</div>
				<!-- end .row --></div>
			<!-- end .col-12 --></div>
		<!-- end .row </div>-->

		<div class="copyright">
			<div class="row">
				<p class="col-12">
					<?php echo apply_filters( 'whiteroom_copyright', sprintf( 'Copyright &copy; %s  All Rights Reserved.', get_bloginfo( 'name' ) ) ); ?>
					
			<!-- end .row --></div>
		<!-- end .copyright --></div>
	<!-- end #footer --></footer>
<!-- end #container --></div>


<?php wp_footer(); ?>
</body>
</html>