<?php
/** Template Name: 会社インデックスページ
 * @author NetBusinessAgent
 * @version 1.0.0
 */
get_header(); ?>

<main id="main" role="main">
		<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
<div id="center-nav">
<?php dynamic_sidebar( 'submenu' ); ?>
</div>

				<?php whiteroom_the_page_heading(); ?>
					<?php whiteroom_the_bread_crumb(); ?>
				<!-- end .entry-header --></header>

				<div class="entry-content">

<?php
$i="0";
$cf_group = SCF::get('ボックス要素');

foreach ($cf_group as $field_name => $field_value ):
$i++;


if($i%3 == 1):
$wrapper = '<div class="box_wrapper">';
else:
$wrapper = '';
endif;

if($i%3 == 0):
$wrapper_end = '</div>';
else:
$wrapper_end = '';
endif;
?>

<?php //echo $i."<br/>";?>
<?php echo $wrapper;?>
<?php //print_r($field_value['リンクのターゲット']); ?>

<div class="box ratio-1_1 <?php if(! $field_value['タイトル']){echo "disp-none";} ?>">
    <div class="inner subindex">
 <div class="table"> <div class="table-in">
<a href="<?php echo $field_value['URL']; ?>" <?php if($field_value['リンクのターゲット']){echo 'target="_blank"';} ?>>　　</a>
<p class="menutitle"><?php echo $field_value['タイトル']; ?></p>
<?php echo $field_value['説明']; ?>
    </div></div>
    </div>
</div>

<?php //echo $wrapper_end;?>
<?php endforeach;?>

					<?php the_content(); ?>
					<?php
					wp_link_pages( array(
						'before' => '<div class="pager"><p>',
						'after'  => '</p></div>',
					) );
					?>
				<!-- end .entry-content --></div>
					
				<div class="entry-meta hidden">
					<?php whiteroom_posted_on(); ?>
				<!-- end .entry-meta --></div>
			<!-- end .hentry --></article>
			<?php
			if ( comments_open() || '0' != get_comments_number() ) {
				comments_template();
			}
			?>
		<?php endwhile; ?>
	<!-- end #main --></main>


	<aside id="sub">
		<?php get_sidebar(); ?>
	<!-- end #sub --></aside>

<?php get_footer(); ?>