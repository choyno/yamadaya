
<?php import_template('parts/renewal-head'); ?>
<?php import_template('parts/renewal-banner'); ?>

<div class="l-container">
  <?php import_template( 'parts/common-section/breadcrumbs', array(
    'paths' => ["TOP", "RECRUIT TOP", "Q&A"],
  )); ?>

  <div class="faq">
    <?php import_template('parts/section-heading', array(
      'modifier' => 'l-section-heading-faq',
      'title' => 'Q&A',
      'text' => 'question and answer',
    )); ?>

      <?php $custom_field = get_post_custom(); ?>

      <section class="faq-block">
        <h3 class="faq-header"><span class="faq-header-text"><?php echo the_title() ?></span></h3>

        <?php
          $faq_count = count($custom_field["faq_question"]);
          $i = 0;
          while( $i < $faq_count){ $i++;
        ?>

         <?php $faq_index = $i-1; ?>
         <?php $faq_question = $custom_field["faq_question"][$faq_index]; ?>
         <?php $faq_answer = $custom_field["faq_answer"][$faq_index]; ?>

          <div class="faq-content">
            <h4 class="faq-question"><span class="faq-question-label">Q:</span>
              <?php echo $faq_question ?>
              <span class="faq-question-icon"></span>
            </h4>
            <div class="faq-answer">
              <div class="faq-answer-inner">
                <span class="faq-answer-label">A.</span>
                <p class="faq-answer-copy">
                  <?php echo $faq_answer ?>
                </p>
              </div>
            </div>
          </div>

        <?php
          }
        ?>

      </section>

  </div>
</div>
<?php import_template('parts/renewal-content'); ?>
<?php import_template('parts/renewal-copyright'); ?>
<?php wp_footer(); ?>
