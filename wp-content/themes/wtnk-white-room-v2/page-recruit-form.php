<?php
/**
  * Template Name: Recruit Form
 */
?>

<?php import_template('parts/renewal-head'); ?>
<?php import_template('parts/renewal-banner'); ?>

<?php  $field_areas = get_field_object('field_5de6255e70b88'); ?>

<div class="l-container">
<?php
  $breadcrumbs = array (
    'breadcrumbs' => array (
      array (
        'title' => '採用応募フォーム',
        'link' => resolve_url('recruit-form'),
      ),
    )
  );
  import_template( 'parts/common-section/breadcrumbs', $breadcrumbs);
?>
</div>

<?php import_template('parts/section-heading', array(
  'modifier' => 'l-section-heading-recruit-form',
  'title' => '採用応募フォーム',
  'text' => 'entry form',
)); ?>

<div class="l-container">
  <div class="recruit-form">

    <?php while (have_posts()) : the_post(); ?>
      <?php echo do_shortcode($post->post_content); ?>
    <?php endwhile; ?>

  </div> <!-- recruit-form -->
</div>

<?php import_template('parts/renewal-copyright'); ?>
<?php wp_footer(); ?>
