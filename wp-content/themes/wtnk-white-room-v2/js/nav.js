$(document).ready(function(){
  const BREAKPOINT_MOBILE = 875;
  const $navItem = $('.js-renewal-nev-item');
  const $hamburger = $('.js-hamburger');
  const sublist = '.renewal-nav-sublist';
  const $nav = $('.js-nav');
  const IS_ACTIVE = 'is-active';
  const IS_MOBILE = window.matchMedia(`(max-width: ${BREAKPOINT_MOBILE}px)`).matches;


  if (IS_MOBILE) {

    $navItem.each(function(){
      if($(this).children(sublist).length > 0) {
        $(this).append('<span class="renewal-nav-caret js-renewal-nav-caret"></span>') 
      }
    })

    $('.js-renewal-nav-caret').on('click', function(e) {
      e.preventDefault();
      $(this).toggleClass(IS_ACTIVE);
      $(this).siblings(sublist).slideToggle(300);
    })


  } else {
    $navItem.each(function(){
      if($(this).children(sublist).length > 0) {
        $(this).children('.renewal-nav-link').append('<span class="renewal-nav-caret"></span>') 
      }
    })
  }

  $hamburger.on('click', function(e){
    e.preventDefault();
    $(this).toggleClass(IS_ACTIVE)
    $nav.toggleClass(IS_ACTIVE)
  })


})