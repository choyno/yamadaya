$(document).ready(function() {
  let $recruitImage = $('.renewal-recruit-image-wrap');
  let $recruitContent = $('.renewal-recruit-content');
  let ctrl = new ScrollMagic.Controller();

  $recruitImage.each((i, e) => {
    let $elem = $(e);

    new ScrollMagic.Scene({
      triggerElement: $elem,
      triggerHook: 0.2,
      duration: '150%'
    })
    .setTween($elem, 4, {y:'-5%', ease:Power0.easeNone}, 0)
    .addTo(ctrl);
  })

  $recruitContent.each((i, e) => {
    let $elem = $(e);

    new ScrollMagic.Scene({
      triggerElement: '.renewal-recruit',
      triggerHook: 0.2,
      duration: '150%'
    })
    .setTween($elem, 5, {y:'5%', ease:Power0.easeNone}, 0)
    .addTo(ctrl);
  })


})