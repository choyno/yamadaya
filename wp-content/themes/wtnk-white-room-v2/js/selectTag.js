$(document).ready(function(){

  const $document = $(document),
    IS_ACTIVE = 'is-active';

  for(i=15; i<=99; i++) {
    $('.recuit-form-age').append($('<option></option>').val(i).html(i))
  }

  //repopulate selectated select values
  $('#js-age-select').attr('data-parent', 'js-age');


  $('select').each((index, elem) =>{

    let $this = $(elem);
    let $dropdownOption = $this.find('option');

    $this.wrap('<div class="select js-select"></div>');

    $('<ul class="select-list js-select-list"></ul>').insertAfter($this);

    let $selectList = $this.siblings('.js-select-list');

    $('<span class="select-text js-select-text"></span>').insertAfter($this);

    let $selectText = $this.siblings('.js-select-text');

    $selectText.text($dropdownOption.eq(0).text());

    for( let i= 0; i < $dropdownOption.length; i++) {
      let $area_val = $dropdownOption.eq(i).val();
      $("<li class='select-item js-select-item' data-val='"+ $area_val +"'>"+ $dropdownOption.eq(i).text() +'</li>').appendTo($selectList);
    }

    let $selectItem = $('.js-select-item');

    $selectItem.unbind().click(function(e){
      $(e.currentTarget).parents('.recruit-form-select-wrap').find('.recruit-data-hidden').val($(e.currentTarget).text());
      $(e.currentTarget).parent('.select-list').siblings('select').val($(e.currentTarget).text());
      $(e.currentTarget).parent('.select-list').siblings('.js-select-text').text($(e.currentTarget).text());
      $this.val($(e.currentTarget).text());
      $current_area_val = $(e.currentTarget).attr('data-val');

      //get the option base on unique parent
       $current_option = $(e.currentTarget).parent('.select-list').siblings('.select');
       $current_option.val($current_area_val);
       $current_option_parent = $current_option.attr('data-parent');
       $current_option_val = $current_option.val();
       $js_parent_selector = $('#'+$current_option_parent);


      if($current_option_parent == "js-age"){
        $('#js-age-txt').val($current_option_val);
      }

      if($current_option_parent == "js-educational-attainment"){
        $('#js-educational-attainment-txt').val($current_option_val);
      }
      if($current_option_parent == "js-desired-job"){
        $('#js-desired-job-txt').val($current_option_val);
      }

      //get request for prefecture value and specific select field
      $area_holder = ["js-desired-work-1", "js-desired-work-2", "js-desired-work-3"];
      if($area_holder.indexOf($current_option_parent) >= 0){

        $current_option_text = $(e.currentTarget).text();

        $js_parent_selector = $('#'+$current_option_parent);
        $js_parent_selector.find('.area-txt').val($current_option_text);
        $prefectureList = JSON.parse(window.customSearchObj)[$current_option_val.trim()]["prefectures"];

        //assign parent
        $js_parent_selector.find('.js-prefecture-options-list').find('.js-select-text').attr('data-parent', $current_option_parent);
        $js_parent_selector.find('.js-store-options-list').find('.js-select-text').attr('data-parent', $current_option_parent);

        populatePrefecture($current_option_parent, $prefectureList, $current_option_val, $current_option_text);
      }
    });
  });

  $('.recruit-data-hidden').each(function(){
    if($(this).val()) {
      $(this).parents('.recruit-form-select-wrap').find('.js-select-text').text($(this).val());
    }
  })

  function populatePrefecture(current_option_parent, prefectureList, area_val, area_text) {
    $js_parent_selector = $("#"+current_option_parent);

    $selectedPrefecture = '';
    let $selectList = $js_parent_selector.find('.js-prefecture-options-list').find('.js-select-list');
    let $selectPrefectureList = $js_parent_selector.find('.js-prefecture-options-list').find('.js-prefecture-options');

    $selectPrefectureList.html('');
    $js_parent_selector.find('.js-prefecture-options-list').find('.select-list').html('');

    //$('<li class="select-item js-select-item" data-val="">都道府県を選択</li>').appendTo($selectList);

    //Populate the prefecture list
    $.each(prefectureList, function (value, label) {
      $('<option value="'+ value  +'" >'+ label +'</option>').appendTo($selectPrefectureList);
      $('<li class="select-item js-select-prefecture-item" data-val="'+ value  +'" >'+ label +'</li>').appendTo($selectList);
    });

    //option item select and assign
    let $selectPrefectureItem =  $js_parent_selector.find('.js-prefecture-options-list').find('.js-select-prefecture-item');
    $selectPrefectureItem.on('click', function(e){

      $current_option = $(e.currentTarget).parent('.select-list').siblings('.select');
      $reselect_js_option_parent = $("#" +$current_option.attr('data-parent'));


      $reselect_js_option_parent.find('.js-prefecture-options-list').find('.js-select-text').text($(e.currentTarget).text());
      $selectedPrefecture = $(e.currentTarget).data('val');

      $reselect_js_option_parent.find('.js-prefecture-options-list').val($selectedPrefecture);
      $prefecture_val = $js_parent_selector.find('.js-prefecture-options-list').val();
      $prefecture_text = $js_parent_selector.find('.js-prefecture-options-list').text();

      $reselect_js_option_parent.find('.prefecture-txt').val($(e.currentTarget).text());
      populateStore(area_val, $prefecture_val, current_option_parent);
    });
  }

  function populateStore(area_val, prefecture_val, current_option_parent) {

    $js_parent_selector = $("#"+current_option_parent);
    let $selectList = $js_parent_selector.find('.js-store-options-list').find('.js-select-list');
    let $selectStoreList = $js_parent_selector.find('.js-prefecture-store-list').find('.js-store-options');

    if(location.hostname != 'localhost'){
      $url = '/wp-json/selector/v1/store-list?area='+ area_val +'&prefecture='+ prefecture_val;
    }else{
      $url = '/wtnk-yamadaya/wp-json/selector/v1/store-list?area='+ area_val +'&prefecture='+ prefecture_val;
    }

    $selectStoreList.html('');
    $selectList.html('');
    $js_parent_selector.find('.js-store-options-list').find('.js-select-text').text("店舗を選択"); //reset

    $.ajax({
    url: $url,
    type: "GET",
    dataType: "json",
      success: function (data) {
       if(data.length != 0){

         $.each(data, function (key, value) {
           //console.log(value["title"]);

           $title =   (value["title"] + '「' + value["terms"] +'」');
           $('<option value="'+ $title  +'" >'+ $title +'</option>').appendTo($selectStoreList);
           $('<li class="select-item js-select-store-item" data-val="'+ $title  +'" >'+ $title + '</li>').appendTo($selectList);
         });

         //option item select and assign store
         let $selectStoreItem =  $js_parent_selector.find('.js-store-options-list').find('.js-select-store-item');
         $selectStoreItem.on('click', function(e){

           $current_option = $(e.currentTarget).parent('.select-list').siblings('select');
           console.log($current_option.attr('data-parent'));
           $reselect_js_option_parent = $("#" +$current_option.attr('data-parent'));

           $reselect_js_option_parent.find('.js-store-options-list').find('.js-select-text').text($(e.currentTarget).text());

           $selectedPrefecture = $(e.currentTarget).data('val');

           $reselect_js_option_parent.find('.js-store-options-list').val($selectedPrefecture);
           $store_val = $reselect_js_option_parent.find('.js-store-options-list').val();
           //console.log($store_val); //assing this to the hide store fields

           $reselect_js_option_parent.find('.store-txt').val($store_val);
          //$(this).find('.js-store-options-list').find('input').val($(e.currentTarget).text());
         });
       }
      },
      error: function (error) {
          console.log(`Error ${error}`);
      }
    });

  }

  $('.recruit-form').find('form').addClass('h-adr');

  $('.js-select-text').on('click', (event) =>{
    $(event.currentTarget).parents('.js-select').toggleClass(IS_ACTIVE);
    event.stopPropagation();
    // $('.js-select').removeClass(IS_ACTIVE);
  });

  $document.on('click', () =>{
    $('.select').removeClass(IS_ACTIVE);
  });

  let $js_age = $('#js-age-txt').val();
  if($js_age != ""){
    $("#js-age").find(".js-select-text").text($js_age);
  }

  let $js_educational_attainment = $('#js-educational-attainment-txt').val();
  if($js_educational_attainment != ""){
    $("#js-educational-attainment").find(".js-select-text").text($js_educational_attainment);
  }

  let $js_desired_job = $('#js-desired-job-txt').val();
  if($js_desired_job != ""){
    $("#js-desired-job").find(".js-select-text").text($js_desired_job);
  }
})
