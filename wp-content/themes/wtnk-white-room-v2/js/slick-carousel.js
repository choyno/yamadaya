$(document).ready(function(){
  $('.js-working-place-gallery').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    infinite: true,
    centerMode: true,
    responsive: [
      {
        breakpoint: 1260,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2
        }
      }
    ]
  });

  $('.js-working-place-gallery').slickLightbox({
    itemSelector: '.working-place-gallery-anchor',
    navigationByKeyboard: true
  });
})