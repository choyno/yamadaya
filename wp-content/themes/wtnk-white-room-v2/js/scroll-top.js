$(document).ready(function(){
    $('#js-scrollTop').on('click', (e)=> {
    e.preventDefault();
    TweenMax.to($('html, body'), 1, {
      scrollTop: 0,
      ease: Expo.easeInOut
    });
  });
})