$(document).ready(function(){
  const IS_ACTIVE = 'is-active';

  $('.faq-question').on('click', function(e){
    e.preventDefault();
    $(this).toggleClass(IS_ACTIVE);
    $(this).next().toggleClass(IS_ACTIVE);
    $(this).next().slideToggle(300);
  })
})