$(document).ready(function(){

    if(location.hostname != 'localhost'){
      $prefecture_url = '/wp-json/selector/v1/prefecture-list';
    }else{
      $prefecture_url = '/wtnk-yamadaya/wp-json/selector/v1/prefecture-list';
    }

    $new_area = {};
    $.ajax({
    url: $prefecture_url,
    type: "GET",
    dataType: "json",
      success: function (data) {
         window.customSearchObj = JSON.stringify(data);

  const $source = document.querySelector('#full-name-field');
  const $result = document.querySelector('#mvp-full-name');

  const $document = $(document),
    IS_ACTIVE = 'is-active';

  let $prefectureList = {};
  let searchParams = new URLSearchParams(window.location.search)
  let paramsArea = searchParams.get('area');

  generateAreaContainer();
  function generateAreaContainer() {
    $('#js-area-options').each((index, elem) =>{

      $('#js-area-options').val(paramsArea);
      let $this = $(elem);
      let $selectItem = $('.js-select-area-item');


      $selectItem.on('click', function(e){
        $(e.currentTarget).parent('.area-select-list').siblings('.js-select-text').text($(e.currentTarget).text());
        $(e.currentTarget).parents('.js-select-area').children('select').val($(e.currentTarget).data('val'));
        $selectedArea =   $('#js-area-options').val();

        $prefectureList = JSON.parse(window.customSearchObj)[$selectedArea.trim()]["prefectures"];

        populatePrefecture($prefectureList);

        $this.val($(e.currentTarget).text());
        $('#js-area-options').val($selectedArea);
        $('#js-area-val').val($selectedArea);
        $('#js-area-label').val($(e.currentTarget).text());

        //For prefecture load the first prefecture
        $prefectureValue =  $('#js-prefecture-val').val();
        $prefectureValueLabel =  $('#js-prefecture-label').val();
        $prefectureFirstData = Object.keys($prefectureList)[0];
        $('#js-prefecture-options option:selected').val($prefectureFirstData);
        $prefectureFirstDataLabel = $('#js-prefecture-options option:selected').text();

        $('.prefecture-select-text').html($prefectureFirstDataLabel);
        $('#js-prefecture-val').val($prefectureFirstData);
        $('#js-prefecture-label').val($prefectureFirstDataLabel);
        //end prefecture assign
      });

    });
  }


  function populatePrefecture(prefectureList) {
    $selectedPrefecture = '';
    $('#js-prefecture-options').each((index, elem) =>{
      let $this = $(elem);
      let $selectList = $('.js-select-prefecture-list');
      let $selectPrefectureList = $('#js-prefecture-options');

      $(".prefecture-select-list").html('');
      $selectPrefectureList.html('');

      $.each(prefectureList, function (value, label) {
        $('<option value="'+ value  +'" >'+ label +'</option>').appendTo($selectPrefectureList);
        $('<li class="select-item js-select-prefecture-item" data-val="'+ value  +'" >'+ label +'</li>').appendTo($selectList);
      });

      let $selectPrefectureItem = $('.js-select-prefecture-item'); 

      $selectPrefectureItem.on('click', function(e){
        $(e.currentTarget).parent('.prefecture-select-list').siblings('.js-select-text').text($(e.currentTarget).text());
        $selectedPrefecture = $(e.currentTarget).data('val');

        $this.val($(e.currentTarget).text());
        $('#js-prefecture-options').val($selectedPrefecture);
        $('#js-prefecture-val').val($selectedPrefecture);
        $('#js-prefecture-label').val($(e.currentTarget).text());
        generateAreaContainer();
      });
    });
  }

  function populateSearchResult() {
    $area_value =   $('#js-area-val').val();
    $prefecture_value =  $('#js-prefecture-val').val();
    $area_value_label =   $('#js-area-label').val();
    $prefecture_value_label =  $('#js-prefecture-label').val();


    $prefectureList = JSON.parse(window.customSearchObj)[$area_value.trim()]["prefectures"];


    populatePrefecture($prefectureList);
    $('#js-area-options').val($area_value);
    $('#js-prefecture-options').val($prefecture_value);

    $('.area-select-text').html($area_value_label);
    $('.prefecture-select-text').html($prefecture_value_label);
  }

  function triggerSelect() {
    $('.js-select-area').on('click', (event) =>{
      $(event.currentTarget).toggleClass(IS_ACTIVE);

      event.stopPropagation();
    });

    $document.on('click', () =>{
      $('.select-area').removeClass(IS_ACTIVE);
    });

    $('.js-select-prefecture').on('click', (event) =>{
      $(event.currentTarget).toggleClass(IS_ACTIVE);

      event.stopPropagation();
    });

    $document.on('click', () =>{
      $('.select-prefecture').removeClass(IS_ACTIVE);
    });
  }

  populateSearchResult();
  triggerSelect();
      },
      error: function (error) {
        console.log(`Error ${error}`);
      }
    });


})












