<?php
/**
 * Template Name: Recruit University Training
 * @author NetBusinessAgent
 * @version 1.0.2
 */
?>

<?php import_template('parts/renewal-head'); ?>
<?php import_template('parts/renewal-banner', array(
  'modifier' => 'renewal-banner-recruit'
)); ?>

<div class="l-container">
<?php
  $breadcrumbs = array (
    'breadcrumbs' => array (
      array (
        'title' => 'RECRUIT TOP',
        'link' => resolve_url('recruit-top'),
      ),
      array (
        'title' => 'ヤマダヤの研修',
        'link' => resolve_url('recruit-university-training'),
      )
    )
  );
  import_template( 'parts/common-section/breadcrumbs', $breadcrumbs);
?>

  <div class="training">
    <?php import_template( 'parts/section-heading', array(
      'modifier' => 'l-section-heading-training',
      'title' => 'ヤマダヤの研修',
      'text' => 'training', 
    )); ?>

    <div class="training-prelude">
      <p class="training-prelude-text">ヤマダヤでは「人が人を育てる」環境をいかし、
      個性をいかしながら活躍する「人」を育てる研修を行っています。
      </p>
    </div>
    
    <ul class="training-type">
      <li class="training-type-item">
        <a class="training-type-anchor" href="#university-grad">新卒（大学院・大学・短大・専門卒）の方向け</a>
      </li>
      <li class="training-type-item">
        <a class="training-type-anchor" href="#highschool-grad">新卒（高校卒）の方向け</a>
      </li>
      <li class="training-type-item">
        <a class="training-type-anchor" href="#employment">中途入社の方向け（社員登用）</a>
      </li>
    </ul>

    <section class="training-process" id="university-grad">
      <h3 class="training-heading"><span class="training-heading-text">新卒（大学院・大学・短大・専門卒）の方向け</span></h3>

      <p class="training-label training-arrow">内定者</p>
      
      <div class="training-wrapper training-arrow">
        <div class="training-col">
          <p class="training-process-label">アルバイト研修</p>
        </div>
        <div class="training-col">
          <p class="training-process-copy">各自のスケジュールに合わせて、アルバイトでのOJT研修</p>
        </div>
      </div>
      
      <div class="training-wrapper training-arrow">
        <div class="training-col training-col-label">
          <p class="training-process-label">9月 内定者懇親会</p>
          <p class="training-process-copy">内定者・先輩スタッフ・人事との交流。
ワークや座談会をとおして会社のこと、仕事のことをより知る。</p>
        </div>
        <div class="training-col">
           <img class="training-col-image" src="<?php echo get_template_directory_uri() . '/images/renewal/university-meeting-01.jpg'?>" alt="">
        </div>
      </div>
      
      <div class="training-wrapper training-arrow">
        <div class="training-col training-col-label">
          <p class="training-process-label">11月 内定式（内定者集会）</p>
        </div>
        <div class="training-col">
          <p class="training-process-copy">職場の雰囲気、入社後について深く知り、仕事の理解をする
          内定者アルバイトの振り返りや目標づくり
          （集会後、懇親会パーティ）</p>
        </div>
      </div>
      
      <div class="training-wrapper training-arrow">
        <div class="training-col training-col-label">
          <p class="training-process-label">年明け 内定者研修</p>
        </div>
        <div class="training-col">
          <ul>
            <li>社会人としての基本を身につける（共通 講義研修）</li>
            <li>FAとして基本を身につけるグループワーク研修</li>
            <li>オンラインとしての基本を身につける研修</li>
          </ul>
        </div>
      </div>

      <p class="training-label">入社</p>
      <div class="training-wrapper training-duo-arrow">
        <p class="training-process-label training-process-center">フレッシュマン</p>
      </div>

      <div class="training-wrap">
        <div class="training-section-col">
          <div class="training-wrapper">
            <p class="training-process-label training-process-center">FA</p>
          </div>
          <div class="training-row training-arrow">
            <p class="training-process-label">新人研修 </p>
            <p class="training-process-copy">研修受け入れ店舗におけるOJT研修。店舗運営業務を行いながら、接客の基本や運営業務の基礎を身につける。</p>
          </div>
          <div class="training-row training-arrow">
            <p class="training-process-label">FMミーティング <span>（夏）</span></p>
            <p class="training-process-copy">本社にて行う全体研修。ディスカッションや接客ロールプレイングを行い、接客の基本や運営業務の基礎を身につける。</p>
          </div>
          <div class="training-process-image training-arrow">
            <img src="<?php echo get_template_directory_uri() . '/images/renewal/university-meeting-02.jpg'?>" alt="">
            <img src="<?php echo get_template_directory_uri() . '/images/renewal/university-meeting-03.jpg'?>" alt="">
          </div>
          <div class="training-row">
            <p class="training-process-label">FAミーティング <span>（2年目・4月）</span></p>
            <p class="training-process-copy">本社にて行う全体研修。ディスカッションや接客ロールプレイングを行い、接客の基本や運営業務の基礎をより掘り下げながら、顧客づくりを目指す。
            ※2年目以降の研修は、スキルアップ、キャリア形成のためなど人に合わせて、必要に応じて行う。</p>
          </div>
        </div>

        <div class="training-section-col">
          <div class="training-wrapper">
            <p class="training-process-label training-process-center">オンラインスタッフ</p>
          </div>
          <div class="training-row training-arrow">
            <p class="training-process-label">新人研修 <span>（入社後約1週間）</span></p>
            <p class="training-process-copy">オンラインの仕事の方向性についての理解。基本的な業務内容を身につける。</p>
          </div>
          <div class="training-row training-arrow">
            <p class="training-process-label">ブランド配属 <span>（仮配属）</span></p>
            <p class="training-process-copy">業務の先輩のサポートを受けながら、配属ブランドでの仕事を覚え、身につける。</p>
          </div>

          <div class="training-row">
            <p class="training-process-label">面談 <span>（6月ごろ）</span></p>
            <p class="training-process-copy">約3カ月を振り返り、最終配属ブランドの決定。</p>
          </div>
          <div class="training-wrapper">
            <div class="training-col">
              <p class="training-process-label">店舗研修 <span>（未定）</span></p>
              <p class="training-process-copy">ヤマダヤのお客様をより具体的に知り、店舗の仕事、ヤマダヤの接客を理解する。</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="training-process" id="highschool-grad">
      <h3 class="training-heading"><span class="training-heading-text">新卒（高校卒）の方向け</span></h3>
      <p class="training-label training-arrow">4月　入社 FM（フレッシュマン）</p>

      <div class="training-wrapper">
        <div class="training-col training-col-label">
          <p class="training-process-label training-process-nowrap">新人研修 <span>（4月〜）</span></p>
        </div>
        <div class="training-col">
          <p class="training-process-copy">研修受け入れ店舗におけるOJT研修。（2カ月以上）
          店舗運営業務を行いながら、接客の基本や運営業務の基本を身につける。
          ※2年目以降の研修は、スキルアップ、キャリア形成のためなど人に合わせて、必要に応じて行う。</p>
        </div>
      </div>
    </section>

    <section class="training-process" id="employment">
      <h3 class="training-heading"><span class="training-heading-text">中途入社の方向け（社員登用）</span></h3>
      <p class="training-label training-arrow">4月　入社</p>

      <div class="training-wrapper training-arrow">
        <div class="training-col training-col-label">
          <p class="training-process-label">アルバイト雇用での試験期間最短4カ月</p>
        </div>
        <div class="training-col">
          <p class="training-process-copy">研修受け入れ店舗におけるOJT研修。店舗運営業務を行いながら、接客の基本や運営業務の基本を身につける。</p>
        </div>
      </div>

      <p class="training-label">社員登用</p>
    </section>

    <?php import_template('parts/renewal-button', array(
      'modifier' => 'renewal-button-training',
      'target' => '_blank',
      'url' => 'https://job.mynavi.jp/21/pc/search/corp38851/outline.html',
      'text' => 'ENTRY'
    )); ?>

  </div>
</div>

<?php import_template('parts/renewal-content'); ?>
<?php import_template('parts/renewal-copyright'); ?>
<?php wp_footer(); ?>
