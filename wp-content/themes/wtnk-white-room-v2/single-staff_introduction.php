<?php
/**
  * Template Name: Interview
 */
?>

<?php
  $custom_fields = get_post_custom();
?>

<?php import_template('parts/renewal-head'); ?>
<?php import_template('parts/renewal-banner'); ?>

<div class="member">
  <div class="l-container">
    <?php
      $breadcrumbs = array (
        'breadcrumbs' => array (
          array (
            'title' => 'RECRUIT TOP',
            'link' => resolve_url('recruit-top'),
          ),
          array (
            'title' => 'スタッフ紹介',
            'link' => resolve_url('staff'),
          ),
          array (
            'title' => '社員インタビュー',
          )
        )
      );
      import_template( 'parts/common-section/breadcrumbs', $breadcrumbs);
    ?>
  </div>

  <?php import_template( 'parts/section-heading', array(
    'modifier' => 'l-section-heading-interview',
    'title' => '社員インタビュー',
    'text' => 'interview',
  )); ?>

  <?php 
      $interview_cover_image_id = ($custom_fields["cover_image"][0]);
      $interview_cover_image= (wp_get_attachment_image_src( $interview_cover_image_id, 'inteview_banner' )[0]);
  ?>
  <div class="member-banner" style="background-image: url(<?php echo $interview_cover_image; ?>)"></div>
  <div class="l-container">
    <?php
        $interview_job_position = $custom_fields["job_position"][0];
        $interview_last_name = $custom_fields["last_name"][0];
        $interview_joined_year = $custom_fields["joined_year"][0];
    ?>

      <div class="member-info">
        <span class="member-position"><?php echo $interview_job_position; ?></span>
        <span class="member-name"><?php echo $interview_last_name; ?></span>
        <span class="member-years"><?php echo $interview_joined_year; ?></span>
      </div>

    <?php
      $interview_images_count = count($custom_fields["interview_image"]);
      $i = 0;
      while( $i < $interview_images_count): $i++;
        $interview_index = $i - 1;

        $interview_base_title = $custom_fields["interview_base_title"][$interview_index];
        $interview_title = $custom_fields["interview_title"][$interview_index];
        $interview_description = $custom_fields["interview_description"][$interview_index];
        $interview_image_id = ($custom_fields["interview_image"][$interview_index]);
        $interview_image= (wp_get_attachment_image_src( $interview_image_id, 'member_interview_image' )[0]);
    ?>
       <section class="member-interview-topics">
        <div class="member-interview-image" style="background-image: url(<?php echo $interview_image; ?>)"></div>
        <div class="member-interview-content">
          <span class="member-interview-subtitle"><?php echo $interview_base_title; ?></span>
          <h3 class="member-interview-title"><?php echo $interview_title; ?></h3>
          <p class="member-interview-text"><?php echo $interview_description; ?></p>
        </div>
      </section>
    <?php endwhile; ?>
  </div>
</div>


<div class="schedule">
  <div class="l-container">
    <h2 class="schedule-heading">業務内容・1日のスケジュール</h2>
    <ul class="schedule-list">
      <?php
        $schedules_count = count($custom_fields["schedule_time"]);
        $i = 0;
        while( $i < $schedules_count): $i++;
          $schedule_index = $i-1;
          $schedule_title = $custom_fields["schedule_time"][$schedule_index];
          $schedule_description = $custom_fields["schedule_description"][$schedule_index];
      ?>
        <li class="schedule-item">
          <time class="schedule-time" date="<?php echo($schedule_title); ?>"><?php echo($schedule_title); ?></time>
          <span class="schedule-detail"><?php echo($schedule_description); ?></span>
        </li>
      <?php endwhile; ?>
    </ul>

    <?php import_template('parts/renewal-button', array(
      'modifier' => '',
      'url' => resolve_url('recruit-form'),
      'text' => 'ENTRY',
    )); ?>
  </div>
</div>

<?php import_template('parts/member-interview', array(
  'modifier' => 'member-interview-detail'
)); ?>
<?php import_template('parts/renewal-content'); ?>
<?php import_template('parts/renewal-copyright'); ?>
<?php wp_footer(); ?>