<?php
/**
 * Template Name: FAQ
 * @author NetBusinessAgent
 * @version 1.0.2
 */
?>
<?php import_template('parts/renewal-head'); ?>
<?php import_template('parts/renewal-banner'); ?>

<div class="l-container">
<?php
  $breadcrumbs = array (
    'breadcrumbs' => array (
      array (
        'title' => 'RECRUIT TOP',
        'link' => resolve_url('recruit-top'),
      ),
      array (
        'title' => 'Q&A',
        'link' => resolve_url('faq'),
      )
    )
  );
  import_template( 'parts/common-section/breadcrumbs', $breadcrumbs);
?>

  <div class="faq">

    <?php import_template('parts/section-heading', array(
      'modifier' => 'l-section-heading-faq',
      'title' => 'Q&A',
      'text' => 'question and answer',
    )); ?>

    <?php
      $values = get_field('field_5deb2619db455');

      if($values)
      {
       foreach($values as $value)
        {
    ?>

      <section class="faq-block">
        <h3 class="faq-header">
          <span class="faq-header-text">
          <?php echo $value["faq_title"]; ?>
          </span>
        </h3>

        <?php
          foreach($value['faq_topic_details'] as $value2)
          {
        ?>
          <div class="faq-content">
            <h4 class="faq-question"><span class="faq-question-label">Q:</span>
              <?php echo $value2["faq_question"]; ?>
              <span class="faq-question-icon"></span>
            </h4>
            <div class="faq-answer">
              <div class="faq-answer-inner">
                <span class="faq-answer-label">A.</span>
                <p class="faq-answer-copy">
                    <?php echo $value2["faq_answer"]; ?>
                </p>
              </div>
            </div>
          </div>
        <?php
          }
        ?>
      </section>
    <?php
        }
      }
    ?>

  </div>
</div>

    <?php import_template('parts/renewal-button', array(
      'modifier' => 'renewal-button-faq',
      'url' => resolve_url('recruit-form'),
      'text' => 'ENTRY',
    )); ?>
<?php import_template('parts/renewal-content'); ?>
<?php import_template('parts/renewal-copyright'); ?>
<?php wp_footer(); ?>
