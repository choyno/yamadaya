<?php
if ( empty( $modifier ) ) { $modifier = ''; }
if ( empty( $caption ) ) { $caption = ''; }
if ( empty( $imgUrl ) ) { $imgUrl = ''; }
if ( empty( $title ) ) { $title = ''; }
if ( empty( $url ) ) { $url = ''; }
if ( empty( $text ) ) { $text = ''; }
?>

<section class="renewal-recruit <?php echo $modifier; ?>">
  <div class="renewal-recruit-image-wrap">
    <span class="renewal-recruit-image" style="background-image: url('<?php echo $imgUrl; ?>') "></span>
  </div>
  <div class="renewal-recruit-content">
    <span class="renewal-recruit-caption"><?php echo $caption; ?></span>
    <h3 class="renewal-recruit-heading"><span>#</span><?php echo $title; ?></h3>
    <a class="renewal-recruit-link" href="<?php echo $url; ?>"><?php echo $text; ?></a>
  </div>
</section> 