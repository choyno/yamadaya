<?php
if ( empty( $modifier ) ) { $modifier = ''; }
if ( empty( $title ) ) { $title = ''; }
if ( empty( $text ) ) { $text = ''; }
?>

<div class="section-heading <?php echo $modifier; ?>">
  <h2 class="section-heading-title"><?php echo $title; ?></h2>
  <span class="section-heading-text"><?php echo $text; ?></span>
</div>