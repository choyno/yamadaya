<div class="renewal-nav-wrapper js-nav">
  <nav class="renewal-nav">
    <ul class="renewal-nav-list">
      <li class="renewal-nav-item js-renewal-nev-item"><a class="renewal-nav-link" href="https://www.ymdy.co.jp/company/">COMPANY</a>
        <ul class="renewal-nav-sublist">
          <!-- <li class="renewal-nav-subitem"><a class="renewal-nav-sublink" href="javascript:;">皆様へのご挨拶</a></li> -->
          <li class="renewal-nav-subitem"><a class="renewal-nav-sublink" href="https://www.ymdy.co.jp/company/company-profile/" target="_blank">会社概要</a></li>
          <li class="renewal-nav-subitem"><a class="renewal-nav-sublink" href="https://www.ymdy.co.jp/company/top-message/" target="_blank">経営理念</a></li>
          <li class="renewal-nav-subitem"><a class="renewal-nav-sublink" href="https://www.ymdy.co.jp/company/history/" target="_blank">沿革</a></li>
          <!-- <li class="renewal-nav-subitem"><a class="renewal-nav-sublink" href="javascript:;">役員紹介</a></li> -->
        </ul>
      </li>
      <li class="renewal-nav-item js-renewal-nev-item"><a class="renewal-nav-link" href="https://www.ymdy.co.jp/shopsearch/">SHOP LIST</a>
        <ul class="renewal-nav-sublist">
          <li class="renewal-nav-subitem"><a class="renewal-nav-sublink" href="https://www.ymdy.co.jp/brands-info/" target="_blank">ヤマダヤの店舗ブランド</a></li>
          <li class="renewal-nav-subitem"><a class="renewal-nav-sublink" href="https://www.ymdy.co.jp/shopsearch/" target="_blank">店舗を探す</a></li>
        </ul>
      </li>
      <li class="renewal-nav-item js-renewal-nev-item"><a class="renewal-nav-link" href="https://www.ymdy.co.jp/category-feature/">NEWS</a></li>
      <li class="renewal-nav-item js-renewal-nev-item"><a class="renewal-nav-link" href="https://www.ymdy.co.jp/brands-info/">BRAND</a></li>
      <li class="renewal-nav-item js-renewal-nev-item"><a class="renewal-nav-link" href="<?php echo resolve_url('recruit-top')?>">RECRUIT</a>
      </li>
      <li class="renewal-nav-item js-renewal-nev-item"><a class="renewal-nav-link" href="https://www.ymdy.co.jp/company/contact/">CONTACT</a>
        <ul class="renewal-nav-sublist">
           <li class="renewal-nav-subitem"><a class="renewal-nav-sublink" target="_blank" href="https://www.ymdy.co.jp/company/contact/">各種お問い合わせ</a></li>
           <li class="renewal-nav-subitem"><a class="renewal-nav-sublink" target="_blank" href="https://www.ymdy.co.jp/member-service/">メンバーズサービス</a></li>
           <li class="renewal-nav-subitem"><a class="renewal-nav-sublink" target="_blank" href="https://www.ymdy.co.jp/privacy-policy/">プライバシーポリシー</a></li>
        </ul>
      </li>
      <li class="renewal-nav-item js-renewal-nev-item"><a class="renewal-nav-link" href="http://store.ymdy.co.jp/fs/ymdy/c/top">ONLINE SHOP <span class="renewal-nav-shop-icon"></span></a></li>
    </ul>
  </nav>
</div>
