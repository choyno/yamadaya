<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head prefix="og: http://ogp.me/ns# <?php echo ( is_single() || is_page() ) ? 'fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#' : 'fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#' ?>">
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-3354725-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-3354725-1');
</script>

  <meta name="google-site-verification" content="PpMmaP2-d0SDdWnOA24MHstlaNeXWAcH6ynoMmQ9IVU" />
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
  <title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo( 'name' ); ?></title>
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <!--[if lt IE 9]>
  <script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <?php wp_head(); ?>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/renewal.css">
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
  <script src="<?php bloginfo('template_url'); ?>/js/TweenMax.min.js"></script>
  
  <script src="<?php bloginfo('template_url'); ?>/js/select.js"></script>
  <script src="<?php bloginfo('template_url'); ?>/js/nav.js"></script>
  <script src="<?php bloginfo('template_url'); ?>/js/scroll-top.js"></script>
  <script src="<?php bloginfo('template_url'); ?>/js/accordion.js"></script>

  <?php if(is_page('renewal')) {?>
    <script src="<?php bloginfo('template_url'); ?>/js/ScrollMagic.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/animation.gsap.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/TimelineMax.min.js"></script>
  <?php }?>

  <?php if(is_page('recruit-form')) {?>
    <script src="<?php bloginfo('template_url'); ?>/js/selectTag.js"></script>
  <?php }?>

  <?php if(is_page('working-place')) {?>
    <script src="<?php bloginfo('template_url'); ?>/js/slick.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/slick-carousel.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/slick-lightbox.js"></script>
  <?php }?>
  
  <!-- <script src="<?php bloginfo('template_url'); ?>/js/parallax.js"></script>  -->
</head>
<body>


<?php import_template( 'parts/renewal-header'); ?>
