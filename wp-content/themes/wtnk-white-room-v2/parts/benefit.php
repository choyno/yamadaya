<div class="benefit">
  <dl class="benefit-row">
    <dt class="benefit-col benefit-label">賞与</dt>
    <dd class="benefit-col">年2回（7月・12月）</dd>
  </dl>
  <dl class="benefit-row">
    <dt class="benefit-col benefit-label">昇給</dt>
    <dd class="benefit-col">年1回（3月）</dd>
  </dl>
  <dl class="benefit-row">
    <dt class="benefit-col benefit-label">昇格</dt>
    <dd class="benefit-col">年1回（3月）</dd>
  </dl>
  <dl class="benefit-row">
    <dt class="benefit-col benefit-label">休日・休暇</dt>
    <dd class="benefit-col">シフト制（月9日公休、土日祝 取得可）、年間休日108日、夏季休暇 最大10日、冬期休暇 最大5日、月1回の3連休取得可、有給休暇、介護休暇、慶弔休暇</dd>
  </dl>
  <dl class="benefit-row">
    <dt class="benefit-col benefit-label">産休育休制度</dt>
    <dd class="benefit-col benefit-direction-row">
      <div class="benefit-content">
        休暇や復帰に向けて、期間や働き方などについて、人事とコミュニケーションをとり、希望を聞きながら決めていきます。法令に基づき
        <ul class="benefit-list">
          <li class="benefit-item">産前産後休暇（産前6週間〜産後8週間まで）</li>
          <li class="benefit-item">育児休暇(産後8週間〜1歳の誕生日の前日まで※延長化)</li>
        </ul>
        取得人数： <br>
        2018年度 35人（40人中） ／2019年度 31人（35人中）
      </div>
      <div class="benefit-graph">
        <div class="benefit-graph-pie benefit-graph-88"><span>取得率<br>88％</span></div>
        <div class="benefit-graph-pie benefit-graph-95"><span>復帰率<br>95％</span></div>
      </div>
    </dd>
  </dl>
  <dl class="benefit-row">
    <dt class="benefit-col benefit-label">時短勤務社員制度</dt>
    <dd class="benefit-col">ライフスタイルに合わせて時短社員・パートなど働き方を選択できます。</dd>
  </dl>
  <dl class="benefit-row">
    <dt class="benefit-col benefit-label">社会保険完備</dt>
    <dd class="benefit-col">健康保険、厚生年金保険、雇用保険、労働災害保険</dd>
  </dl>
  <dl class="benefit-row">
    <dt class="benefit-col benefit-label">定期健康診断</dt>
    <dd class="benefit-col">あり</dd>
  </dl>
  <dl class="benefit-row">
    <dt class="benefit-col benefit-label">待遇</dt>
    <dd class="benefit-col">社員割引制度（30~50%OFF）、報奨金制度、ノルマなし</dd>
  </dl>
  <dl class="benefit-row">
    <dt class="benefit-col benefit-label">社員寮 メゾン101</dt>
    <dd class="benefit-col benefit-direction-row">
      名古屋に完備。マンション1棟丸ごとが社員寮で、部屋はワンルーム、全101室あります。
      <div class="benefit-image" style="background-image: url(<?php echo get_template_directory_uri() . '/images/renewal/workplace/building-sideview.jpg'?>)"></div>
    </dd>
  </dl>
</div>
