<?php
if ( empty( $modifier ) ) { $modifier = ''; }
?>


<div class="renewal-banner <?php echo $modifier?>">
  <div class="renewal-banner-content">
    <span class="renewal-banner-caption">YAMADAYA RECRUIT</span>
    <h2 class="renewal-banner-slogan"><span>「 DRESS UP </span><span>YOUR LIFE 」</span></h2>
    <p class="renewal-banner-copy">ここであなたの人生はもっと輝く</p>
  </div>
</div>