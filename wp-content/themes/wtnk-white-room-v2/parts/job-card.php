<?php
if ( empty( $modifier ) ) { $modifier = ''; }
if ( empty( $tags ) ) { $tags = []; }
if ( empty( $heading ) ) { $heading = ''; }
if ( empty( $address ) ) { $address = ''; }
if ( empty( $category ) ) { $category = ''; }
if ( empty( $url ) ) { $url = 'javascript:;'; }
if ( empty( $linktext ) ) { $linktext = ''; }
?>

<section class="job-card <?php echo $modifier?>">
  <div class="job-card-tags">
    <?php foreach ($tags as $tag) { ?>
       <span class="job-card-tag"><?php echo $tag ?></span>
    <?php } ?>
  </div>
  <h3 class="job-card-heading"><?php echo $heading?></h3>
  <p class="job-card-address"><?php echo $address?></p>
  <p class="job-card-category">応募職種：<?php echo $category?></p>
  <a class="job-card-link" href="<?php echo $url?>"><?php echo $linktext?></a>
</section>