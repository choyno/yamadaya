<div class="essentials">
  <ul class="essentials-list">
    <li class="essentials-item">
      <a class="essentials-link" style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/essential-img01.jpg'?>')" href="https://www.ymdy.co.jp/20170824-01/" target="_blank">
        <span>CLOTH</span>
        生地づくり
      </a>
    </li>
    <li class="essentials-item">
      <a class="essentials-link" style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/essential-img02.jpg'?>')" href="https://www.ymdy.co.jp/20171007-1/" target="_blank">
        <span>CLOTHES</span>
        服づくり
      </a>
    </li>
    <li class="essentials-item">
      <a class="essentials-link" style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/essential-img03.jpg'?>')" href="https://www.ymdy.co.jp/yamadaya%e3%80%8c%e5%b7%9d%e4%b8%8a%e3%81%8b%e3%82%89%e5%b7%9d%e4%b8%8b%e3%81%be%e3%81%a7%e3%80%8d%e7%ac%ac%e4%b8%89%e5%bc%be/" target="_blank">
        <span>COLLECTION</span>
        コレクション
      </a>
    </li>
  </ul>
</div>