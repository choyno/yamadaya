
<header class="renewal-header">
  <div class="renewal-header-logo">
    <a class="renewal-header-logo-link" href="<?php echo resolve_url('renewal')?>"><img src="<?php echo get_template_directory_uri() . '/images/renewal/logo.png'?>" alt=""></a>
  </div>
  <div class="hamburger js-hamburger">
    <span class="hamburger-line-wrap">
      <span class="hamburger-line hamburger-line-one"></span>
      <span class="hamburger-line hamburger-line-two"></span>
      <span class="hamburger-line hamburger-line-three"></span>
    </span>
  </div>
  <?php import_template( 'parts/nav'); ?>
</header>