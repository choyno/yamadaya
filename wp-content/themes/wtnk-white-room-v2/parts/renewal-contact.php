<div class="renewal-contact">
  <div class="renewal-contact-content">
    <?php import_template( 'parts/section-heading', array(
      'modifier' => 'section-heading-contact',
      'title' => 'お問い合わせ',
      'text' => 'contact'
    )); ?>
    <ul class="renewal-contact-list">
      <li class="renewal-contact-item"><a class="renewal-contact-link" href="https://www.ymdy.co.jp/company/contact/" target="_blank"><span class="renewal-contact-icon"></span>各種お問い合わせはコチラ</a></li>
      <li class="renewal-contact-item"><a class="renewal-contact-link" href="https://store.ymdy.co.jp/fs/ymdy/c/contact" target="_blank"><span class="renewal-contact-icon"></span>オンラインサイトの
      お問い合わせはコチラ</a></li>
    </ul>
  </div>
</div>