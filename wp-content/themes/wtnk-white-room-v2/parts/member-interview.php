<?php
if ( empty( $modifier ) ) { $modifier = ''; }
?>

<div class="member-interview <?php echo $modifier?>">
  <div class="l-container">
    <div class="member-interview-top">
      <h2 class="member-interview-heading">MEMBER INTERVIEW</h2>
<!--       <ul class="member-interview-designation">
        <li class="member-interview-designation-item">ファッションアドバイザー </li>
        <li class="member-interview-designation-item">バイヤー </li>
        <li class="member-interview-designation-item">専門職 </li>
        <li class="member-interview-designation-item">本部スタッフ</li>
      </ul> -->
    </div>

    <div class="member-interview-card">
    <?php
      $args = array(
        'post_type' => 'staff_introduction',
        'posts_per_page' => 8,
        'order' => 'ASC',
      );

      $index = 0;
      $query = new WP_Query($args);
      while($query->have_posts()) : $query->the_post();
    ?>

      <?php $interview_item_index = $index + 1; ?>
      <?php $post = get_post_custom(); ?>

      <?php import_template('parts/card', array(
        'modifier' => '',
        'label' => $post['job_position'][0],
        'imgUrl' => get_the_post_thumbnail_url(get_the_ID(), 'member_card_image'),
        'title' => 'INTERVIEW0' . $interview_item_index,
        'interviewUrl' => get_permalink(),
        'text' => $post['interview_page_title'][0] . '<span>(' .$post['interview_name'][0] . ')</span>'
      )); ?>

      <?php $index++; ?>
    <?php endwhile; wp_reset_query(); ?>

    </div>
  </div>
</div>
