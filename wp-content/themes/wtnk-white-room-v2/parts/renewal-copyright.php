<div class="renewal-copyright">
  <ul class="renewal-copyright-list">
    <li class="renewal-copyright-item"><a href="https://apps.apple.com/jp/app/ymdy-members/id1102726501" target="_blank" class="renewal-copyright-link"><img src="<?php echo get_template_directory_uri() . '/images/renewal/app_store.png'?>" alt=""></a></li>
    <li class="renewal-copyright-item"><a href="https://play.google.com/store/apps/details?id=jp.co.ymdy.members&hl=ja&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" target="_blank" class="renewal-copyright-link"><img src="<?php echo get_template_directory_uri() . '/images/renewal/play_store.png'?>" alt=""></a></li>
  </ul>
  <span class="renewal-copyright-copy">Copyright ©YAMADAYA Inc</span>
  <span class="renewal-copyright-scrollTop <?php echo (is_page('renewal')) ? 'is-renewal' : '' ?>" id="js-scrollTop">TOP</span>
</div>