<?php
  if ( empty( $for) ) { $for = ''; }
  if ( empty( $label ) ) { $label = ''; }
  if ( empty( $value ) ) { $value = ''; }
  if ( empty( $name ) ) { $name = ''; }
  if ( empty( $checked ) ) { $checked = ''; }
?>

 <div class="checkbox-wrapper">
   <label class="checkbox-label" for="<?php echo $for; ?>">
     <input type="checkbox" id="<?php echo $for; ?>" value="<?php echo $value; ?>"  name="<?php echo $name; ?>" class="checkbox-input" <?php echo $checked; ?> />
     <div class="checkbox-indicator"></div>
     <span class="checkbox-text-wrapper">
        <?php echo $label; ?>
     </span>
   </label>
 </div>
