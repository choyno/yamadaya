<?php
if ( empty( $modifier ) ) { $modifier = ''; }
?>

<div class="renewal-content <?php echo $modifier?>">
  <div class="l-container">
    <?php import_template( 'parts/section-heading', array(
      'modifier' => 'l-section-heading-content',
      'title' => 'コンテンツ',
      'text' => 'content',
    )); ?>

    <?php if(is_page('recruit-new-grad')):?>
      <a href="<?php echo resolve_url('recruit-internship')?>" class="renewal-content-graduate">
        <span class="renewal-content-graduate-label">新卒採用インターンシップ</span>
        <p class="renewal-content-graduate-copy">ヤマダヤについて知りたい方、
        アパレル業界の仕事に興味のある方は
        ぜひご参加ください。</p>
      </a>
    <?php endif; ?>

    <a href="<?php echo resolve_url('top-message')?>" class="renewal-content-citation">
      <span class="renewal-content-label">TOP MESSAGE</span>
      <blockquote class="renewal-content-quote">
        <p class="renewal-content-citation-copy">ファッションの世界は、
        この先もっと広がっていく。</p>
        <span class="renewal-content-citation-author">代表取締役社長　山田太郎</span>
      </blockquote>
    </a>

    <ul class="renewal-content-list">
      <li class="renewal-content-item">
        <a class="renewal-content-link" style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/content-img-01.jpg'?>')" href="<?php echo resolve_url('company-data')?>">
          <span>DATA</span>
          データ
        </a>
      </li>
      <li class="renewal-content-item">
        <a class="renewal-content-link" style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/content-img-02.jpg'?>')" href="<?php echo resolve_url('career-plan')?>">
          <span>CAREER</span>
          キャリア
        </a>
      </li>
      <li class="renewal-content-item">
        <a class="renewal-content-link" style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/content-img-03.jpg'?>')" href="<?php echo resolve_url('recruit-university-training')?>">
          <span>TRAINING</span>
          研修
        </a>
      </li>
      <li class="renewal-content-item">
        <a class="renewal-content-link" style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/content-img-04.jpg'?>')" href="<?php echo resolve_url('working-place')?>">
          <span>WORK LOCATION/<br>WELFARE</span>
          勤務地・福利厚生
        </a>
      </li>
      <li class="renewal-content-item">
        <a class="renewal-content-link" style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/content-img-05.jpg'?>')" href="<?php echo resolve_url('staff')?>">
          <span>STAFF</span>
          スタッフ紹介
        </a>
      </li>
      <li class="renewal-content-item">
        <a class="renewal-content-link" style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/content-img-06.jpg'?>')" href="<?php echo resolve_url('faq')?>">
          <span>QUESTION AND <br>ANSWER </span>
          Q&A
        </a>
      </li>
    </ul>
  </div>
</div>

