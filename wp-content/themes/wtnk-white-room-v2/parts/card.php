<?php
if ( empty( $modifier ) ) { $modifier = ''; }
if ( empty( $label ) ) { $label = ''; }
if ( empty( $imgUrl ) ) { $imgUrl = ''; }
if ( empty( $title ) ) { $title = ''; }
if ( empty( $text ) ) { $text = ''; }
if ( empty( $interviewUrl ) ) { $interviewUrl = ''; }
?>

<a href="<?php echo $interviewUrl?>" class="card <?php echo $modifier?>">
  <span class="card-label"><?php echo $label?></span>
  <div class="card-image-wrap">
    <span class="card-image" style="background-image: url('<?php echo $imgUrl?>')"></span>
  </div>
  <h3 class="card-title"><?php echo $title?></h3>
  <p class="card-text"><?php echo $text?></p>
</a>
