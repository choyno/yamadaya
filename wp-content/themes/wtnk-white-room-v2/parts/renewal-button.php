<?php
if ( empty( $modifier ) ) { $modifier = ''; }
if ( empty( $url ) ) { $url = ''; }
if ( empty( $text ) ) { $text = ''; }
if ( empty( $target ) ) { $target = ''; }
?>

<div class="renewal-button <?php echo $modifier; ?>">
  <a class="renewal-button-link" target="<?php echo $target; ?>" href="<?php echo $url; ?>"><?php echo $text; ?><span class="renewal-button-arrow"></span></a>
</div>