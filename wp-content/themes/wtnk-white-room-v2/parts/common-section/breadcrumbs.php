<div class="breadcrumbs">
  <ul class="breadcrumbs-list">
    <li class='breadcrumbs-item'> <a href="<?php echo resolve_url('renewal') ?>">TOP</a><span class='breadcrumbs-arrow'></span></li>
    <?php
      $count_bread = count($breadcrumbs);
      foreach($breadcrumbs as $key => $crumb):
      $link  = !empty($crumb['link']) ? $crumb['link'] : '';
      $title = !empty($crumb['title']) ? $crumb['title'] : '';
      ?>
      <li class='breadcrumbs-item'> <a class="breadcrumbs-link" href="<?php echo $link; ?>"><?php echo $title; ?></a><span class='breadcrumbs-arrow'></span></li>
      <?php endforeach; ?>
  </ul>
</div>
