<?php
if ( empty( $modifier ) ) { $modifier = ''; }
if ( empty( $title ) ) { $title = ''; }
?>

<div class="common-content-title-wrapper">
  <h3 class="common-content-base-title <?php echo $modifier; ?>">
    <?php echo $title; ?>
  </h3>
</div>
