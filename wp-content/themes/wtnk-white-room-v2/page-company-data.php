<?php
/**
  * Template Name: company data
 */
?>

<?php import_template('parts/renewal-head'); ?>
<?php import_template('parts/renewal-banner'); ?>

<div class="l-container">
  <?php
    $breadcrumbs = array (
      'breadcrumbs' => array (
        array (
          'title' => 'データ',
          'link' => resolve_url('company-data'),
        )
      )
    );
    import_template( 'parts/common-section/breadcrumbs', $breadcrumbs); ?>
</div>

<?php import_template('parts/section-heading', array(
  'modifier' => 'l-section-heading-recruit-form',
  'title' => 'データ',
  'text' => 'data',
)); ?>


<div class="l-container">
  <div class="data">
    <ul class="data-list">
      <li class="data-item">
        <h3 class="data-title">創業した年</h3>
        <p class="data-copy">なんと明治26年！
        120年以上の歴史があるのです。</p>
        <div class="data-image data-img-one">
          <img src="<?php echo get_template_directory_uri() . '/images/renewal/data/data-img01.png'?>">
        </div>
      </li>
      <li class="data-item">
        <h3 class="data-title">全国の店舗数</h3>
        <p class="data-copy">北から南まで。
        全国各地で働くことができます。</p>
        <div class="data-image data-img-two">
          <img src="<?php echo get_template_directory_uri() . '/images/renewal/data/data-img02.png'?>">
        </div>
      </li>
      <li class="data-item">
        <h3 class="data-title">従業員の男女比</h3>
        <p class="data-copy">ファッションアドバイザーを中心に、多くの女性が活躍中。</p>
        <div class="data-image data-img-three">
          <img src="<?php echo get_template_directory_uri() . '/images/renewal/data/data-img03.png'?>">
        </div>
      </li>
      <li class="data-item">
        <h3 class="data-title">従業員数</h3>
        <p class="data-copy">ファッションが大好きな社員がたくさん！</p>
        <div class="data-image data-img-four">
          <img src="<?php echo get_template_directory_uri() . '/images/renewal/data/data-img04.png'?>">
        </div>
      </li>
      <li class="data-item">
        <h3 class="data-title">平均年齢</h3>
        <p class="data-copy">若い感性をどんどん取り入れていますよ。</p>
        <div class="data-image data-img-five">
          <img src="<?php echo get_template_directory_uri() . '/images/renewal/data/data-img05.png'?>">
        </div>
      </li>
      <li class="data-item">
        <h3 class="data-title">有給の取得率</h3>
        <p class="data-copy">有給を使用して夏季休暇（最大10連休）・冬期休暇（最大5連休）取得も。</p>
        <div class="data-image data-img-six">
          <img src="<?php echo get_template_directory_uri() . '/images/renewal/data/data-img06.png'?>">
        </div>
      </li>
      <li class="data-item">
        <h3 class="data-title">産休育休取得率</h3>
        <p class="data-copy">年々取得する方が増えています。</p>
        <div class="data-image data-img-seven">
          <img src="<?php echo get_template_directory_uri() . '/images/renewal/data/data-img07.png'?>">
        </div>
      </li>
      <li class="data-item">
        <h3 class="data-title">産休育休からの <br class="show-pc">
        復職率</h3>
        <p class="data-copy">ライフスタイルに合わせて復帰。</p>
        <div class="data-image data-img-eight">
          <img src="<?php echo get_template_directory_uri() . '/images/renewal/data/data-img08.png'?>">
        </div>
      </li>
      <li class="data-item">
        <h3 class="data-title">女性管理職の割合</h3>
        <p class="data-copy">チームのリーダーとして男女共に活躍。</p>
        <div class="data-image data-img-nine">
          <img src="<?php echo get_template_directory_uri() . '/images/renewal/data/data-img09.png'?>">
        </div>
      </li>
    </ul>

    <div class="data-award">
      <div class="data-award-image" style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/workplace/award.png'?>')"></div>
      <div class="data-award-content">
        <h3 class="data-award-title">Forbes JAPAN 
        WOMEN AWARD 2018 入賞</h3>
        <p class="data-award-copy">「意欲ある女性が働きやすい環境づくりを積極的に行っている企業」を表彰
        するForbes JAPAN WOMEN AWARD 2018に選出されました！</p>
      </div>
      
    </div>
  </div>
</div>

<?php import_template('parts/renewal-button', array(
  'modifier' => '',
  'url' => resolve_url('recruit-form'),
  'text' => 'ENTRY',
)); ?>

<?php import_template('parts/renewal-content'); ?>
<?php import_template('parts/renewal-copyright'); ?>
<?php wp_footer(); ?>