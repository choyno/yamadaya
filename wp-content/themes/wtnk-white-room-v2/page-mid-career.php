<?php
/**
 * Template Name: Recruit Midcareer
 * @author NetBusinessAgent
 * @version 1.0.2
 */
?>

<?php
  //if(isset($_GET)) {
  //  echo "<pre>";
  //  print_r($_GET);
  //  echo "</pre>";
  //}
?>

<?php import_template('parts/renewal-head'); ?>
<?php import_template('parts/renewal-banner', array(
  'modifier' => 'renewal-banner-recruit'
)); ?>


<div class="l-container">
<?php
  $breadcrumbs = array (
    'breadcrumbs' => array (
      array (
        'title' => 'RECRUIT TOP',
        'link' => resolve_url('recruit-top'),
      ),
      array (
        'title' => '中途採用',
        'link' => resolve_url('recruit-midcareer'),
      )
    )
  );
  import_template( 'parts/common-section/breadcrumbs', $breadcrumbs);
?>

 <?php import_template( 'parts/section-heading', array(
   'modifier' => 'l-section-heading-partime',
   'title' => '中途採用',
   'text' => 'recruit midcareer',
 )); ?>

  <section class="recruit-partime">
    <form class="recruit-partime-form" method="GET" action="">
      <h6 class="recruit-partime-title">勤務地から探す</h6>

      <?php
        $search_area = $_GET['search']['area'];
        if ( empty($search_area) ) $search_area = 'hokkaido';

        $search_prefecture = $_GET['search']['prefecture'];
        if ( empty($search_prefecture) ) $search_prefecture = 'hokkaido';

        $search_area_label = $_GET['search']['area_label'];
        if ( empty($search_area_label) ) $search_area_label = 'エリアを選択';

        $search_prefecture_label = $_GET['search']['prefecture_label'];
        if ( empty($search_prefecture_label) ) $search_prefecture_label = '都道府県を選択';
      ?>

      <input id="js-area-val" type="hidden" name="search[area]" value="<?php echo($search_area); ?>" />
      <input id="js-prefecture-val" type="hidden" name="search[prefecture]" value="<?php echo($search_prefecture); ?>" />
      <input id="js-area-label" type="hidden"  name="search[area_label]" value="<?php echo($search_area_label); ?>" />
      <input id="js-prefecture-label" type="hidden" name="search[prefecture_label]" value="<?php echo($search_prefecture_label); ?>" />
      <input id="js-paged" type="hidden" name="paged" value="1" />

      <div class="recruit-partime-columns">
        <div class="recruit-partime-column">
          <div class="select select-area js-select-area js-select">
            <select id="js-area-options" class="recruit-partime-select select">
              <?php
                $field = get_field_object('field_5de6255e70b88');
                if( $field['choices'] ): ?>
                  <?php foreach( $field['choices'] as $field => $label ): ?>
                    <option value="<?php echo $field; ?>">
                      <?php echo $label ?>
                    </option>
                  <?php endforeach; ?>
              <?php endif; ?>
            </select>

            <span class="area-select-text select-text js-select-text">エリアを選択</span>

            <ul class="select-list area-select-list js-select-list">
              <?php
                $field = get_field_object('field_5de6255e70b88');
                if( $field['choices'] ): ?>
                 <?php foreach( $field['choices'] as $field => $label ): ?>
                   <li class="select-item js-select-area-item" data-val="<?php echo $field; ?>">
                     <?php echo $label ?>
                   </li>
                 <?php endforeach; ?>
               <?php endif; ?>
            </ul>
          </div>
        </div>
        <div class="recruit-partime-column">
          <div class="select select-prefecture js-select-prefecture js-select">
            <select id="js-prefecture-options" name="prefecture-select" class="recruit-partime-select select">
              <?php
                $field = get_field_object('field_5de625a670b89');
                if( $field['choices'] ): ?>
                  <?php foreach( $field['choices'] as $field => $label ): ?>
                    <option value="<?php echo $field; ?>">
                      <?php echo $label ?>
                    </option>
                  <?php endforeach; ?>
              <?php endif; ?>
            </select>

           <span class="prefecture-select-text select-text js-select-text">都道府県を選択</span>

           <ul class="prefecture-select-list  select-list js-select-prefecture-list">
             <?php
               $field = get_field_object('field_5de625a670b89');
               if( $field['choices'] ): ?>
                <?php foreach( $field['choices'] as $field => $label ): ?>
                  <li class="select-item js-select-prefecture-item" data-val="<?php echo $field; ?>">
                    <?php echo $label ?>
                  </li>
               <?php endforeach; ?>
             <?php endif; ?>
           </ul>
         </div>
        </div>
      </div>
      <h6 class="recruit-partime-title">職種から探す</h6>

      <div class="recruit-partime-checkboxes">
        <?php
          $field = get_field_object('field_5de72023df926');
          if( $field['choices'] ): ?>
            <?php $index = 0; ?>

            <?php foreach( $field['choices'] as $value => $label ): ?>
              <?php import_template( 'parts/form/checkbox', array(
                'for' => "option$index",
                'label' => $label,
                'name' => 'search[job_type][]',
                'value' => $value,
                'checked' => (in_array($value, $_GET['search']['job_type']) ? "checked='true'" : "")
              )); ?>

            <?php $index++; ?>
            <?php endforeach; ?>
        <?php endif; ?>

      </div>
      <button class="recruit-partime-button" type="submit">検索</button>
    </form>
  </section><!-- end of partime seach form -->
  <section class="recruit-partime-search-result">
   <ul class="recruit-partime-result-list">

  <?php
     $job_type = $_GET['search']['job_type'];
     if (empty($job_type)) {
       $job_type =  array_keys(get_field_object('field_5de72023df926')['choices']);
     };
  ?>
   <?php

     if(isset($_GET['search']) == 1){
      $args = array(
        'post_type' => 'recruit_detail',
        'posts_per_page' => 6,
        'order' => 'ASC',
        'paged' => $paged,
        'meta_query'	=> array(
          'relation'		=> 'AND',
          array(
            'key'	 	=> 'job_type',
            'value'	  => $job_type,
            'compare' 	=> 'IN',
          ),
          array(
            'key'	 	=> 'area',
            'value'	  => $_GET['search']['area'],
            'compare' 	=> '=',
          ),
          array(
            'key'	 	=> 'prefecture',
            'value'	  => $_GET['search']['prefecture'],
            'compare' 	=> '=',
          ),
          array(
            'key'	  	=> 'recruit_page_type',
            'value'	  	=> 'mid_career',
            'compare' 	=> '=',
          ),
        ),
      );
     } else {
      $args = array(
        'post_type' => 'recruit_detail',
        'posts_per_page' => 6,
        'order' => 'ASC',
        'paged' => $paged,
        'meta_query'	=> array(
          'relation'		=> 'AND',
          array(
            'key'	  	=> 'recruit_page_type',
            'value'	  	=> 'mid_career',
            'compare' 	=> '=',
          ),
        ),
      );
     }

     $index = 0;
     $query = new WP_Query($args);
     while($query->have_posts()) : $query->the_post();
   ?>

     <?php $get_field = get_fields(); ?>
     <?php $storeID = get_post_field('ID', $get_field['recruit_store']); ?>
     <?php // 'address'   => get_post_meta($storeID)['住所'][0],  Street Address in store ?>

       <li class="recruit-partime-result-item">
         <?php import_template('parts/job-card', array(
           'modifier'  => '',
           'tags'      => array($get_field['employment_status']['label']),
           'heading'   => get_the_title(),
           'address'   => $get_field['recruit_address'], // Street Address
           'category'  => $get_field['job_type']['label'],
           'url'       => get_permalink(),
           'linktext'  => '詳細を見る',
         )); ?>
       </li>

     <?php $index++; ?>
   <?php endwhile; wp_reset_query(); ?>
   </ul>
   <?php if (function_exists("pagination")){
      pagination($query->max_num_pages);
   }?>
  </section><!-- end of partime seach result -->
</div>

<?php import_template('parts/renewal-content', array(
  'modifier' => 'renewal-content-partime'
)); ?>
<?php import_template('parts/renewal-copyright'); ?>
<?php wp_footer(); ?>
