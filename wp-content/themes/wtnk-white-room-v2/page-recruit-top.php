<?php
/**
 * Template Name: Recruit Top
 * @author NetBusinessAgent
 * @version 1.0.2
 */
?>

<?php import_template('parts/renewal-head'); ?>
<?php import_template('parts/renewal-banner', array(
  'modifier' => 'renewal-banner-recruit-top'
)); ?>

<div class="l-container">
  <div class="recruit-main-page">
    <div class="recruit-main-wrapper">
      <div class="recruit-type-list-wrapper">
         <ul class="recruit-type-list">
           <li class="recruit-type-item">
              <a href="<?php echo resolve_url('recruit-new-grad')?>" class="recruit-type-link">
                新卒採用<br class=“show-sp”>大学院・大学・短大・専門卒向け
              </a>
            </li>
           <li class="recruit-type-item">
              <a href="<?php echo resolve_url('recruit-highschool-grad')?>" class="recruit-type-link">
                新卒採用<br class=“show-sp”>高校卒向け
              </a>
            </li>
           <li class="recruit-type-item">
              <a href="<?php echo resolve_url('recruit-internship')?>" class="recruit-type-link">
                新卒採用<br/> インターンシップ
              </a>
           </li>
           <li class="recruit-type-item">
              <a href="<?php echo resolve_url('recruit-midcareer')?>" class="recruit-type-link">
                中途採用
              </a>
           </li>
           <li class="recruit-type-item">
              <a href="<?php echo resolve_url('recruit-partime')?>" class="recruit-type-link">
                アルバイト採用
              </a>
           </li>
           <li class="recruit-type-item">
              <a href="<?php echo resolve_url('recruit-handicapped')?>" class="recruit-type-link">
               障がい者採用
              </a>
           </li>
         </ul>
      </div>
    </div>
  </div>
</div>

<?php import_template('parts/renewal-content'); ?>
<?php import_template('parts/renewal-copyright'); ?>
<?php wp_footer(); ?>
