<?php
/**
  * Template Name: Interview
 */
?>

<?php import_template('parts/renewal-head'); ?>
<?php import_template('parts/renewal-banner'); ?>

<div class="member">
  <div class="l-container">
    <?php import_template( 'parts/common-section/breadcrumbs', array(
      'paths' => ["TOP", "RECRUIT TOP", "スタッフ紹介 > 社員インタビュー"],
    )); ?>
  </div>

  <?php import_template( 'parts/section-heading', array(
    'modifier' => 'l-section-heading-interview',
    'title' => '社員インタビュー',
    'text' => 'interview',
  )); ?>
  <div class="member-banner" style="background-image: url(<?php echo get_template_directory_uri() . '/images/renewal/member/member-banner.jpg'?>)"></div>
  <div class="l-container">
    
    <div class="member-info">
      <span class="member-position">バイヤー・店舗責任者</span>
      <span class="member-name">名字</span>
      <span class="member-years">0000年入社</span>
    </div>
    <section class="member-interview-topics">
      <div class="member-interview-image" style="background-image: url(<?php echo get_template_directory_uri() . '/images/renewal/member/member-img01.jpg'?>)"></div>
      <div class="member-interview-content">
        <span class="member-interview-subtitle">01.入社のきっかけとその当時の思い</span>
        <h3 class="member-interview-title">テキストが入りますテキストが入りますテキストが入ります</h3>
        <p class="member-interview-text">テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。</p>
      </div>
    </section>

    <section class="member-interview-topics">
      <div class="member-interview-image" style="background-image: url(<?php echo get_template_directory_uri() . '/images/renewal/member/member-img02.jpg'?>)"></div>
      <div class="member-interview-content">
        <span class="member-interview-subtitle">02.今、大事にしている想い、姿勢</span>
        <h3 class="member-interview-title">テキストが入りますテキストが入りますテキストが入ります</h3>
        <p class="member-interview-text">テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。</p>
      </div>
    </section>

    <section class="member-interview-discussion">
      <span class="member-interview-discussion-subtitle">03.アパレル業界で働いていてよかったと思うことは？</span>
      <h3 class="member-interview-discussion-title">テキストが入りますテキストが入りますテキストが入ります</h3>
      <div class="member-interview-discussion-image" style="background-image: url(<?php echo get_template_directory_uri() . '/images/renewal/member/member-img03.png'?>)"></div>
      <p class="member-interview-discussion-text">テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。</p>
    </section>
  </div>
</div>

<div class="schedule">
  <div class="l-container">
    <h2 class="schedule-heading">業務内容・1日のスケジュール</h2>
    <ul class="schedule-list">
      <li class="schedule-item">
        <time class="schedule-time" date="09:30">09:30</time>
        <span class="schedule-detail">出勤、開店準備</span>
      </li>
      <li class="schedule-item">
        <time class="schedule-time" date="10:00">10:00</time>
        <span class="schedule-detail">出勤、開店準備</span>
      </li>
      <li class="schedule-item">
        <time class="schedule-time" date="00:00">00:00</time>
        <span class="schedule-detail">テキストが入りますテキストが入りますテキストが入りますテキストが入りますテキストが入りますテキストが入ります</span>
      </li>
      <li class="schedule-item">
        <time class="schedule-time" date="00:00">00:00</time>
        <span class="schedule-detail">テキストが入りますテキストが入りますテキストが入りますテキストが入りますテキストが入りますテキストが入ります</span>
      </li>
      <li class="schedule-item">
        <time class="schedule-time" date="00:00">00:00</time>
        <span class="schedule-detail">テキストが入りますテキストが入りますテキストが入りますテキストが入りますテキストが入りますテキストが入ります</span>
      </li>
    </ul>

    <?php import_template('parts/renewal-button', array(
      'modifier' => '',
      'url' => 'javascript:;',
      'text' => 'ENTRY',
    )); ?>
  </div>
</div>

<?php import_template('parts/member-interview', array(
  'modifier' => 'member-interview-detail'
)); ?>
<?php import_template('parts/renewal-content'); ?>
<?php import_template('parts/renewal-copyright'); ?>
