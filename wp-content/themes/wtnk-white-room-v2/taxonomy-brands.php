<?php get_header(); ?>

	<main id="main" role="main">
	
				<header class="entry-header">

					<h1 class="page-title">SHOP LIST -
<?php $term = $wp_query->queried_object;
//var_dump($term);

// term名を出力
echo esc_attr( $term->name );?>-</h1>
<div class="list_logo_wrapper">
<img src="<?php echo home_url(); ?>/img/logo/<?php echo $term->slug ; ?>.png" class="list_logo">
</div>
					<?php whiteroom_the_bread_crumb(); ?>
				<!-- end .entry-header --></header>

				<div class="entry-content">
	<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php
 $custom_fields = get_post_custom();
 $id = get_the_ID();
$number =  $custom_fields["店舗コード"][0];
 ?>
<?php if($number):?>
<p><?php echo $number; ?>　【<?php $area = get_the_terms( $id, "area" ); echo $area[0]->name;?>
<?php //echo get_the_term_list($id, "area", "", "　/　",""); ?>
】　<a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a>　【<?php //echo get_the_term_list($id, "brands", "", "　/　","");  ?>
<?php
$terms = get_the_terms( $id, 'brands' );
if ( !empty($terms) ) : if ( !is_wp_error($terms) ) :
$length = count($terms);
$no = 0;
?>
<?php foreach( $terms as $term ) : ?>
<?php echo $term->name;
$no++;
if($no !== $length){
echo "/";
}
?>
<?php endforeach; ?>
<?php endif; endif; ?>
】</p>
<?php endif; ?>
<?php endwhile;  ?>

<!-- end .entry-content --></div>

</section>
	<!-- end #main --></main>

	<aside id="sub">
		<?php get_sidebar(); ?>
	<!-- end #sub --></aside>

<?php get_footer(); ?>
