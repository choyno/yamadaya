<?php

$absolute_path="/var/www/home/users/ymdy.co.jp_admin/www/ymdy.co.jp/";


/**
 * @author NetBusinessAgent
 * @version 1.0.4
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 1170;
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */

if ( ! function_exists( 'whiteroom_setup' ) ) :
function whiteroom_setup() {

	// Make theme available for translation.
	load_theme_textdomain( 'whiteroom', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	// Enable support for Post Thumbnails on posts and pages.
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'responsive-size', 740, 493, true );
	add_image_size( 'column-3', 270, 180, true );
add_image_size( 'brand_thumbnail', 600, 400, true );
add_image_size('brand_list', 420, 280, true );
add_image_size( 'news_thumbnail', 450, 300, true );
add_image_size( 'inteview_banner', 2000, 1000, true );
add_image_size( 'member_interview_image', 970, 970, true );
add_image_size( 'member_card_image', 626, 646, true );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'global-nav' => 'グローバルナビゲーション',
		'footer-nav' => 'フッターナビゲーション',
		'social-nav' => 'ソーシャルメディア',
	) );

	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array( 'comment-list', 'search-form', 'comment-form', ) );

	// Enable support for editor style.
	add_editor_style();
	function whiteroom_custom_editor_settings( $initArray ){
		$class = 'entry-content';
		if ( get_theme_mod( 'font_family' ) === 'mincho' ) {
			$class .= ' mincho';
		} else {
			$class .= ' gothic';
		}
		$initArray['body_class'] = $class;
		return $initArray;
	}
	add_filter( 'tiny_mce_before_init', 'whiteroom_custom_editor_settings' );

	function whiteroom_excerpt_mblength( $length ) {
		if ( is_page() ) {
			return 50;
		}
		return $length;
	}
	add_filter( 'excerpt_mblength', 'whiteroom_excerpt_mblength' );

	function whiteroom_excerpt_more( $more ) {
		$more = '';
		if ( !has_excerpt() ) {
			$more .= '...';
		}
	}
	add_filter( 'excerpt_more', 'whiteroom_excerpt_more' );

	function whiteroom_wp_trim_excerpt( $excerpt ) {
		return esc_html( $excerpt ) . '<span class="read-more">' . apply_filters( 'whiteroom_more_text', 'Read More &raquo;' ) . '</span>';
	}
	add_filter( 'wp_trim_excerpt', 'whiteroom_wp_trim_excerpt' );


	/**
	 * fix_nav_menu_css_class
	 * nav_menuのCSSクラス付与の不具合を修正
	 */
	function fix_nav_menu_css_class( $classes, $item ) {
		global $wp_query;
		$page_template = get_post_meta( $item->object_id, '_wp_page_template', true );

		$page_for_posts = get_option( 'page_for_posts' );
		$post_type_query = $wp_query->query_vars['post_type'];
		$del_flag = true;

		if ( is_singular( 'post' ) || is_category() || is_tag() ) {
			$del_flag = false;
		} elseif ( ( is_author() || is_date() || is_author() ) ) {
			if ( in_array( $post_type_query, array ( '', 'post' ) ) ) {
				$del_flag = false;
			}
		} elseif ( is_singular() ) {
			if ( $post_type_query === 'news' && $page_template == 'template/template-news.php' ||
				 $post_type_query === 'voice' && $page_template == 'template/template-voice.php' ||
				 $post_type_query === 'faq' && $page_template == 'template/template-faq.php'
				) {
				$classes[] = 'current_page_parent';
			}
		} elseif ( is_tax() ) {
			$taxonomy = get_taxonomy( $wp_query->query_vars['taxonomy'] );
			if ( count( $taxonomy->object_type ) == 1 && $taxonomy->object_type[0] == 'post' ) {
				$del_flag = false;
			}
			if ( $taxonomy->object_type[0] == $item->object && $item->type === 'post_type_archive' ) {
				$classes[] = 'current_page_parent';
			}
			if ( $taxonomy->object_type[0] == 'news' && $page_template == 'template/template-news.php' ||
				 $taxonomy->object_type[0] == 'voice' && $page_template == 'template/template-voice.php' ||
				 $taxonomy->object_type[0] == 'faq' && $page_template == 'template/template-faq.php'
				) {
				$classes[] = 'current_page_parent';
			}
		}

		if ( $del_flag && is_numeric( $page_for_posts ) && $item->object_id == $page_for_posts && $item->object == 'page' && $key = array_search( 'current_page_parent', $classes ) ) {
			unset( $classes[$key] );
		}
		return $classes;
	}
	add_filter( 'nav_menu_css_class', 'fix_nav_menu_css_class', 10, 2 );

	add_post_type_support( 'page', 'excerpt' );
}
endif;
add_action( 'after_setup_theme', 'whiteroom_setup' );

/**
 * Register widgetized area and update sidebar with default widgets.
 */
function whiteroom_widgets_init() {
	register_sidebar( array(
		'name'		  => 'ブログサイドバー',
		'id'			=> 'blog-sidebar',
		'before_widget' => '<div id="%1$s" class="widget-container %2$s"><dl>',
		'after_widget'  => '</dd></dl></div>',
		'before_title'  => '<dt class="widget-title">',
		'after_title'   => '</dt><dd class="widget-content">',
	) );

	register_sidebar( array(
		'name'		  => '固定ページ等記事下',
		'id'			=> 'sidebar',
		'before_widget' => '<div id="%1$s" class="widget-container col-4 %2$s"><dl>',
		'after_widget'  => '</dd></dl></div>',
		'before_title'  => '<dt class="widget-title">',
		'after_title'   => '</dt><dd class="widget-content">',
	) );
	register_sidebar( array(
		'name'		  => 'フッター',
		'id'			=> 'footer',
		'before_widget' => '<div id="%1$s" class="widget col-4 %2$s"><dl>',
		'after_widget'  => '</dd></dl></div>',
		'before_title'  => '<dt class="widget-title">',
		'after_title'   => '</dt><dd class="widget-content">',
	) );
	register_sidebar( array(
		'name'		  => 'サブメニュー',
		'id'			=> 'submenu',
		'before_widget' => '<div id="submenu">',
		'after_widget'  => '</div>',
		'before_title'  => '<p>',
		'after_title'   => '</p>'
	) );
}
add_action( 'widgets_init', 'whiteroom_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function whiteroom_scripts() {
	$template_url = get_template_directory_uri();
	wp_enqueue_style( 'whiteroom-base', get_stylesheet_uri() );
	wp_enqueue_style( 'jquery.responsive-nav', $template_url . '/js/jquery.responsive-nav/jquery.responsive-nav.css' );
	wp_enqueue_script( 'jquery.responsive-nav', $template_url . '/js/jquery.responsive-nav/jquery.responsive-nav.js', array( 'jquery' ), false, true );
	
	wp_enqueue_style( 'jquery.scrollButton', $template_url . '/js/jquery.scrollButton/jquery.scrollButton.css' );
	wp_enqueue_script( 'jquery.scrollButton', $template_url . '/js/jquery.scrollButton/jquery.scrollButton.js', array( 'jquery' ), false, true );

	wp_enqueue_script( 'jquery.SmoothScroll', $template_url . '/js/jquery.SmoothScroll/jquery.smoothScroll.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'yubinbango', 'https://yubinbango.github.io/yubinbango/yubinbango.js', true, false); 

	wp_enqueue_style( 'whiteroom-style', $template_url . '/css/layout.css?2017');
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	wp_enqueue_script( 'whiteroom-base', $template_url . '/js/whiteroom.js', array( 'jquery' ), 2, true );
}
add_action( 'wp_enqueue_scripts', 'whiteroom_scripts' );

/**
 * Enqueue scripts and styles.
 */
function whiteroom_admin_scripts() {
	add_thickbox();
	wp_enqueue_style( 'whiteroom-admin', get_template_directory_uri() . '/css/admin.css' );
}
add_action( 'admin_enqueue_scripts', 'whiteroom_admin_scripts' );

/**
 * Display recommend plugin message in widgets
 */
function whiteroom_widgets_admin_page() {
	?>
	<?php if ( !is_plugin_active( 'black-studio-tinymce-widget/black-studio-tinymce-widget.php' ) ) : ?>
	<div class="whiteroom-information">
		<ul>
			<li><a href="<?php echo admin_url( 'plugin-install.php?tab=plugin-information&amp;plugin=black-studio-tinymce-widget&amp;TB_iframe=true&amp;width=600&amp;height=550' ); ?>" class="thickbox">Black Studio TinyMCE Widget</a>をインストールするとWYSIWYGエディタがウィジェットで使用可能になります。</li>
		</ul>
	<!-- end .whiteroom-information --></div>
	<?php endif; ?>
	<?php
}
add_action( 'widgets_admin_page', 'whiteroom_widgets_admin_page' );

/**
 * Display recommend plugin message in nav-menus
 */
function whiteroom_user_edit_notices() {
	?>
	<?php if( !is_plugin_active( 'simple-local-avatars/simple-local-avatars.php' ) ) : ?>
	<div class="whiteroom-information">
		<ul>
			<li><a href="<?php echo admin_url( 'plugin-install.php?tab=plugin-information&amp;plugin=simple-local-avatars&amp;TB_iframe=true&amp;width=600&amp;height=550' ); ?>" class="thickbox">Simple Local Avatars</a>をインストールするとアバター画像をアップロードできるようになります。</li>
		</ul>
	<!-- end .whiteroom-information --></div>
	<?php endif; ?>
	<?php
}
function whiteroom_set_user_edit_notices() {
	add_action( 'admin_notices', 'whiteroom_user_edit_notices' );
}
add_action( 'admin_head-user-edit.php', 'whiteroom_set_user_edit_notices' );

/**
 * whiteroom_youtube_responsive
 */
function whiteroom_youtube_responsive( $html, $url, $attr, $post_ID ) {
	if ( preg_match( '/^https?:\/\/www\.youtube\.com/', $url ) ) {
		$html = '<div class="whiteroom-video-container">' . $html . '</div>';
	}
	return $html;
}
add_filter( 'embed_oembed_html', 'whiteroom_youtube_responsive', 10, 4 );

/**
 * whiteroom_add_custom_image_size_select
 */
function whiteroom_add_custom_image_size_select( $size_names ) {
	$custom_sizes = get_intermediate_image_sizes();
	foreach ( $custom_sizes as $custom_size ) {
		if ( !isset( $size_names[$custom_size] ) && $custom_size !== 'whiteroom-header-image' ) {
			$size_names[$custom_size] = $custom_size;
		}
	}
	return $size_names;
}
add_filter( 'image_size_names_choose', 'whiteroom_add_custom_image_size_select' );

/**
 * whiteroom_wp_link_pages_link
 */
function whiteroom_wp_link_pages_link( $link, $i ) {
	$link = preg_replace( '/^(<a.+>)<span.+?>(.+?)<\/span>(<\/a>)$/', '$1$2$3', $link );
	return $link;
}
add_filter( 'wp_link_pages_link', 'whiteroom_wp_link_pages_link', 10, 2 );

/**
 * add thumbnail setting description
 * ブログの場合：column-3
 * インデックスページの場合：responsive-size
 * お客様の声の場合：responsive-size
 */
function whiteroom_admin_post_thumbnail_html( $content ) {
	global $_wp_additional_image_sizes;

	if ( isset( $image_size, $_wp_additional_image_sizes[$image_size] ) ) {
		$postThumbnail = $_wp_additional_image_sizes[$image_size];
		if ( isset( $postThumbnail['height'], $postThumbnail['width'] ) ) {
			$height = $postThumbnail['height'];
			$width  = $postThumbnail['width'];
			$content .= '<p class="howto">推奨サイズ：' . $width . ' x ' . $height . '<br />※これより大きいサイズの画像を指定した場合は自動的にリサイズ&amp;トリミングされます。</p>';
		}
	}
	return $content;
}
add_filter( 'admin_post_thumbnail_html', 'whiteroom_admin_post_thumbnail_html' ) ;


/**
 * whiteroom_front_page_main_visual_default
 */
if ( ! function_exists( 'whiteroom_front_page_main_visual_default' ) ) :
function whiteroom_front_page_main_visual_default() {
	if ( get_header_image() ) :
		?>
		<div class="main-visual">
			<img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" />
		<!-- end .main-visual --></div>
		<?php
	endif;
}
endif;
add_action( 'whiteroom_front_page_main_visual', 'whiteroom_front_page_main_visual_default' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';


/**
 * Widgets
 */
require get_template_directory() . '/inc/widgets/recent-entries.php';
require get_template_directory() . '/inc/widgets/eyecatch-entry.php';
require get_template_directory() . '/inc/widgets/taxonomy.php';

/**
 * Customize front page widget areas.
 */
require get_template_directory() . '/inc/front-page/front-page.php';

/**
 * Add header image in single or page.
 */
require get_template_directory() . '/inc/header-image/header-image.php';

/**
 * Bread crumb
 */
require get_template_directory() . '/inc/bread-crumb.php';

/**
 * Include shortcodes
 */
require get_template_directory() . '/inc/shortcodes.php';


/**
 * Theme updater
 */
require get_template_directory() . '/inc/theme-update.php';
$ATPU_Theme = new ATPU_Theme( 'http://www.wp-flat.com/api/' );

function is_mobile() {
  $useragents = array(
    'iPhone',          // iPhone
    'iPod',            // iPod touch
    'Android',         // 1.5+ Android
    'dream',           // Pre 1.5 Android
    'CUPCAKE',         // 1.5+ Android
    'blackberry9500',  // Storm
    'blackberry9530',  // Storm
    'blackberry9520',  // Storm v2
    'blackberry9550',  // Storm v2
    'blackberry9800',  // Torch
    'webOS',           // Palm Pre Experimental
    'incognito',       // Other iPhone browser
    'webmate'          // Other iPhone browser
  );
  $pattern = '/'.implode('|', $useragents).'/i';
  return preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
}

register_sidebar (array(
	'name'          => 'フッター１',
	'id'            => 'footer1',
	'before_title'  => '<h3 class="footer_widget_title">',
	'after_title'   => '</h3>',
) );
register_sidebar (array(
	'name'          => 'フッター２',
	'id'            => 'footer2',
	'before_title'  => '<h3 class="footer_widget_title">',
	'after_title'   => '</h3>',
) );
register_sidebar (array(
	'name'          => 'フッター３',
	'id'            => 'footer3',
	'before_title'  => '<h3 class="footer_widget_title">',
	'after_title'   => '</h3>',
) );
register_sidebar (array(
	'name'          => 'フッター４',
	'id'            => 'footer4',
	'before_title'  => '<h3 class="footer_widget_title">',
	'after_title'   => '</h3>',
) );
register_sidebar (array(
	'name'          => 'フッター５',
	'id'            => 'footer5',
	'before_title'  => '<h3 class="footer_widget_title">',
	'after_title'   => '</h3>',
) );
register_sidebar (array(
	'name'          => 'フッター６',
	'id'            => 'footer6',
	'before_title'  => '<h3 class="footer_widget_title">',
	'after_title'   => '</h3>',
) );

register_sidebar (array(
	'name'          => 'フッターモバイル１',
	'id'            => 'footer1_mo',
	'before_widget' => '<div class="footer-content m-item">',
	'after_widget'  => '</div>',
	'before_title'  => '<h3 class="footer_widget_title">',
	'after_title'   => '</h3>',
) );
register_sidebar (array(
	'name'          => 'フッターモバイル２',
	'id'            => 'footer2_mo',
	'before_widget' => '<div class="footer-content m-item">',
	'after_widget'  => '</div>',
	'before_title'  => '<h3 class="footer_widget_title">',
	'after_title'   => '</h3>',
) );


function nav_menu_first_last( $items ) {
$pos = strrpos($items, 'class="menu-item', -1);
$items=substr_replace($items, 'menu-item-last ', $pos+7, 0);
$pos = strpos($items, 'class="menu-item');
$items=substr_replace($items, 'menu-item-first ', $pos+7, 0);
return $items;
}
add_filter( 'wp_nav_menu_items', 'nav_menu_first_last' );

/*nav改造*/
class Walker_Nav_Menu_Test extends Walker_Nav_Menu {
function start_el(&$output, $item, $depth, $args) {
$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
$classes = empty( $item->classes ) ? array() : (array) $item->classes;
$classes[] = 'menu-item-' . $item->ID;
$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );

$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
$output .= $indent . '<li ' . $id . $class_names . '>';
$atts = array();
$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
$atts['target'] = ! empty( $item->target )     ? $item->target     : '';
$atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
$atts['href']   = ! empty( $item->url )        ? $item->url        : '';
$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );
$attributes = '';
foreach ( $atts as $attr => $value ) {
if ( ! empty( $value ) ) {
$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
$attributes .= ' ' . $attr . '="' . $value . '"';
}
}
//echo "<pre>";
//print_r($item);
$item_output = $args->before;
$item_output .= '<a'. $attributes .'>';
$item_output .= '<span onmouseover="this.innerText=\''.$item->description.'\'" onmouseout="this.innerText=\''.$item->title.'\'">';

$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
$item_output .= '</a>';
$item_output .= $args->after;
$item_output .= "</span>";

$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
}
}



function my_columns($columns) {
    $columns['店舗コード'] = "店舗コード";

   //if ( get_query_var( 'post_type' )=='recruit_detail') {
   //  $columns['Recruit Job Type'] = "Recruit Job Type";
   //}

    return $columns;
}

add_filter( 'manage_posts_columns', 'my_columns' );

function add_column_value ($column_name, $post_ID) {
    if( $column_name == '店舗コード' ) {
        echo get_post_meta($post_ID, '店舗コード', true);
        //echo get_post_meta($post_ID, 'wpcf-item_id', true);//カスタムフィールドの作成に、プラグインの「Types」をつかている場合は、wpcf-をつける
    }

   //if ( get_query_var( 'post_type' ) == 'recruit_detail') {
   //    echo get_post_meta($post_ID, 'recruit_job_type', true);
   //}

}
add_filter( 'manage_pages_custom_column', 'add_column_value', 10, 2 );
add_filter( 'manage_posts_custom_column', 'add_column_value', 10, 2 );

function store_add_acf_columns ( $columns ) {
  return array_merge ( $columns, array (
    'store_brand' => __ ( 'Store brand' ),
  ) );
}

function job_add_acf_columns ( $columns ) {
  return array_merge ( $columns, array (
    'recruit_page_type' => __ ( 'Recruit Page Type' ),
  ) );
}

add_filter ( 'manage_recruit_detail_posts_columns', 'job_add_acf_columns' );
add_filter ( 'manage_store_detail_posts_columns', 'store_add_acf_columns' );

function post_type_detail_custom_column ( $column, $post_id ) {
  $get_field = get_fields($post_id);
  switch ( $column ) {
    case 'recruit_page_type':
      echo $get_field['recruit_page_type']['label'];
      break;
    case 'store_brand':
      $brands = get_field('store_brand', $post_id);
      echo implode( " ", $brands);;
      break;
  }
}

add_action ( 'manage_recruit_detail_posts_custom_column', 'post_type_detail_custom_column', 10, 2 );
add_action ( 'manage_store_detail_posts_custom_column', 'post_type_detail_custom_column', 10, 2 );

//https://pluginrepublic.com/add-acf-fields-to-admin-columns/

add_action( 'pre_get_posts', 'foo_modify_main_queries' );
function foo_modify_main_queries ( $query ) {
	if ( is_admin() || ! $query -> is_main_query() ){
		return;
	}
	if (is_tax()  or  (is_post_type_archive() and get_query_var( 'post_type' )=='store')) {
		//echo "タクソノミーかアーカイブです";

		$query -> set( 'orderby', 'meta_value_num' );
		$query -> set( 'meta_key', '店舗コード' );
		$query -> set( 'order', 'asc' );
		return;
	}

}

function import_template($tpl, $vars = array())
{
    $tpl = ltrim($tpl, '/').'.php';
    $path = locate_template(array($tpl));
    if (empty($path)) {
        throw new LogicException("Cannot locate the template '$tpl'.");
    }
    extract($vars);
    include $path;
}

function resolve_url($path = '')
{
    return esc_url(get_home_url(null, $path));
}


function createCustomPostType($postType, $labels = array(), $isPublic = true, $taxonomies = array(), $hasArchive = true, $menuPosition = null)
{
	//make sure that name and basic labels have values
	$labelSingular = $labels['singular'];
	$labelPlural = $labels['plural'];

	if ($postType && $labelSingular && $labelPlural) {
		$singlePostLabelLower = strtolower($labelSingular);

		//register taxonomy
		$defaultTaxonomy = $singlePostLabelLower . '_category';

		register_taxonomy($defaultTaxonomy, $postType, array(
			'label' => __($labelSingular . ' Category'),
			'rewrite' => array('slug' => $singlePostLabelLower . "-category"),
			'hierarchical' => true,
			'public' => true,
		));

		$postTaxonomies = $taxonomies;
		array_push($postTaxonomies, $singlePostLabelLower . '_category');

		register_post_type($postType, array(
			'labels' => array(
				'name' => __(ucfirst($labelPlural)),
				'all_items'  => __(ucfirst($labelPlural)),
				'singular_name' => __(ucfirst($labelSingular)),
				'add_new'            => _x('Add ' . $labelSingular, $postType),
				'add_new_item'       => __('Add New ' . $labelSingular),
				'new_item'           => __('New ' . $labelSingular),
				'edit_item'          => __('Edit ' . $labelSingular),
				'view_item'          => __('View ' . $labelSingular),
			),
			'rewrite' => array(
				'with_front' => false
			),
			'menu_position' => $menuPosition,
			'public' => $isPublic,
			'publicly_queryable' => true,
			'has_archive' => true,
			'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments'),
			'taxonomies' => $postTaxonomies,
		));
	}
}

function createShopCustomPostType()
{
  $labels = array(
    'singular' => 'Shop',
    'plural' => 'Shops',
  );
  createCustomPostType('shops', $labels, true);
}

//execute
//createShopCustomPostType();

/**
* 
*
*
*/
function pagination($pages = '', $range = 4){

  $showitems = ($range * 2) + 1;
  global $paged;

  if(empty($paged)) $paged = 1;

  if($pages == ''){
    global $wp_query;
    $pages = $wp_query->max_num_pages;
    if(!$pages){
        $pages = 1;
    }
  }

  if(1 != $pages)
  {
     echo "<div class='paginate'>";
     if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First </a>";

     if($paged > 1 && $showitems > $pages) 
     	echo "<a class='paginate-controls paginate-prev' href='".get_pagenum_link($paged - 1)."'>&lsaquo; 前へ</a>";

     for($i=1; $i <= $pages; $i++)
     {
       if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
          echo ($paged == $i)? "<span class='paginate-item paginate-current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='paginate-item'>".$i."</a>";
        }
     }

     if ($paged < $pages || $showitems < $pages) {
        echo "<a class='paginate-controls paginate-next' href='".get_pagenum_link($paged + 1)."'>次へ &rsaquo;</a>";
     }

     if ($paged < $pages-1 && $paged+$range-1 < $pages && $showitems < $pages){
       echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
     }
     echo "</div>\n";
  }
}


$CONFIG = array(
    'js_hooks' => array(
      'select_area' => 'js-area-options',
      'select_prefecture' => 'js-prefecture-options'
    ),
    'custom_post_types' => array(
      'shops' => 'shops'
    ),
    'areas' => array(
      'area' => 'prefecture',
    ),
    'prefectures' => array(
    	'hokkaido' => 'field_5de625a670b89',
    	'tohoku' => 'field_5de628b037e1a',
    	'koshinetsu' => 'field_5de6809013855',
    	'kitakantou' => 'field_5de699bdbaae3',
    	'minamikantou' => 'field_5de69a03baae4',
    	'tokai_area' => 'field_5de69a4dbaae5',
    	'hokuriku' => 'field_5de69c0117934',
    	'kansai' => 'field_5de69d0693ed0',
    	'chugoku' => 'field_5de69da129939',
    	'shikoku' => 'field_5de69dc8da76f',
    	'okinawa' => 'field_5de86e6f15261',
    	'kyushu_area' => 'field_5de69e38b6615'
    )
  );

define('_CONFIG', $CONFIG);
define('_PREFECTURES', $CONFIG['prefectures']);

function getPrefectureFieldName($key) {

	$key = str_replace('_', '', $key);

	$areas = array(
	    'hokkaido' => 'field_5de625a670b89',
	    'tohoku' => 'field_5de628b037e1a',
	    'koshinetsu' => 'field_5de6809013855',
	    'kitakantou' => 'field_5de699bdbaae3',
	    'minamikantou' => 'field_5de69a03baae4',
	    'tokai_area' => 'field_5de69a4dbaae5',
	    'hokuriku' => 'field_5de69c0117934',
	    'kansai' => 'field_5de69d0693ed0',
	    'chugoku' => 'field_5de69da129939',
	    'shikoku' => 'field_5de69dc8da76f',
	    'okinawa' => 'field_5de86e6f15261',
	    'kyushu_area' => 'field_5de69e38b6615',
	  );

	foreach($areas as $k => $val) {
		if($k == $key ) {
			echo 'foreach key: ' . $k . ' with val ' . $val . '<br />';
			//return $val;
		} else {
			echo 'foreach key: ' . $k . ' without val ' . $val . '<br />';
		}
	}


	echo 'key: ' . $key . '<br />';
	echo 'Type: '. gettype($key) . '<br />';
	echo 'name: ' . 'prefecture_'.$key . '<br />';
  	echo 'total: ' . count($areas) . "<br />";

  	if(  array_key_exists($key, $areas)) {
  		echo 'key found! <br /><br />';
  	} else {
  		echo 'no key found!<br /><br />';
  	}

  return $areas[$key];
}

add_action( 'wp_print_scripts', 'search_area_prefecture' );

function logme($data) {
	echo "<pre>";
	print_r($data);
	echo "</pre>";
	die();
}

function contact_form_error_message( $error, $key, $rule ) {
  switch ( $key ) {

    case 'email-confirm':
      $error = '確認のため、同じメールアドレスを入力してください。';
      break;

    case 'age':
    case 'desired-job':
    case 'educational-attainment':
    case 'gender':
    case 'store1':
      $error = '選択してください';
      break;

    default:
      $error = '入力してください';
      break;
  }

  return $error;
}

add_filter( 'mwform_error_message_mw-wp-form-2340', 'contact_form_error_message', 10, 3 );

function _getAreasACF() {
	$out = array();
	
	foreach(_PREFECTURES as $key => $val) {
		$k = trim($key);
		$area_prefectures = get_field_object($val);

		$out[$k]['area'] = $k;
		$out[$k]['prefectures'] = $area_prefectures['choices'];
	}

	return $out;
}



function search_area_prefecture() {
  $page_name = 'recruit-partime';

  if( is_page( $page_name ) OR  is_page( 'recruit-midcareer' ) OR  is_page( 'recruit-form' ) ) {
  	$areas = _getAreasACF();

    $hook_area = _CONFIG['js_hooks']['select_area'];
    $hook_prefecture = _CONFIG['js_hooks']['select_prefecture'];
    $data = json_encode($areas);

?>
  <script type="text/javascript">

      window.customSearchObj = `<?= $data ?>`;
      var areaElem = document.querySelector(`#<?= $hook_area ?>`);
      var prefectureElem = document.querySelector(`<?= $hook_prefecture ?>`);

      console.log(JSON.parse(window.customSearchObj));
  </script>

<?php
    }
  }

?>



