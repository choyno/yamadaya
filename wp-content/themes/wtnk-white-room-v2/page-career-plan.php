<?php
/**
  * Template Name: Career Plan
 * @author NetBusinessAgent
 * @version 1.0.2
 */
?>
<?php import_template('parts/renewal-head'); ?>
<?php import_template('parts/renewal-banner'); ?>

<div class="l-container">

<?php
  $breadcrumbs = array (
    'breadcrumbs' => array (
      array (
        'title' => 'ヤマダヤのキャリア',
        'link' => resolve_url('career-plan'),
      )
    )
  );
  import_template( 'parts/common-section/breadcrumbs', $breadcrumbs);
?>

  <div class="renewal-career">
    <?php import_template( 'parts/section-heading', array(
      'modifier' => 'l-section-heading-career',
      'title' => 'ヤマダヤのキャリア',
      'text' => 'career',
    )); ?>

    <div class="renewal-career-content">
      <p class="renewal-career-copy">ファッションアドバイザー（実店舗/オンライン）からキャリアがスタート。
      経験をいかし、よりプロフェッショナルに、そしてマネジメント側へと活躍の場を広げることができます。</p>

      <div class="renewal-career-structure">
        <span class="renewal-career-caption">より運営側へ</span>
        <div class="renewal-career-image-structure">
          <img src="<?php echo get_template_directory_uri() . '/images/renewal/plan1.png'?>" alt="">
        </div>
      </div>
      <div class="renewal-career-position">
        <h3 class="renewal-career-position-heading">ヤマダヤグループを運営する職種・役割</h3>
        <p class="renewal-career-text">ヤマダヤグループでは、SPA（川上～川下）業態の中でもさまざまな職種が活躍し、役割を担っています。</p>
        <div class="renewal-career-image-position">
          <img src="<?php echo get_template_directory_uri() . '/images/renewal/plan2.png'?>" alt="">
        </div>
      </div>
      <div class="renewal-career-description">
        <h3 class="renewal-career-description-heading">各職種・職位の紹介</h3>
        <ul class="renewal-career-list">
          <li class="renewal-career-item">
            <h4 class="renewal-career-description-title">ファッションアドバイザー<span>（FA）</span></h4>
            <p class="renewal-career-description-copy">接客を中心に、店舗ディスプレイや在庫管理、顧客管理などを行います。
            マニュアルがないお客様にあわせた接客スタイルで、ファッションを楽しんでいただけるようにトータルコーディネート提案をします。ファッションを楽しみながら追求し、お客様に「また来たい」、「接客をしてほしい」と思って頂く接客を目指します。</p>
          </li>
          <li class="renewal-career-item">
            <h4 class="renewal-career-description-title">スーパーファッションアドバイザー<span>（SFA）</span></h4>
            <p class="renewal-career-description-copy">シーズンを通してお客様に支持されるトータルコーディネートでの提案力・接客力をいかし、お店づくりに携わるスペシャリスト。一定条件を満たした方に与えられる職位。ファッションを追求し、培った接客スキルを発信しながら、お店の顧客づくりを目指します。スタッフからも目標となる存在としての活躍が期待されています。</p>
          </li>
          <li class="renewal-career-item">
            <h4 class="renewal-career-description-title">ショップマスター<span>（SM）</span></h4>
            <p class="renewal-career-description-copy">ショップのトータルマネジメントの責任者。平均的に入社4～6年目でキャリアアップ。
            接客と店舗の演出、売上管理、商品管理、スタッフ教育などの主な仕事をとおしてお店の顧客づくりを目指します。
            スーパーバイザーや本社からのサポートをうけながら、スタッフ一人ひとりの成長とチームとして成長をするためのリーダーシップが求められます。</p>
          </li>
          <li class="renewal-career-item">
            <h4 class="renewal-career-description-title">ブランドマネージャー<span>（BM）</span></h4>
            <p class="renewal-career-description-copy">自社ブランド・商品のPRを担当。ブランド認知の促進から、各店舗の運営チェックやブランド情報の発信を行います。また、カタログ・DM制作、コレクション写真のためのスタイリングを考案します。担当ブランドの世界観やマーケットでの位置づけを理解して掘り下げる力、とスタイリングなどで表現する力が求められます。</p>
          </li>
          <li class="renewal-career-item">
            <h4 class="renewal-career-description-title">バイヤー・ストアバイヤー<span>（SB）</span></h4>
            <p class="renewal-career-description-copy">商品企画サポートや店舗監修、商品の買い付けを担当。店舗監修では、売り場の演出や市場ニーズの目線から、店長へアドバイスを行います。また、シーズンごとに様々なブランド、メーカーの展示会をまわり、旬のアイテムを買い付けします。半歩先のファッション動向を予測する視点が求められます。</p>
          </li>
          <li class="renewal-career-item">
            <h4 class="renewal-career-description-title">スーパーバイザー<span>（SV）</span></h4>
            <p class="renewal-career-description-copy">ショップマスターの教育および店舗運営のサポート。
            担当店舗またはエリアをまわり、各店舗のお客様満足度が上がるようにショップマスターやスタッフを育成します。モチベーションの高いチームをつくれるようなマネジメント能力と問題解決能力が求められます。</p>
          </li>
          <li class="renewal-career-item">
            <h4 class="renewal-career-description-title">企画MD</h4>
            <p class="renewal-career-description-copy">企画は、メーカーやパタンナーとのコミュニケーションを重ね、オリジナルブランドの商品を企画します。また、バイヤーと連動してシーズンMDの構築、店のディレクション案を組み立て、スタッフへ情報を発信していきます。先見性、感性とともに、売上管理や生産管理などのマネジメント能力が求められます。</p>
          </li>
          <li class="renewal-career-item">
            <h4 class="renewal-career-description-title">店舗運営部</h4>
            <p class="renewal-career-description-copy">各店舗の売り上げをあげるための施策、スタッフの育成が主な業務です。
            イベントの企画、商品展開を考える担当は、各店舗の分析、スタッフとのミーティング、トライ＆エラーを繰り返し、売り上げアップを目指します。お客様を想像する力、予測と振り返りから次につなげる分析力が求められます。
            教育担当は、各部署・店舗スタッフとのさまざまなコミュニケーションからお店の状況・スタッフ状況を把握し、スタッフ一人ひとりが成長できるようにマネージャー・店長の支援をしていきます。多角的な視点、状況把握力と先を見据えた予測、想像力が求められます。</p>
          </li>
          <li class="renewal-career-item">
            <h4 class="renewal-career-description-title">オンライン運営部</h4>
            <p class="renewal-career-description-copy">自社ECサイトおよび外部モールの運営・管理を中心に、サイト内の企画ページ立案・作成や、ショップ内の商品計画を担当します。画面の向こうにいるお客様を想像する力と、常に変化するWEBの中で、スピーディな対応力が求められます。</p>
          </li>
        </ul>
      </div>
    </div>

    <?php import_template( 'parts/renewal-button', array(
      'modifier' => '',
      'url' => resolve_url('recruit-form'),
      'text' => 'ENTRY'
    )); ?>
  </div>
</div>

<?php import_template('parts/renewal-content'); ?>
<?php import_template('parts/renewal-copyright'); ?>
<?php wp_footer(); ?>
