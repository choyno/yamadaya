<?php
/**
  * Template Name: Top Message
 * @author NetBusinessAgent
 * @version 1.0.2
 */
?>
<?php import_template( 'parts/renewal-head'); ?>
<?php import_template('parts/renewal-banner', array(
  'modifier' => 'renewal-banner-message'
)); ?>

<div class="l-container">
<?php
  $breadcrumbs = array (
    'breadcrumbs' => array (
      array (
        'title' => 'RECRUIT TOP',
        'link' => resolve_url('recruit-top'),
      ),
      array (
        'title' => 'TOP MESSAGE',
        'link' => resolve_url('top-message'),
      )
    )
  );
  import_template( 'parts/common-section/breadcrumbs', $breadcrumbs);
?>

  <?php import_template( 'parts/section-heading', array(
    'modifier' => 'l-section-heading-message',
    'title' => 'TOP MESSAGE',
    'text' => 'message',
  )); ?>

  <div class="message">
    <div class="message-feature-image" style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/ceo-message.jpg'?>')"></div>
    <div class="message-quote">
      <h2 class="message-quote-heading">ファッションの世界は、<br>この先もっと広がっていく。</h2>
      <p class="message-quote-copy">代表取締役社長 <span>山田 太郎</span></p>
    </div>

    <div class="message-prelude">
      <p class="message-prelude-text">あなたが「また行きたいな」と思うお店は、どんなお店ですか？ <br>
      ブランドや商品が気に入ったことに加えて、気持ちのいい接客やファッションのワクワク感を与えてくれるようなお店ではないでしょうか。
      </p>
      <div class="message-prelude-image" style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/ceo-prelude.jpg'?>')"></div>
      <p class="message-prelude-text">YAMADAYAでは、目先の売り上げよりも“顧客様づくり“を大切にしています。
      接客したお客様が、もう一度お店に戻ってきてくれた時にはじめて接客が完了する、という考え方です。
      そのため当社では、絶対に購入を無理強いしませんし、お客様に似合っていない服はお勧めしません。</p>
      <p class="message-prelude-text">くつろぎながら服選びをし、コーディネートを楽しみ、YAMADAYAの服に、人に、出会えてよかったと思っていただけるような接客やお店づくりをめざしています。</p>
      <p class="message-prelude-text u-clear">そして今、私たちのライバルは“隣のお店“だけではありません。<br>
      ネット販売や他の業種など、あらゆるものがライバルになる時代。</p>
      <p class="message-prelude-text">“ファッション=服“ではなく、ライフスタイルなども提案できるよう、新しい分野へ挑戦していきたいと考えています。
      それには若い感性や高い志を持った、あなたの力が必要です。<br> 
      すべてはお客様に「また行きたいな」と思っていただくために。<br>
      私たちと一緒に働きませんか。</p>
    </div>

    <?php import_template( 'parts/renewal-button', array(
      'modifier' => '',
      'url'      => resolve_url('recruit-form'),
      'text'     => 'Entry'
    )); ?>
  </div>
</div>


<?php import_template( 'parts/renewal-content'); ?>
<?php import_template( 'parts/renewal-copyright'); ?>
<?php wp_footer(); ?>