<?php
/**
 * Template Name: Recruit Handicapped
 */
?>

<?php import_template('parts/renewal-head'); ?>
<?php import_template('parts/renewal-banner', array(
  'modifier' => 'renewal-banner-recruit'
)); ?>

<div class="l-container">
<?php
  $breadcrumbs = array (
    'breadcrumbs' => array (
      array (
        'title' => 'RECRUIT TOP',
        'link' => resolve_url('recruit-top'),
      ),
      array (
        'title' => '新卒（高校卒）採用向け',
        'link' => resolve_url('recruit-highschool-grad'),
      ),
      array (
        'title' => '新卒（高校卒）採用　募集要項',
      )
    )
  );
  import_template( 'parts/common-section/breadcrumbs', $breadcrumbs);
?>

  <div class="recruit-main-page">
    <div class="recruit-main-wrapper max-width-800">
      <div class="recruit-partime-header">
       <?php import_template( 'parts/section-heading', array(
         'modifier' => 'l-section-heading-new-grad',
         'title' => '新卒（高校卒）採用　募集要項',
         'text' => '',
       )); ?>
      </div>

      <section class="detail">
        <ul class="detail-list">
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">職種</span>
              <p class="detail-copy">ファッションアドバイザー</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">仕事内容</span>
              <p class="detail-copy">レディスアパレル・アクセサリー・ファッション雑貨の接客販売を中心に、商品整理やディスプレイの変更など
              店舗業務全般に携わっていただきます。</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">雇用形態</span>
              <p class="detail-copy">正社員</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">給与</span>
              <p class="detail-copy">月給175,000円（服飾手当・通信手当含む）　※2019年3月実績</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">賞与</span>
              <p class="detail-copy">年２回（７月・１２月）</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">昇給</span>
              <p class="detail-copy">年１回（３月）</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">研修</span>
              <p class="detail-copy">研修店舗にて研修あり（2カ月～）</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">勤務日</span>
              <p class="detail-copy">シフト自己申告制（1カ月単位）</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">勤務時間</span>
              <p class="detail-copy">実働７時間４０分、休憩１１０分、2～3交代制（営業時間により異なりますので詳しくはお問い合わせください。）</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">休日・休暇</span>
              <p class="detail-copy">公休月９日（土日祝の取得・月１回３連休の取得可能）、年間休日１０８日、
              夏季休暇（最大１０日間）・冬期休暇（最大５日間）、有給休暇、産前産後休暇、育児休暇、介護休暇、慶弔休暇</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">福利厚生 待遇</span>
              <p class="detail-copy">各種社会保険完備、交通費支給（50,000円迄／月）、時間外手当あり、定期健康診断補助あり、社員割引制度あり、
              社員寮あり（名古屋・関東のみ）、報奨金制度あり、時短勤務社員制度（時短社員・準社員（パート））あり、
              ※車通勤の可否は店舗により異なりますので詳しくはお問い合わせください。
              ノルマなし、髪型・ネイル自由、服装自由（ショップの雰囲気に合っていれば着用ブランドは問いません）</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">応募前職場見学</span>
              <p class="detail-copy">求人票公開後、随時可能。
              見学希望の方は見学日時の相談をさせていただきますので、就職担当の先生を通して下記問合せ先までご連絡ください。</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">選考方法</span>
              <p class="detail-copy">面接（60分）・作文（30分）・一般常識（30分）　※面接重視</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">応募方法</span>
              <p class="detail-copy">就職担当の先生より、下記問合せ先までご連絡ください。</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">問合せ先</span>
              <p class="detail-copy">052-531-9306（代表）</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">補足事項</span>
              <p class="detail-copy">入社後、まずは先輩社員が１人ひとりにあわせて仕事を任せながら、丁寧に育成していきます。
              接客については、先輩社員のフォローや接客の振り返り・ロールプレイングの実施などを通してスキルを磨いていただきます。
              スタッフの個性を大切に考えているので、販売スタイルをマニュアル化していません。
              また、ノルマはないので数字に追われることなく「また来たい」と思っていただける接客を社員全員で目指しています。</p>
            </div>
          </li>
        </ul>
      </section>
    </div>
  </div>
</div>

<?php import_template('parts/renewal-content'); ?>
<?php import_template('parts/renewal-copyright'); ?>
<?php wp_footer(); ?>
