<?php
/**
  * Template Name: Renewal
 * @author NetBusinessAgent
 * @version 1.0.2
 */
?>
<?php import_template( 'parts/renewal-head'); ?>

<div class="renewal-mv">
  <ul class="renewal-mv-list">
    <li class="renewal-mv-item">
      <section class="renewal-mv-content" style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/mv.jpg'?>')">
        <span class="renewal-mv-text">リテイラーになる。</span>
        <span class="renewal-mv-text">クリエイティブ</span>
        <span class="renewal-mv-text">夢と感動を届ける、</span>
      </section>
    </li>
  </ul>
  <?php import_template( 'parts/renewal-button', array(
    'modifier' => 'renewal-button-mv',
    'url' => 'https://www.ymdy.co.jp/company/',
    'text' => '会社を知る'
  )); ?>
</div>

<?php import_template( 'parts/section-heading', array(
  'modifier' => 'section-heading-essentials l-section-heading-essentials',
  'title' => 'ヤマダヤが大事に<br class="show-sp">していること',
  'text' => 'important'
)); ?>
<?php import_template( 'parts/essentials'); ?>

<div class="l-container l-container-recruit">
  <div class="renewal-recruit-block">
    <?php import_template( 'parts/section-heading', array(
      'modifier' => 'section-heading-recruit l-section-heading-recruit',
      'title' => '採用情報',
      'text' => 'recruit'
    )); ?>

    <?php import_template( 'parts/recruit-section', array (
      'modifier' => 'test',
      'caption' => 'WHAT’ S YAMADAYA',
      'imgUrl' => get_template_directory_uri() . '/images/renewal/recruit-img01.jpg',
      'title' => 'ヤマダヤのキャリア',
      'url' => resolve_url('career-plan'),
      'text' => '詳しく見る'
    )); ?>
    
    <?php import_template( 'parts/recruit-section', array (
      'modifier' => 'recruit-section-two',
      'caption' => 'DATA',
      'imgUrl' => get_template_directory_uri() . '/images/renewal/recruit-img02.jpg',
      'title' => 'ヤマダヤをデータで知る',
      'url' => resolve_url('company-data'),
      'text' => '詳しく見る'
    )); ?>
    
    <?php import_template( 'parts/recruit-section', array (
      'modifier' => '',
      'caption' => 'INTERVIEW',
      'imgUrl' => get_template_directory_uri() . '/images/renewal/recruit-img03.jpg',
      'title' => 'ヤマダヤで働く人',
      'url' => resolve_url('staff'),
      'text' => '詳しく見る'
    )); ?>

    <?php import_template( 'parts/renewal-button', array(
      'modifier' => 'renewal-button-recruit',
      'url' => resolve_url('recruit-top'),
      'text' => '採用ページに行く'
    )); ?>
  </div>

</div>
<div class="l-container">

  <div class="renewal-event">
    <?php import_template( 'parts/section-heading', array(
      'modifier' => 'section-heading-event l-section-heading-event',
      'title' => '最近の出来事',
      'text' => 'event'
    )); ?>

    <a href="https://www.ymdy.co.jp/20190627-1-2/" target="_blank" class="event-section">
      <div class="event-section-image-wrap">
        <span style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/event01.jpg'?>')"></span>
      </div>
      <div class="event-section-content">
        <time class="event-section-time">2019.9.11</time> 
        <span class="event-section-category">オープン＆リニューアル</span>
        <h3 class="event-section-heading">RADIATE 横浜ジョイナス店　9/12 NEW OPEN!</h3>
        <p class="event-section-copy">新規でYMDYメンバーズカードにご登録のお客様
        及び既存のメンバーズ様
        店内全商品10%OFFでご提供致します。</p>
      </div>
    </a>

    <a href="https://www.ymdy.co.jp/20190627-1/" target="_blank" class="event-section">
      <div class="event-section-image-wrap">
        <span style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/event02.jpg'?>')"></span>
      </div>
      <div class="event-section-content">
        <time class="event-section-time">2019.6.27</time> 
        <span class="event-section-category">オープン＆リニューアル</span>
        <h3 class="event-section-heading">MEDOC ｻﾝｴｰ浦添ﾊﾟﾙｺｼﾃｨ店　6/27 NEW OPEN!</h3>
        <p class="event-section-copy">新規でYMDYメンバーズカードにご登録のお客様
        及び既存のメンバーズ様
        店内全商品10%OFFでご提供致します。</p>
      </div>
    </a>

    <a href="https://www.ymdy.co.jp/20190620-1/" target="_blank" class="event-section">
      <div class="event-section-image-wrap">
        <span style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/event03.jpg'?>')"></span>
      </div>
      <div class="event-section-content">
        <time class="event-section-time">2019.6.20 </time> 
        <span class="event-section-category">オープン＆リニューアル</span>
        <h3 class="event-section-heading">LASUD ｲｵﾝﾓｰﾙ大和郡山店　6/20 RENEWAL OPEN!</h3>
        <p class="event-section-copy">新規でYMDYメンバーズカードにご登録のお客様
        及び既存のメンバーズ様
        店内全商品10%OFFでご提供致します。</p>
      </div>
    </a>

    <?php import_template( 'parts/renewal-button', array(
      'modifier' => 'renewal-button-event',
      'target' => '_blank',
      'url' => 'https://www.ymdy.co.jp/category-feature/',
      'text' => '出来事を見る'
    )); ?>
  </div>

</div>

<?php import_template( 'parts/renewal-contact'); ?>
<?php import_template( 'parts/renewal-copyright'); ?>

<!--?php get_footer(); ?-->
<?php wp_footer(); ?>
