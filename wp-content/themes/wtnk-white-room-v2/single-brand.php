<?php
/**
 * @author NetBusinessAgent
 * @version 1.0.0
 */
get_header(); ?>
	<header class="entry-header">
		<?php /*whiteroom_the_page_heading();*/ ?>
		<?php whiteroom_the_bread_crumb(); ?>
	<!-- end .entry-header --></header>

	<div class="row single-brand-wrapper">
		<main id="main" role="main" class="col-12 single-brand-in">
<?php
$terms = get_the_terms("" ,"classify");
//print_r($terms);
//echo $terms[0]->name; 
if($terms[0]->name == "product-brand"):
?>
<h1 class="entry-title">Fashion Brand</h1>
<?php else:?>

			<h1 class="entry-title">Shop Brand</h1>
<?php endif;?>


			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content">
<div class="imgcenter">
<?php the_post_thumbnail('brand_thumbnail');?>
</div>
			<h2 ><?php the_title(); ?></h2>

						<?php the_content(); ?>
						<!--<?php
						wp_link_pages( array(
							'before' => '<div class="pager"><p>',
							'after'  => '</p></div>',
							'pagelink' => '<span class="current">%</span>',
						) );
						?>-->

					<!-- end .entry-content --></div>

				<!-- end .hentry --></article>
			<?php endwhile; ?>
		<!-- end #main --></main>
		<aside id="sub" class="col-3">
			<?php get_sidebar(); ?>
		<!-- end #sub --></aside>
	<!-- end .row --></div>
	
<?php get_footer(); ?>
