<?php get_header(); ?>

<div id="content">
<section>

<header class="entry-header">		
<h1 class="title first">Shop</h1>
<?php whiteroom_the_bread_crumb(); ?>
<!-- end .entry-header -->
</header>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<article  class="content">
<?php /*echo __FILE__;*/ ?>
<?php
$custom_fields = get_post_custom();
$address = $custom_fields["住所"][0];

$filepath = $absolute_path."img/".$custom_fields["店舗コード"][0].".jpg";
//echo $filepath;
if(has_post_thumbnail()){
$thumbnail_id = get_post_thumbnail_id(); 

// mediumサイズの画像内容を取得（引数にmediumをセット）
$eye_img = wp_get_attachment_image_src( $thumbnail_id , 'large' );

$filename = $eye_img[0];
$imgtag = '<img class="store_imgs" src="'.$filename .'">';
}
elseif(file_exists($filepath)){
$filename = esc_url( home_url( '/' ) )."img/".$custom_fields["店舗コード"][0].".jpg";
//echo $filename;
$imgtag = '<img class="store_imgs" src="'.$filename .'">';
}
else{
//echo "画像がありません";
//echo $filename;
$imgtag = "<img class='store_imgs' src='".get_template_directory_uri()."/images/noimg200.png' alt='no image'>";
}
?>
<div class="store_text">
<h2 class="store_brand"><?php echo get_the_term_list( get_the_ID(), "brands", "", "　/　","");  ?></h2>
<p class="store_name"><?php echo $custom_fields["店舗コード"][0]; ?>　　<?php the_title(); ?></p>

<?php echo $imgtag; ?>

</div>
<!--store_text-->

<?php if($custom_fields["郵便番号"][0]): ?>
<div class="store_map">
<?php 
$url = "https://maps.googleapis.com/maps/api/geocode/json?address=".$address;
$json = file_get_contents($url);
$json = mb_convert_encoding($json, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
$arr = json_decode($json,true);
//print_r($arr);

$lat = $arr[results][0]["geometry"]["location"]["lat"];
$lng = $arr[results][0]["geometry"]["location"]["lng"];
?>

<iframe width="650" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.jp/maps?ll=<?php echo $lat;?>,<?php echo $lng;?>&q=<?php $address;?>&output=embed&t=m&z=16"></iframe>

<p>〒<?php echo $custom_fields["郵便番号"][0]; ?><br/>
<?php 
echo nl2br(str_replace('\n',"\n",$address)); ?></p>

<?php else:?>
ただいまデータ掲載準備中です。
<?php endif;?>

<?php if($custom_fields["TEL"][0]): ?>
<p>TEL : <?php echo $custom_fields["TEL"][0]; ?></p>
<?php endif;?>
</div>
<!--store_map-->

</div>
<!--"content"-->

<p class="mark"><a href="<?php echo esc_url( home_url( '/' ) );?>shopsearch/">ブランド検索へ戻る</p>

    <?php /*wp_link_pages('before=<p id="pageLinks">ページ:&after=</p>');*/ ?> 
  </article>

	<?php endwhile;?>

<?php whiteroom_the_pager(); ?>

  <!--<div class="pagenav">
			<span class="prev"><?php previous_post_link( '%link', '&laquo; 前のページ' ); ?></span>          
			<span class="next"><?php next_post_link( '%link', '次のページ &raquo;' ); ?></span>
	</div>-->
<!--pagenav-->

  <?php endif; ?><!--havepost-->

</section>
<?php get_footer(); ?>