<?php
/**
  * Template Name: Staff
 */
?>

<?php import_template('parts/renewal-head'); ?>
<?php import_template('parts/renewal-banner'); ?>


<div class="l-container">
<?php
  $breadcrumbs = array (
    'breadcrumbs' => array (
      array (
        'title' => 'スタッフ紹介',
        'link' => resolve_url('staff'),
      )
    )
  );
  import_template( 'parts/common-section/breadcrumbs', $breadcrumbs);
?>
</div>

<div class="staff">
  <?php import_template('parts/section-heading', array(
    'modifier' => 'l-section-heading-staff',
    'title' => 'スタッフ紹介',
    'text' => 'staff'
  )); ?>

  <div class="staff-content">
    <div class="special-interview">
      <h3 class="special-interview-heading">SPECIAL INTERVIEW</h3>
      <div class="special-interview-contents">
        <div class="l-container">
<!--           <section class="special-interview-block">
            <div class="special-interview-video"></div>
            <div class="special-interview-words">
              <h4 class="special-interview-title">タイトルが入ります</h4>
              <p class="special-interview-text">テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。テキストが入ります。</p>
            </div>
          </section> -->

          <section class="special-interview-block">
            <div class="special-interview-video">
              <video controls>
                <source src="<?php echo get_template_directory_uri() . '/images/renewal/special-interview.mp4'?>" type="video/mp4">
            
                Your browser does not support HTML5 video.
              </video>
            </div>
            <div class="special-interview-words">
              <h4 class="special-interview-title">ファッションアドバイザー座談会</h4>
              <p class="special-interview-text">新卒入社、中途入社のファッションアドバイザーが、リアルに感じていることを話してもらいました！</p>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
  <?php import_template('parts/member-interview'); ?>
  <?php import_template('parts/renewal-button', array(
    'url' => resolve_url('recruit-form'),
    'text' => 'ENTRY'
  )); ?>

</div>

<?php import_template('parts/renewal-content'); ?>
<?php import_template('parts/renewal-copyright'); ?>
<?php wp_footer(); ?>