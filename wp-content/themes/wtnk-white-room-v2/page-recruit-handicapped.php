<?php
/**
 * Template Name: Recruit Handicapped
 */
?>

<?php import_template('parts/renewal-head'); ?>
<?php import_template('parts/renewal-banner', array(
  'modifier' => 'renewal-banner-recruit'
)); ?>

<div class="l-container">
<?php
  $breadcrumbs = array (
    'breadcrumbs' => array (
      array (
        'title' => 'RECRUIT TOP',
        'link' => resolve_url('recruit-top'),
      ),
      array (
        'title' => '障がい者採用',
        'link' => resolve_url('recruit-handicapped'),
      )
    )
  );
  import_template( 'parts/common-section/breadcrumbs', $breadcrumbs);
?>

  <div class="recruit-main-page">
    <div class="recruit-main-wrapper max-width-800">
      <div class="recruit-partime-header">
       <?php import_template( 'parts/section-heading', array(
         'modifier' => 'l-section-heading-new-grad',
         'title' => '障がい者採用 募集要項',
         'text' => '',
       )); ?>
      </div>

      <section class="detail">
        <!--?php import_template( 'parts/common-section/content-title', array(
          'modifier' => 'content-title-new-grad',
          'title' => 'NEWS',
        )); ?-->
        <ul class="detail-list">
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">メッセージ</span>
              <p class="detail-copy">※ヤマダヤでは障がいをお持ちの方の採用も随時行っております。
              障がいの種類(1級~3級)や業務経験の有無は関係ありません。
              努力と挑戦する気持ちがあれば大丈夫です。</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">業務内容</span>
              <p class="detail-copy">本社物流センター内での軽作業(発送業務~商品管理)</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">応募資格</span>
              <p class="detail-copy">高卒以上 18歳~60歳位までの方</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">雇用形態</span>
              <p class="detail-copy">パート社員(準社員)</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">給与</span>
              <p class="detail-copy">時給950円 x 実働時間</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">福利厚生</span>
              <p class="detail-copy">各種社会保険完備、社員割引、交通費実費支給(1ヶ月、5万円まで支給)</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">勤務時間</span>
              <p class="detail-copy">9:00~18:00(休憩60分) の間で、実働1日6~7時間勤務可能な方</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">休日</span>
              <p class="detail-copy">週休2日制(月に1度または2度の土曜出勤有り ※ 応相談)</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">環境</span>
              <p class="detail-copy">エレベーター有り、車椅子不可、トイレ和洋式、障がい者用トイレ有り</p>
            </div>
          </li>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">応募方法</span>
              <p class="detail-copy">履歴書（顔写真貼付）に障がい者手帳のコピーを添付し、
              必要事項を明記の上、「人事部障がい者採用係」宛にご郵送ください。
              ※書類選考をさせていただき、選考を通過した方のみ10日以内にご連絡を差しあげます。
              尚、書類選考で不採用の方には応募書類を返却いたします。</p>
            </div>
          </li>
        </ul>

       <div class="detail-contact">
         <span class="detail-contact-heading">【応募書類送付先】</span>
         <p class="detail-contact-number">TEL: 052-531-9306（代表）</p>
         <p class="detail-contact-email">〒451-8550 名古屋市西区城西1-3-1 株式会社ヤマダヤ 人事部障がい者採用係</p>
       </div>
      </section>
    </div>
  </div>
  
</div>
<?php import_template('parts/renewal-content'); ?>
<?php import_template('parts/renewal-copyright'); ?>
<?php wp_footer(); ?>
