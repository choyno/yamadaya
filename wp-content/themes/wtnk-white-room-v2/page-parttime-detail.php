<?php
/**
 * Template Name: Part-time Detail
 */
?>

<?php import_template('parts/renewal-head'); ?>
<?php import_template('parts/renewal-banner', array(
  'modifier' => 'renewal-banner-recruit'
)); ?>

<div class="l-container">
  <?php import_template( 'parts/common-section/breadcrumbs', array(
    'paths' => ["TOP", "RECRUIT TOP", "店 募集要項"],
  )); ?>

  <div class="recruit-main-page">
    <div class="recruit-main-wrapper max-width-800">
      <div class="recruit-partime-header">
       <?php import_template( 'parts/section-heading', array(
         'modifier' => 'l-section-heading-new-grad',
         'title' => '店 募集要項',
         'text' => '',
       )); ?>
      </div>

      <section class="detail">
        <!--?php import_template( 'parts/common-section/content-title', array(
          'modifier' => 'content-title-new-grad',
          'title' => 'NEWS',
        )); ?-->
        <ul class="detail-list">
          <li class="detail-item">
            <a href="#" class="detail-link">
              <time class="detail-time">2019.08.10</time>
              <p class="detail-copy">新卒2次募集をはじめました！</p>
            </a>
          </li>
          <li class="detail-item">
            <a href="#" class="detail-link">
              <time class="detail-time">2019.08.10</time>
              <p class="detail-copy">新卒2次募集をはじめました！</p>
            </a>
          </li>
          <li class="detail-item">
            <a href="#" class="detail-link">
              <time class="detail-time">2019.08.10</time>
              <p class="detail-copy">
                テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ
                ストテキストテキストテキストテキストテキスト
              </p>
            </a>
          </li>
          <li class="detail-item">
            <a href="#" class="detail-link">
              <time class="detail-time">2019.08.10</time>
              <p class="detail-copy">新卒2次募集をはじめました！</p>
            </a>
          </li>
        </ul>
      </section>
    </div>
  </div>
  <?php import_template('parts/renewal-button', array(
    'url' => resolve_url('recruit-form'),
    'text' => 'ENTRY'
  )); ?>
  <?php import_template('parts/renewal-content'); ?>
</div>

<?php import_template('parts/renewal-copyright'); ?>
<?php wp_footer(); ?>
