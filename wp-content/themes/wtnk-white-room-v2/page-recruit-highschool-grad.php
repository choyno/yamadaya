<?php
/**
 * Template Name: Recruit Highschool Grad
 */
?>

<?php import_template('parts/renewal-head'); ?>
<?php import_template('parts/renewal-banner', array(
  'modifier' => 'renewal-banner-recruit'
)); ?>

<div class="l-container">
<?php
  $breadcrumbs = array (
    'breadcrumbs' => array (
      array (
        'title' => 'RECRUIT TOP',
        'link' => resolve_url('recruit-top'),
      ),
      array (
        'title' => '新卒（高校卒）採用向け',
        'link' => resolve_url('recruit-highschool-grad'),
      )
    )
  );
  import_template( 'parts/common-section/breadcrumbs', $breadcrumbs);
?>

  <div class="recruit-main-page">
    <div class="recruit-main-wrapper">
      <div class="recruit-partime-header">
       <?php import_template( 'parts/section-heading', array(
         'modifier' => 'l-section-heading-highschool',
         'title' => '新卒（高校卒）採用向け',
         'text' => 'recruit',
       )); ?>
      </div>
      <section class="recruit-partime-search-result-highschool">
        <ul class="recruit-partime-result-list">
          <li class="recruit-partime-result-item">
             <?php import_template('parts/job-card', array(
               'modifier'  => '',
               'tags'      => ['正社員'],
               'heading'   => '全国各SHOP（北海道～沖縄 約150店舗）',
               'address'   => '', // Street Address
               'category'  => 'ファッションアドバイザー',
               'url'       => resolve_url('highschool-detail'),
               'linktext'  => '詳細を見る',
             )); ?>
           </li>
        </ul>
      </section><!-- end of partime seach result -->
    </div>
  </div>
</div>
<?php import_template('parts/renewal-content'); ?>
<?php import_template('parts/renewal-copyright'); ?>
<?php wp_footer(); ?>
