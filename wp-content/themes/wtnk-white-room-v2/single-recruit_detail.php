<?php
/**
 * Template Name: Recruit Single Details
 */
?>

<?php import_template('parts/renewal-head'); ?>
<?php import_template('parts/renewal-banner', array(
  'modifier' => 'renewal-banner-recruit'
)); ?>

<div class="l-container">

  <?php $get_field = get_fields(); ?>
  <?php $recruit_page_type = $get_field["recruit_page_type"]; ?>
  <?php $job_type = $get_field["job_type"]['label']; ?>
  <?php $recruit_job_description = $get_field["recruit_job_description"]; ?>
  <?php $employment_status = $get_field["employment_status"]['label']; ?>
  <?php $recruit_salary = $get_field["recruit_salary"]; ?>
  <?php $recruit_address = $get_field["recruit_address"]; ?>
  <?php $recruit_bonus = $get_field["recruit_bonus"]; ?>
  <?php $recruit_promotion = $get_field["recruit_promotion"]; ?>
  <?php $recruit_training = $get_field["recruit_training"]; ?>
  <?php $recruit_trial_period = $get_field["recruit_trial_period"]; ?>
  <?php $recruit_qualification = $get_field["recruit_qualification"]; ?>
  <?php $recruit_working_day = $get_field["recruit_working_day"]; ?>
  <?php $recruit_working_time = $get_field["recruit_working_time"]; ?>
  <?php $recruit_holiday_vacation = $get_field["recruit_holiday_vacation"]; ?>
  <?php $recruit_welfare = $get_field["recruit_welfare"]; ?>
  <?php $recruit_application_method = $get_field["recruit_application_method"]; ?>
  <?php $recruit_contact = $get_field["recruit_contact"]; ?>


<?php
  if($recruit_page_type['value'] == 'part_time') {
    $recruit_page_link =  resolve_url('recruit-partime');

  } else {
   $recruit_page_link =  resolve_url('recruit-midcareer');
  }
?>


  <?php
    $breadcrumbs = array (
      'breadcrumbs' => array (
        array (
          'title' => 'RECRUIT TOP',
          'link' => resolve_url('recruit-top')
        ),
        array (
          'title' => $recruit_page_type['label'],
          'link' =>  $recruit_page_link,
        ),
        array (
          'title' => get_the_title()
        )
      )
    );
    import_template( 'parts/common-section/breadcrumbs', $breadcrumbs);
  ?>

  <div class="recruit-main-page">
    <div class="recruit-main-wrapper max-width-800">
      <div class="recruit-partime-header">
       <?php import_template( 'parts/section-heading', array(
         'modifier' => 'l-section-heading-new-grad',
         'title' => get_the_title(),
         'text' => '',
       )); ?>
      </div>

      <section class="detail">
        <ul class="detail-list">
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">募集職種</span>
              <p class="detail-copy"> <?php echo $job_type; ?></p>
            </div>
          </li>

          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">住所</span>
              <p class="detail-copy"> <?php echo $recruit_address; ?></p>
            </div>
          </li>

          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">仕事内容</span>
              <p class="detail-copy"><?php echo $recruit_job_description; ?></p>
            </div>
          </li>

          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">雇用形態</span>
              <p class="detail-copy"><?php echo $employment_status; ?></p>
            </div>
          </li>

          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">給与</span>
              <p class="detail-copy"><?php echo $recruit_salary; ?></p>
            </div>
          </li>

        <?php
          if($recruit_page_type["value"] === "mid_career") {
        ?>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">賞与</span>
              <p class="detail-copy"><?php echo $recruit_bonus; ?></p>
            </div>
          </li>

          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">昇給</span>
              <p class="detail-copy"><?php echo $recruit_promotion; ?></p>
            </div>
          </li>

          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">研修</span>
              <p class="detail-copy"><?php echo $recruit_training; ?></p>
            </div>
          </li>

          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">試用期間</span>
              <p class="detail-copy"><?php echo $recruit_trial_period; ?></p>
            </div>
          </li>
        <?php
           }
        ?>

          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">応募条件　勤務条件</span>
              <p class="detail-copy"><?php echo $recruit_qualification; ?></p>
            </div>
          </li>

          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">勤務日</span>
              <p class="detail-copy"><?php echo $recruit_working_day; ?></p>
            </div>
          </li>

          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">勤務時間</span>
              <p class="detail-copy"><?php echo $recruit_working_time; ?></p>
            </div>
          </li>

        <?php
          if($recruit_page_type["value"] === "mid_career") {
        ?>
          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">休日・休暇</span>
              <p class="detail-copy"><?php echo $recruit_holiday_vacation; ?></p>
            </div>
          </li>
        <?php
           }
         ?>

          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">福利厚生　待遇</span>
              <p class="detail-copy"><?php echo $recruit_welfare; ?></p>
            </div>
          </li>

          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">応募方法</span>
              <p class="detail-copy"><?php echo $recruit_application_method; ?></p>
            </div>
          </li>

          <li class="detail-item">
            <div class="detail-block">
              <span class="detail-label">問い合わせ先</span>
              <p class="detail-copy"><?php echo $recruit_contact; ?></p>
            </div>
          </li>
       </ul>

        <?php
          if(empty(($recruit_page_type["value"] === "part_time") OR ($recruit_page_type["value"] === "mid_career")))  {
        ?>
          <div class="detail-contact">
            <span class="detail-contact-heading">応募先</span>
            <p class="detail-contact-number">Tel: 123-456-789</p>
            <p class="detail-contact-email">Mail: <a href="mailto:sample@sample.com">sample@sample.com</a></p>
          </div>
        <?php
          }
        ?>
      </section>
    </div>
  </div>
  <?php import_template('parts/renewal-button', array(
    'url' => resolve_url('recruit-form'),
    'text' => 'ENTRY'
  )); ?>
  <?php import_template('parts/renewal-content'); ?>
</div>

<?php import_template('parts/renewal-copyright'); ?>
<?php wp_footer(); ?>
