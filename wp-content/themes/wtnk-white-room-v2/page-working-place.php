<?php
/**
  * Template Name: Working Place
 */
?>


<?php import_template('parts/renewal-head'); ?>
<?php import_template('parts/renewal-banner'); ?>

<div class="l-container">
<?php
  $breadcrumbs = array (
    'breadcrumbs' => array (
      array (
        'title' => 'RECRUIT TOP',
        'link' => resolve_url('recruit-top'),
      ),
      array (
        'title' => '勤務地・福利厚生とサポート制度',
        'link' => resolve_url('working-place'),
      )
    )
  );
  import_template( 'parts/common-section/breadcrumbs', $breadcrumbs);
?>

  <div class="working-place">
    <?php import_template( 'parts/section-heading', array(
      'modifier' => 'l-section-heading-worked-place',
      'title' => '勤務地・福利厚生と<br>サポート制度',
      'text' => 'work location/welfare',
    )); ?>

    <section class="working-place-office">
      <?php import_template( 'parts/common-section/content-title', array(
        'modifier' => 'common-content-title-office',
        'title' => '勤務地 本社',
      )); ?>

      <div class="working-place-office-image" style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/workplace/buildings.jpg'?>')"></div>
      <p class="working-place-copy">
        ヤマダヤの本社が、2019年8月16日に新しく生まれ変わりました。<br>
        ビルに洋服がまとうイメージで、クリエイティブリテイラーとしてのヤマダヤを体現しています。
      </p>
    </section>

    <div class="working-place-interior">
      <div class="working-place-interior-image" style="background-image: url(<?php echo get_template_directory_uri() . '/images/renewal/workplace/workplace.jpg'?>)"></div>
      <p class="working-place-interior-copy">
        ランチタイムになると、5階のラウンジにみんな集まって食事をしています。日差しがしっかり入るカフェテリアのようで、会話もはずみます。自販機や、流し台、電子レンジ、冷蔵庫等も完備しています。
      </p>
    </div>
  </div>
</div>

<section class="working-place-gallery">
  <div class="l-container">
    <div class="working-place-gallery-title"><span>photo</span></div>
  </div>

  <div class="working-place-gallery-content">

    <div class="working-place-gallery-list js-working-place-gallery">
      <div class="working-place-gallery-item">
        <a class="working-place-gallery-anchor" href="<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img01.jpg'?>">
          <span class="working-place-gallery-image" style="background-image: url(<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img01.jpg'?>)" ></span>
        </a>
      </div>
      <div class="working-place-gallery-item">
        <a class="working-place-gallery-anchor" href="<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img02.jpg'?>">
          <span class="working-place-gallery-image" style="background-image: url(<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img02.jpg'?>)" ></span>
        </a>
      </div>
      <div class="working-place-gallery-item">
        <a class="working-place-gallery-anchor" href="<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img03.jpg'?>">
          <span class="working-place-gallery-image" style="background-image: url(<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img03.jpg'?>)" ></span>
        </a>
      </div>
      <div class="working-place-gallery-item">
        <a class="working-place-gallery-anchor" href="<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img04.jpg'?>">
          <span class="working-place-gallery-image" style="background-image: url(<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img04.jpg'?>)" ></span>
        </a>
      </div>
      <div class="working-place-gallery-item">
        <a class="working-place-gallery-anchor" href="<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img05.jpg'?>">
          <span class="working-place-gallery-image" style="background-image: url(<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img05.jpg'?>)" ></span>
        </a>
      </div>
      <div class="working-place-gallery-item">
        <a class="working-place-gallery-anchor" href="<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img06.jpg'?>">
          <span class="working-place-gallery-image" style="background-image: url(<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img06.jpg'?>)" ></span>
        </a>
      </div>
      <div class="working-place-gallery-item">
        <a class="working-place-gallery-anchor" href="<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img07.jpg'?>">
          <span class="working-place-gallery-image" style="background-image: url(<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img07.jpg'?>)" ></span>
        </a>
      </div>
      <div class="working-place-gallery-item">
        <a class="working-place-gallery-anchor" href="<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img08.jpg'?>">
          <span class="working-place-gallery-image" style="background-image: url(<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img08.jpg'?>)" ></span>
        </a>
      </div>
      <div class="working-place-gallery-item">
        <a class="working-place-gallery-anchor" href="<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img09.jpg'?>">
          <span class="working-place-gallery-image" style="background-image: url(<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img09.jpg'?>)" ></span>
        </a>
      </div>
      <div class="working-place-gallery-item">
        <a class="working-place-gallery-anchor" href="<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img010.jpg'?>">
          <span class="working-place-gallery-image" style="background-image: url(<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img010.jpg'?>)" ></span>
        </a>
      </div>
      <div class="working-place-gallery-item">
        <a class="working-place-gallery-anchor" href="<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img011.jpg'?>">
          <span class="working-place-gallery-image" style="background-image: url(<?php echo get_template_directory_uri() . '/images/renewal/workplace/office-img011.jpg'?>)" ></span>
        </a>
      </div>
    </div>
  </div>
</section>

<div class="l-container">
    <section class="working-place-store">
      <?php import_template( 'parts/common-section/content-title', array (
      'modifier' => 'common-content-title-shop',
      'title' => '全国の店舗',
      )); ?>

      <div class="working-place-store-image" style="background-image: url(<?php echo get_template_directory_uri() . '/images/renewal/workplace/store-front.jpg'?>)"></div>

      <p class="working-place-copy">
        ヤマダヤは、全国に約150店舗展開しています。800名を超えるヤマダヤのファッションアドバイザー、ショップマスターたちは日々、全国の店舗でお客さまにファッションを通じて特別な時間を届けています。
      </p>
      <p class="working-place-copy"><span>全国の店舗で活躍する主な職種職位</span>: ファッションアドバイザー、スーパーファッションアドバイザー、ショップマスター、ブランドマネージャー、ストアバイヤー、スーパーバイザー</p>
      <a class="working-place-button" href="https://www.ymdy.co.jp/shopsearch/" target="_blank">店舗を探す</a>
    </section>

    <?php import_template( 'parts/common-section/content-title', array(
      'title' => 'ヤマダヤの福利厚生と<br class="show-sp">サポート制度',
    )); ?>

    <?php import_template('parts/benefit'); ?>
</div>

<div class="workplace-place-award">
  <div class=" l-container">
    <div class="workplace-place-award-block">
      <div class="workplace-place-award-image" style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/workplace/award.png'?>')"></div>
      <div class="workplace-place-award-content">
        <h3 class="workplace-place-award-title">Forbes JAPAN <br>WOMEN AWARD 2018選出</h3>
        <p class="workplace-place-award-copy">
          「意欲ある女性が働きやすい環境づくりを積極的に行っている企業」を表彰するForbes JAPAN <br/>
          WOMEN AWARD 2018に従業員300名以上1000名以下の部で、第8位に選出されました！
        </p>
        <div class="workplace-place-award-buttons">
          <a class="workplace-place-award-button" href="https://prtimes.jp/main/html/rd/p/000000030.000010727.html" target="_blank">PRTIMES</a>
          <a class="workplace-place-award-button" href="https://forbesjapan.com/womenaward/" target="_blank">Forbes JAPAN WOMEN AWARD</a>
        </div>
      </div>
    </div>
  </div>
</div>

<?php import_template('parts/renewal-button', array(
  'modifier' => '',
  'url' => resolve_url('recruit-form'),
  'text' => 'ENTRY',
)); ?>


<?php import_template('parts/renewal-content'); ?>
<?php import_template('parts/renewal-copyright'); ?>
<?php wp_footer(); ?>