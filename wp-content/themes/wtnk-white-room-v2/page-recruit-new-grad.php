<?php
/**
 * Template Name: Recruit New Grad
 */
?>

<?php import_template('parts/renewal-head'); ?>
<?php import_template('parts/renewal-banner', array(
  'modifier' => 'renewal-banner-recruit'
)); ?>

<div class="l-container">
  <?php
    $breadcrumbs = array (
      'breadcrumbs' => array (
        array (
          'title' => 'RECRUIT TOP',
          'link' => resolve_url('recruit-top'),
        ),
        array (
          'title' => '新卒(大学院・大学・短大・専門卒)採用向け',
          'link' => resolve_url('recruit-new-grad'),
        )
      )
    );
    import_template( 'parts/common-section/breadcrumbs', $breadcrumbs); 
  ?>

  <div class="recruit-main-page">
    <div class="recruit-main-wrapper max-width-800">
      <div class="recruit-partime-header">
       <?php import_template( 'parts/section-heading', array(
         'modifier' => 'l-section-heading-new-grad',
         'title' => '新卒採用<br class="show-sp">大学院・大学・短大・専門卒向け',
         'text' => 'recruit',
       )); ?>
      </div>

      <section class="detail">
        <?php import_template( 'parts/common-section/content-title', array(
          'modifier' => 'content-title-new-grad',
          'title' => 'NEWS',
        )); ?>
        <ul class="detail-list">
          <li class="detail-item">
            <a href="javascript:;" class="detail-block">
              <time class="detail-time">2019.12.18</time>
              <p class="detail-copy">リクルートサイトリニューアルしました</p>
            </a>
          </li>
        </ul>
      </section>
    </div>
  </div>
  <?php import_template('parts/renewal-button', array(
    'url' => 'https://job.mynavi.jp/21/pc/search/corp38851/outline.html',
    'target' => '_blank',
    'text' => 'ENTRY'
  )); ?>
  <?php import_template('parts/renewal-content'); ?>
</div>

<?php import_template('parts/renewal-copyright'); ?>
<?php wp_footer(); ?>
