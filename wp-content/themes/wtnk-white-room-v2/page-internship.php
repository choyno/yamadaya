<?php
/**
  * Template Name: Internship
 */
?>

<?php import_template('parts/renewal-head'); ?>
<?php import_template('parts/renewal-banner'); ?>

<div class="internship">
  <div class="l-container">

<?php
  $breadcrumbs = array (
    'breadcrumbs' => array (
      array (
        'title' => 'RECRUIT TOP',
        'link' => resolve_url('recruit-top'),
      ),
      array (
        'title' => '新卒採用 インターンシップ',
        'link' => resolve_url('recruit-internship'),
      )
    )
  );
  import_template( 'parts/common-section/breadcrumbs', $breadcrumbs);
?>

    <?php import_template('parts/section-heading', array(
      'modifier' => 'l-section-heading-internship',
      'title' => '新卒採用 <br>インターンシップ',
      'text' => 'internship',
    )); ?>
    <p class="internship-intro">ヤマダヤのことはもちろんアパレル業界の仕事について知っていただくために、 
    インターンシップを実施しています。 2つのコースを時期に合わせて開催しています。<br> 
    ヤマダヤについて知りたい方、アパレル業界の仕事に興味のある方はぜひご参加ください。</p>
    <div class="internship-schedule">
      <a href="#intership-day01" class="internship-day">1day インターンシップ</a>
      <a href="#intership-day03" class="internship-day">3days インターンシップ</a>
    </div>

    <section class="conference" id="intership-day01">
      <h3 class="conference-title"><span>1day インターンシップ</span></h3>
      <div class="conference-mv" style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/internship/internship-01.jpg'?>')"></div>
      <p class="conference-intro">1日の中で、グループワークを中心に仕事体験ができるコースです。
      ファッションアドバイザーや企画バイヤー、営業、プレス職など総合的に体験ができます。
      体験した方からはアパレルで仕事をする楽しさ、面白さはもちろん、 もっと知りたくなったという声をいただいています。
      </p>
      <h4 class="conference-subtitle">グループワーク例</h4>
      <div class="conference-activity">
        <div class="conference-activity-row">
          <div class="conference-activity-col">
            <span class="conferrence-activity-title">企画バイヤー職</span>
            <ul class="conferrence-activity-list">
              <li class="conferrence-activity-item">ブランドを立ち上げよう（ブランド企画）</li>
              <li class="conferrence-activity-item">ブランドを広めるためのPR企画とプレゼンをしよう!</li>
            </ul>
          </div>
          <div class="conference-activity-col">
            <span class="conferrence-activity-title">プレス職</span>
            <ul class="conferrence-activity-list">
              <li class="conferrence-activity-item">今季のトレンドをアピールするスタイリングを考えよう</li>
              <li class="conferrence-activity-item">instagramで○○を発信しよう</li>
            </ul>
          </div>
        </div>
        <div class="conference-activity-row">
          <div class="conference-activity-col">
            <span class="conferrence-activity-title">営業職</span>
            <ul class="conferrence-activity-list">
              <li class="conferrence-activity-item">数字分析からお客様のニーズを考えよう</li>
              <li class="conferrence-activity-item">□月に行うイベント企画を考えよう</li>
            </ul>
          </div>
          <div class="conference-activity-col">
            <span class="conferrence-activity-title">ファッションアドバイザー職</span>
            <ul class="conferrence-activity-list">
              <li class="conferrence-activity-item">お客様に「また来たい」と思って頂けるディスプレイ、接客を考えよう</li>
              <li class="conferrence-activity-item">接客を体験してみよう</li>
            </ul>
          </div>
        </div>
      </div>

      <div class="conference-images">
        <div class="conference-image" style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/internship/conferrence-01.jpg'?>')"></div>
        <div class="conference-image" style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/internship/conferrence-02.jpg'?>')"></div>
      </div>

      <div class="conference-details">
        <p class="conference-details-text">時期により、実施内容は異なります。
        詳細・応募は、「マイナビ」の「株式会社ヤマダヤ」ページをご覧ください。</p>

      <?php import_template('parts/renewal-button', array(
        'modifier' => 'renew-button-internship',
        'target' => '_blank',
        'url' => 'https://job.mynavi.jp/21/pc/search/corp38851/outline.html',
        'text' => 'ENTRY',
      )); ?>
      </div>
    </section>

    <section class="conference" id="intership-day03">
      <h3 class="conference-title"><span>3days インターンシップ</span></h3>
      <div class="conference-mv" style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/internship/internship-02.jpg'?>')"></div>
      <div class="conference-activity-row">
        <div class="conference-activity-col">
          <p class="conference-activity-copy">短期間で、ファッションアドバイザー職の仕事体験ができるコースです。 直営店舗で働くスタッフとコミュニケーションをとりながら、 店舗の仕事について理解を深めることができます。体験した方からは、アパレルの仕事を今までよりも知ることができた。ヤマダヤの接客スタイルや働く博境を知ることができました。</p>
        </div>

        <div class="conference-activity-col">
          <span class="conferrence-activity-title">店頭 体験例</span>
          <ul class="conferrence-activity-list">
            <li class="conferrence-activity-item">接客の基本のレクチャー</li>
            <li class="conferrence-activity-item">商品整理</li>
            <li class="conferrence-activity-item">接客（スタッフとのロールプレイング、お客様への接客）</li>
            <li class="conferrence-activity-item">ディスプレイの変更</li>
            <li class="conferrence-activity-item">商品の情報や知識の把握ワーク</li>
          </ul>
          <p class="conference-activity-copy">参考資料、ワークシートを活用しています。</p>
        </div>
      </div>

      <div class="conference-images">
        <div class="conference-image" style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/internship/conferrence-03.jpg'?>')"></div>
        <div class="conference-image" style="background-image: url('<?php echo get_template_directory_uri() . '/images/renewal/internship/conferrence-04.jpg'?>')"></div>
      </div>

      <div class="conference-details">
        <p class="conference-details-text">店構状況により体験内容が変わる場合があります。
        詳細・応募は、「マイナビ」の「株式会社ヤマダヤ」ページをご覧ください。
        ※単位認定（取得）型インターンシップも随時受入中！</p>

      <?php import_template('parts/renewal-button', array(
        'modifier' => 'renew-button-internship',
        'target' => '_blank',
        'url' => 'https://job.mynavi.jp/21/pc/search/corp38851/outline.html',
        'text' => 'ENTRY',
      )); ?>
      </div>
    </section>

  </div>
</div>

<?php import_template('parts/renewal-content'); ?>
<?php import_template('parts/renewal-copyright'); ?>
<?php wp_footer(); ?>