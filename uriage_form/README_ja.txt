﻿＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

    PHPメールフォーム - Webサイトに設置できる問い合わせ受付フォーム

                                               (c)ジューベー株式会社

＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

本ソフトウェアは、Webサイトで問い合わせを受け付けるPHPスクリプトです。

  【特徴】
  ・HTMLファイルを修正するだけでデザイン変更できます。
  ・問い合わせ項目の変更が自由にできます。
  ・ユーザーに受付内容を自動送信。
  ・JavaScriptによる必須項目チェック機能。
  ・(UTF-8のみ) スペイン語、ロシア語、中国語などの問い合わせも受け付けます。
  ・対スパム機能がついています (試験中)。
  ・sendmailが使えなくても大丈夫。保存データをあとで閲覧できます。
  ・PEAR::MailによるSMTPメール送信に対応(上級者向け)
  ・PHP4、PHP5、Linux/Unixサーバー、Windowsサーバーに対応。

■使用許諾条件
￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣
無料でご利用の場合は、リンクと著作者の表示を維持してください。
http://jubei.co.jp/php/eula_ja.html

有料のビジネスライセンス購入で、上記の表示をなくしてご利用いただけます。
http://jubei.co.jp/php/license.html

■設置方法
￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣
下記のページに、詳しい画像つきの説明があります。
http://jubei.co.jp/formmail/setup.html

1. config.phpを変更する(推奨)

  「管理者(自分)、ユーザ宛てにメールを送りたい」、「ログ閲覧機能をすぐに使
  いたい」場合はconfig.phpを変更します。
  詳しくは、http://jubei.co.jp/formmail/config_vars.html をご覧ください。

2. レンタルサーバースペースにアップロードする
3. dataフォルダに属性757、または777を設定する
4. Webブラウザでjform.phpにアクセスする。

■動作確認環境
￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣
・Windows XP SP3 + PHP5
・CentOS5 + PHP5

レンタルサーバーでの動作試験状況もご覧ください。
http://jubei.co.jp/formmail/servertest.html

■お問い合わせ
￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣
開発元：ジューベー株式会社  http://jubei.co.jp/  E-mail info@jubei.co.jp

ご不明な点はサポート掲示板http://jubei.co.jp/bbs/bbs2.php、またはメールにてお問
い合わせください。

■履歴
￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣￣
2010-02-10  Ver.0.34
    ・共用SSLレンタルサーバーの場合、送信しましたページが表示されない不具合
      に対応。(config.phpのSCRIPT_URLを設定してください)
    ・<input type="image">をボタンにした場合に画面が進まない不具合を修正。
    ・送信メールヘッダのエンコーディング処理不具合を修正。
    ・データフォルダに書き込み権限がない場合にエラーメッセージを表示する機能
      を追加。
2009-03-02  Ver.0.33
    ・PHP4での動作不具合を修正。
2009-02-06  Ver.0.32
    ・データファイルに記録する問い合わせ内容を、メールテンプレートを用いない
      従来方式に変更。
      (テンプレート記述ミスで、項目が保存されない問題の回避) 
    ・JavaScript入力チェックで、必須項目を満たしている場合のエラーメッセージ
      表示を修正。
    ・入力文字エンコーディングのチェック機能を追加。
    ・Shift_JIS版、EUC-JP版の配布を再開。
2009-02-05  Ver.0.31
    ・ファイル名変更: form.php→jform.php、form.js→jform.js
    ・PHPメールフォーム本体(jform.php)のソースコードを非公開に変更。
    ・リンクと著作者表示を自動挿入に変更。
    ・config.php: SERIAL_KEY定数を追加。OUTHEADER_APP定数を廃止。
    ・Shift_JIS版、EUC-JP版の配布を一時停止。
2009-02-02  Ver.0.3
    ・ユーザへのメール送信機能を追加。
    ・メールテンプレート機能の導入。
    ・JavaScriptによる必須項目のチェック機能を追加。
    ・スパム対策機能の強化。
    ・SMTPメール送信機能(上級者向け)を追加。
    ・<formitem>の追加。確認ページからフォームページへ戻ったときに、以前の
      入力値を復帰するように変更。
    ・{$_POST_…}を廃止予定に指定。代わりに{$_POSTHTML_…}、{$_POSTVAL_…}
      を使ってください。
2008-03-11  Ver.0.24b
    ・タイムゾーンの設定方法を変更。APP_TIMEZONE廃止、TIMEZONE_HOURS追加。
    ・php.iniによるmbstring関係設定値の自動チェック機能を追加。
    ・OUTHEADER_APP定数を追加。
2007-11-27  Ver.0.23b
    ・レンタルサーバーで動作試験を実施 (14件)。
    ・メールの改行コードを自動判別するように修正。MAIL_LINEBREAKを廃止。
    ・sendmail使用時の-fオプション付加を廃止。
    ・gzipエンコーディングを任意に変更 (GZIP_ENCODING定数を追加)。
2007-11-25  Ver.0.22b
    ・送信データを常にstripslashes()してしまう不具合を修正
2007-11-23  Ver.0.21b
    ・SJIS、EUC-JP版を公開
    ・必須項目の指定ができる機能を追加
2007-11-07  Ver.0.2b
    ・qmailのsendmailラッパー使用時にメールヘッダが破損する不具合を修正
2007-09-20  Ver.0.1b
    ・弊社Webサイトにて公開開始
